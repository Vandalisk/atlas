class AddFullNameToUsers < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      UPDATE users SET full_name = (first_name || ' ' ||last_name)
    SQL
  end

  def self.down
    User.update_all(full_name: nil)
  end
end
