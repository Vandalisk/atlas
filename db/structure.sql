--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: cube; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS cube WITH SCHEMA public;


--
-- Name: EXTENSION cube; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION cube IS 'data type for multidimensional cubes';


--
-- Name: earthdistance; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS earthdistance WITH SCHEMA public;


--
-- Name: EXTENSION earthdistance; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION earthdistance IS 'calculate great-circle distances on the surface of the Earth';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: administrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE administrations (
    id integer NOT NULL,
    title character varying,
    description text,
    email character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: administrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE administrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: administrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE administrations_id_seq OWNED BY administrations.id;


--
-- Name: admins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE admins (
    id integer NOT NULL,
    email character varying,
    encrypted_password character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: admins_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE admins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE admins_id_seq OWNED BY admins.id;


--
-- Name: black_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE black_lists (
    id integer NOT NULL,
    place_id integer,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: black_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE black_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: black_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE black_lists_id_seq OWNED BY black_lists.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE categories (
    id integer NOT NULL,
    title character varying NOT NULL
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: chats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE chats (
    id integer NOT NULL,
    sender_id integer NOT NULL,
    recipient_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: chats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE chats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: chats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE chats_id_seq OWNED BY chats.id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE comments (
    id integer NOT NULL,
    body text DEFAULT ''::text,
    rating integer DEFAULT 0,
    place_id integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    CONSTRAINT check_constraint_rating CHECK (((rating > 0) AND (rating <= 5)))
);


--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE comments_id_seq OWNED BY comments.id;


--
-- Name: data_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE data_migrations (
    version character varying NOT NULL
);


--
-- Name: events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE events (
    id integer NOT NULL,
    title character varying NOT NULL,
    description text NOT NULL,
    "time" timestamp without time zone NOT NULL,
    place_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- Name: friendships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE friendships (
    id integer NOT NULL,
    friendable_id integer,
    friendable_type character varying,
    friend_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    blocker_id integer,
    status integer
);


--
-- Name: friendships_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE friendships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: friendships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE friendships_id_seq OWNED BY friendships.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE messages (
    id integer NOT NULL,
    body text,
    chat_id integer,
    user_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE messages_id_seq OWNED BY messages.id;


--
-- Name: moderators; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE moderators (
    id integer NOT NULL,
    email character varying NOT NULL,
    encrypted_password character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    radius integer DEFAULT 500 NOT NULL
);


--
-- Name: moderators_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE moderators_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: moderators_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE moderators_id_seq OWNED BY moderators.id;


--
-- Name: news; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE news (
    id integer NOT NULL,
    title character varying NOT NULL,
    description text NOT NULL,
    place_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    radius integer DEFAULT 500 NOT NULL,
    moderator_id integer NOT NULL
);


--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE news_id_seq OWNED BY news.id;


--
-- Name: places; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE places (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    image_path character varying,
    additional_phone_number character varying NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    average_rating double precision DEFAULT 0.0,
    max_events_per_day integer DEFAULT 1,
    events_today integer DEFAULT 0,
    address character varying,
    weekdays_hours character varying,
    weekends_hours character varying,
    category_id integer
);


--
-- Name: places_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: places_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE places_id_seq OWNED BY places.id;


--
-- Name: read_marks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE read_marks (
    id integer NOT NULL,
    readable_id integer,
    readable_type character varying NOT NULL,
    reader_id integer,
    reader_type character varying NOT NULL,
    "timestamp" timestamp without time zone
);


--
-- Name: read_marks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE read_marks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: read_marks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE read_marks_id_seq OWNED BY read_marks.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE settings (
    id integer NOT NULL,
    timezone character varying DEFAULT 'Europe/Moscow'::character varying,
    max_events_per_day integer DEFAULT 1,
    search_radius_friends integer DEFAULT 500,
    super_user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE settings_id_seq OWNED BY settings.id;


--
-- Name: super_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE super_users (
    id integer NOT NULL,
    email character varying,
    encrypted_password character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: super_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE super_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: super_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE super_users_id_seq OWNED BY super_users.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    age integer NOT NULL,
    sex character varying NOT NULL,
    search_radius_friends integer DEFAULT 500 NOT NULL,
    invisible boolean DEFAULT false NOT NULL,
    phone_number character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image_path character varying,
    social_id character varying,
    social_marker integer DEFAULT 3 NOT NULL,
    owner boolean DEFAULT false NOT NULL,
    latitude numeric(10,6) NOT NULL,
    longitude numeric(10,6) NOT NULL,
    private boolean DEFAULT false,
    full_name character varying
);


--
-- Name: users_events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users_events (
    id integer NOT NULL,
    user_id integer,
    event_id integer
);


--
-- Name: users_events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_events_id_seq OWNED BY users_events.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: violations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE violations (
    id integer NOT NULL,
    title character varying NOT NULL,
    description text NOT NULL,
    latitude numeric(10,6) NOT NULL,
    longitude numeric(10,6) NOT NULL,
    category integer DEFAULT 1,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer,
    status integer DEFAULT 0,
    photos json DEFAULT '{}'::json
);


--
-- Name: violations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE violations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: violations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE violations_id_seq OWNED BY violations.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY administrations ALTER COLUMN id SET DEFAULT nextval('administrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY admins ALTER COLUMN id SET DEFAULT nextval('admins_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY black_lists ALTER COLUMN id SET DEFAULT nextval('black_lists_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY chats ALTER COLUMN id SET DEFAULT nextval('chats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY friendships ALTER COLUMN id SET DEFAULT nextval('friendships_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY messages ALTER COLUMN id SET DEFAULT nextval('messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY moderators ALTER COLUMN id SET DEFAULT nextval('moderators_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY news ALTER COLUMN id SET DEFAULT nextval('news_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY places ALTER COLUMN id SET DEFAULT nextval('places_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY read_marks ALTER COLUMN id SET DEFAULT nextval('read_marks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY settings ALTER COLUMN id SET DEFAULT nextval('settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY super_users ALTER COLUMN id SET DEFAULT nextval('super_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_events ALTER COLUMN id SET DEFAULT nextval('users_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY violations ALTER COLUMN id SET DEFAULT nextval('violations_id_seq'::regclass);


--
-- Name: administrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY administrations
    ADD CONSTRAINT administrations_pkey PRIMARY KEY (id);


--
-- Name: admins_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (id);


--
-- Name: black_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY black_lists
    ADD CONSTRAINT black_lists_pkey PRIMARY KEY (id);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: chats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY chats
    ADD CONSTRAINT chats_pkey PRIMARY KEY (id);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: friendships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY friendships
    ADD CONSTRAINT friendships_pkey PRIMARY KEY (id);


--
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: moderators_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY moderators
    ADD CONSTRAINT moderators_pkey PRIMARY KEY (id);


--
-- Name: news_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: places_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY places
    ADD CONSTRAINT places_pkey PRIMARY KEY (id);


--
-- Name: read_marks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY read_marks
    ADD CONSTRAINT read_marks_pkey PRIMARY KEY (id);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: super_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY super_users
    ADD CONSTRAINT super_users_pkey PRIMARY KEY (id);


--
-- Name: users_events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_events
    ADD CONSTRAINT users_events_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: violations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY violations
    ADD CONSTRAINT violations_pkey PRIMARY KEY (id);


--
-- Name: chats_recipint_sender_uniq_bunch; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX chats_recipint_sender_uniq_bunch ON chats USING btree ((GREATEST(sender_id, recipient_id)), (LEAST(sender_id, recipient_id)));


--
-- Name: index_black_lists_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_black_lists_on_place_id ON black_lists USING btree (place_id);


--
-- Name: index_black_lists_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_black_lists_on_user_id ON black_lists USING btree (user_id);


--
-- Name: index_categories_on_title; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_categories_on_title ON categories USING btree (title);


--
-- Name: index_chats_on_recipient_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_chats_on_recipient_id ON chats USING btree (recipient_id);


--
-- Name: index_chats_on_recipient_id_and_sender_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_chats_on_recipient_id_and_sender_id ON chats USING btree (recipient_id, sender_id);


--
-- Name: index_chats_on_sender_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_chats_on_sender_id ON chats USING btree (sender_id);


--
-- Name: index_comments_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_place_id ON comments USING btree (place_id);


--
-- Name: index_comments_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_user_id ON comments USING btree (user_id);


--
-- Name: index_events_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_place_id ON events USING btree (place_id);


--
-- Name: index_messages_on_chat_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_messages_on_chat_id ON messages USING btree (chat_id);


--
-- Name: index_messages_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_messages_on_created_at ON messages USING btree (created_at);


--
-- Name: index_messages_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_messages_on_user_id ON messages USING btree (user_id);


--
-- Name: index_moderators_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_moderators_on_email ON moderators USING btree (email);


--
-- Name: index_news_on_moderator_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_news_on_moderator_id ON news USING btree (moderator_id);


--
-- Name: index_news_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_news_on_place_id ON news USING btree (place_id);


--
-- Name: index_on_users_location; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_on_users_location ON users USING gist (ll_to_earth((latitude)::double precision, (longitude)::double precision));


--
-- Name: index_places_on_category_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_category_id ON places USING btree (category_id);


--
-- Name: index_places_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_user_id ON places USING btree (user_id);


--
-- Name: index_settings_on_super_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_settings_on_super_user_id ON settings USING btree (super_user_id);


--
-- Name: index_users_events_on_event_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_events_on_event_id ON users_events USING btree (event_id);


--
-- Name: index_users_events_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_events_on_user_id ON users_events USING btree (user_id);


--
-- Name: index_users_on_phone_number_and_social_marker; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_phone_number_and_social_marker ON users USING btree (phone_number, social_marker);


--
-- Name: index_users_on_social_id_and_social_marker; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_social_id_and_social_marker ON users USING btree (social_id, social_marker);


--
-- Name: index_violations_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_violations_on_user_id ON violations USING btree (user_id);


--
-- Name: read_marks_reader_readable_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX read_marks_reader_readable_index ON read_marks USING btree (reader_id, reader_type, readable_type, readable_id);


--
-- Name: unique_data_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_data_migrations ON data_migrations USING btree (version);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: fk_rails_03de2dc08c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_rails_03de2dc08c FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_3524b5b059; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY violations
    ADD CONSTRAINT fk_rails_3524b5b059 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_3f63e6ecd1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY black_lists
    ADD CONSTRAINT fk_rails_3f63e6ecd1 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_5a520410f3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_rails_5a520410f3 FOREIGN KEY (place_id) REFERENCES places(id);


--
-- Name: fk_rails_7508c19fa1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY black_lists
    ADD CONSTRAINT fk_rails_7508c19fa1 FOREIGN KEY (place_id) REFERENCES places(id);


--
-- Name: fk_rails_8047f44aee; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY places
    ADD CONSTRAINT fk_rails_8047f44aee FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_91654ba2c2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY news
    ADD CONSTRAINT fk_rails_91654ba2c2 FOREIGN KEY (place_id) REFERENCES places(id);


--
-- Name: fk_rails_b2e7252813; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events
    ADD CONSTRAINT fk_rails_b2e7252813 FOREIGN KEY (place_id) REFERENCES places(id) ON DELETE CASCADE;


--
-- Name: fk_rails_d7b05b07d2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT fk_rails_d7b05b07d2 FOREIGN KEY (super_user_id) REFERENCES super_users(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20160622112103');

INSERT INTO schema_migrations (version) VALUES ('20160624054559');

INSERT INTO schema_migrations (version) VALUES ('20160628105657');

INSERT INTO schema_migrations (version) VALUES ('20160628113323');

INSERT INTO schema_migrations (version) VALUES ('20160628134636');

INSERT INTO schema_migrations (version) VALUES ('20160629074355');

INSERT INTO schema_migrations (version) VALUES ('20160630134816');

INSERT INTO schema_migrations (version) VALUES ('20160707104722');

INSERT INTO schema_migrations (version) VALUES ('20160707104723');

INSERT INTO schema_migrations (version) VALUES ('20160707104724');

INSERT INTO schema_migrations (version) VALUES ('20160713124849');

INSERT INTO schema_migrations (version) VALUES ('20160713130240');

INSERT INTO schema_migrations (version) VALUES ('20160719110049');

INSERT INTO schema_migrations (version) VALUES ('20160721132939');

INSERT INTO schema_migrations (version) VALUES ('20160722083756');

INSERT INTO schema_migrations (version) VALUES ('20160725140801');

INSERT INTO schema_migrations (version) VALUES ('20160726140430');

INSERT INTO schema_migrations (version) VALUES ('20160727113445');

INSERT INTO schema_migrations (version) VALUES ('20160727151729');

INSERT INTO schema_migrations (version) VALUES ('20160729130124');

INSERT INTO schema_migrations (version) VALUES ('20160801121753');

INSERT INTO schema_migrations (version) VALUES ('20160802071452');

INSERT INTO schema_migrations (version) VALUES ('20160802073615');

INSERT INTO schema_migrations (version) VALUES ('20160804071103');

INSERT INTO schema_migrations (version) VALUES ('20160808142223');

INSERT INTO schema_migrations (version) VALUES ('20160808160344');

INSERT INTO schema_migrations (version) VALUES ('20160809102710');

INSERT INTO schema_migrations (version) VALUES ('20160811081209');

INSERT INTO schema_migrations (version) VALUES ('20160811095413');

INSERT INTO schema_migrations (version) VALUES ('20160812081352');

INSERT INTO schema_migrations (version) VALUES ('20160815113642');

INSERT INTO schema_migrations (version) VALUES ('20160815134834');

INSERT INTO schema_migrations (version) VALUES ('20160815140829');

INSERT INTO schema_migrations (version) VALUES ('20160816075408');

INSERT INTO schema_migrations (version) VALUES ('20160816081415');

INSERT INTO schema_migrations (version) VALUES ('20160816130025');

INSERT INTO schema_migrations (version) VALUES ('20160816130942');

INSERT INTO schema_migrations (version) VALUES ('20160817104228');

INSERT INTO schema_migrations (version) VALUES ('20160817122313');

INSERT INTO schema_migrations (version) VALUES ('20160818094454');

INSERT INTO schema_migrations (version) VALUES ('20160823191410');

INSERT INTO schema_migrations (version) VALUES ('20160825101229');

INSERT INTO schema_migrations (version) VALUES ('20160829090855');

INSERT INTO schema_migrations (version) VALUES ('20160901115452');

INSERT INTO schema_migrations (version) VALUES ('20160902082055');

INSERT INTO schema_migrations (version) VALUES ('20160902111656');

INSERT INTO schema_migrations (version) VALUES ('20160916131443');

INSERT INTO schema_migrations (version) VALUES ('20160919082427');

INSERT INTO schema_migrations (version) VALUES ('20160923105959');

INSERT INTO schema_migrations (version) VALUES ('20160926121807');

INSERT INTO schema_migrations (version) VALUES ('20160928075628');

INSERT INTO schema_migrations (version) VALUES ('20160929113355');

INSERT INTO schema_migrations (version) VALUES ('20161003144103');

INSERT INTO schema_migrations (version) VALUES ('20161004144423');

INSERT INTO schema_migrations (version) VALUES ('20161004145127');

INSERT INTO schema_migrations (version) VALUES ('20161005101140');

INSERT INTO schema_migrations (version) VALUES ('20161005124814');

