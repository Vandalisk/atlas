# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

            Admin.find_by(email: "1@1.com")     || FactoryGirl.create(:admin, email: "1@1.com")
            SuperUser.find_by(email: "1@1.com") || FactoryGirl.create(:super_user, email: "1@1.com")
moderator = Moderator.find_by(email: "1@1.com") || FactoryGirl.create(:moderator, email: "1@1.com")

FactoryGirl.create_list(:administration, 10)

users = (1..50).collect do
  FactoryGirl.create(:user, latitude: rand(50.7959..59.321), longitude: rand(45.7959..50.321))
end

["поесть", "бары-клубы", "гостиницы", "турестические развлечения", "супорты"].each do |title|
  Category.find_or_create_by(title: title)
end

book = Spreadsheet.open 'kazan.xls'
sheet = book.worksheet 0

(2..501).each do |num|
  begin
    phone_number = if sheet.row(num)[7].blank?
                     if sheet.row(num)[8].blank?
                       Faker::PhoneNumber.cell_phone
                     else
                       sheet.row(num)[8].split(';')[0].strip
                     end
                   else
                     sheet.row(num)[7].split(';')[0].strip
                   end
    puts num, phone_number

    next if User.where(phone_number: phone_number).exists?

    FactoryGirl.create(:place,
      name: sheet.row(num)[1],
      description: sheet.row(num)[2],
      address: sheet.row(num)[13],
      user: FactoryGirl.create(:user,
                                latitude: sheet.row(num)[15],
                                longitude: sheet.row(num)[16],
                                phone_number: phone_number,
                                owner: true
                               )
    )
  rescue Exception
    next
  end
end

places_owners = (1..50).collect do
  FactoryGirl.create(:user,
    owner: true,
    latitude: rand(50.7959..59.321),
    longitude: rand(45.7959..50.321)
  )
end

places = places_owners.map { |owner| FactoryGirl.create(:place, user: owner) }

10.times do
  FactoryGirl.create(:violation, latitude: rand(50.7959..59.321), longitude: rand(45.7959..50.321))
end

popular_user = users.first

places[0..20].each { |place| place.friend_request(popular_user) }
users[1..20].each { |user| user.friend_request(popular_user) }

popular_user.my_requested_friends.first(10).each do |requested_friend|
  popular_user.accept_request(requested_friend)
end

popular_user.my_requested_friends.last(10).each do |requested_friend|
  popular_user.accept_request(requested_friend)
end

popular_place = places.first

FactoryGirl.create_list(:news, 10, place: popular_place, moderator: moderator)

users[0..20].each { |user| user.friend_request(popular_place) }
places[1..20].each { |place| place.friend_request(popular_place) }

popular_place.my_requested_friends.first(10).each do |requested_friend|
  popular_place.accept_request(requested_friend)
end

popular_place.my_requested_friends.last(10).each do |requested_friend|
  popular_place.accept_request(requested_friend)
end
