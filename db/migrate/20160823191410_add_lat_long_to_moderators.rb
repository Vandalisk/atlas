class AddLatLongToModerators < ActiveRecord::Migration
  def change
    add_column :moderators, :latitude, :decimal, precision: 10, scale: 6, null: false
    add_column :moderators, :longitude, :decimal, precision: 10, scale: 6, null: false
  end
end
