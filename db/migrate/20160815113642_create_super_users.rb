class CreateSuperUsers < ActiveRecord::Migration
  def change
    create_table :super_users do |t|
      t.string :email
      t.string :encrypted_password
      t.timestamps null: false
    end
  end
end
