class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title, null: false
      t.text :description, null: false
      t.references :place, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
