class AddRatingConstraintToComment < ActiveRecord::Migration
  def self.up
    execute "ALTER TABLE comments ADD CONSTRAINT check_constraint_rating CHECK (rating > 0 AND rating <= 5)"
  end

  def self.down
    execute "ALTER TABLE comments DROP CONSTRAINT check_constraint_rating"
  end
end
