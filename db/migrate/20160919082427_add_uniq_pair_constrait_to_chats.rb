class AddUniqPairConstraitToChats < ActiveRecord::Migration
  def up
    add_index :chats, [:recipient_id, :sender_id], unique: true
    execute <<-SQL
      create unique index chats_recipint_sender_uniq_bunch on chats(greatest(sender_id, recipient_id), least(sender_id, recipient_id));
    SQL
  end

  def down
    remove_index(:chats, { column:[:recipient_id, :sender_id]})
    execute <<-SQL
      drop index IF EXISTS chats_recipint_sender_uniq_bunch;
    SQL
  end
end
