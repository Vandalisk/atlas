class ChangeModeratorsTable < ActiveRecord::Migration
  def self.up
    rename_column :moderators, :crypted_password, :encrypted_password
    remove_column :moderators, :salt
    remove_column :moderators, :remember_me_token
    remove_column :moderators, :remember_me_token_expires_at
  end

  def self.down
    rename_column :moderators, :encrypted_password, :crypted_password
    add_column :moderators,  :salt, :string
    add_column :moderators,  :remember_me_token, :string
    add_column :moderators,  :remember_me_token_expires_at, 'timestamp without time zone'
  end
end
