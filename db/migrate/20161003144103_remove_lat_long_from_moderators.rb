class RemoveLatLongFromModerators < ActiveRecord::Migration
  def change
    remove_column :moderators, :latitude
    remove_column :moderators, :longitude
  end
end
