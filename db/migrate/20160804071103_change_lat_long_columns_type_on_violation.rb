class ChangeLatLongColumnsTypeOnViolation < ActiveRecord::Migration
  def change
    change_column :violations, :latitude, :decimal, precision: 10, scale: 6, null: false
    change_column :violations, :longitude, :decimal, precision: 10, scale: 6, null: false
  end
end
