class AddEventsPerDayToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :max_events_per_day, :integer, default: 1
    add_column :places, :events_today, :integer, default: 0
  end
end
