class AddIndexToLlToEarthExpression < ActiveRecord::Migration
  def self.up
    execute %{drop index if exists index_on_users_location}
    execute %{drop extension if exists postgis}
    execute %{
      create index index_on_users_location ON users using gist (
        ll_to_earth(latitude, longitude)
      )
    }
  end

  def self.down
    execute %{drop index index_on_users_location}
  end
end
