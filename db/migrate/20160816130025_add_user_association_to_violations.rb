class AddUserAssociationToViolations < ActiveRecord::Migration
  def change
    add_reference :violations, :user, index: true, foreign_key: true
  end
end
