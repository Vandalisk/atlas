class AddAddressToUsersAndPlaces < ActiveRecord::Migration
  def change
    add_column :users, :address, :string
    add_column :places, :address, :string
  end
end
