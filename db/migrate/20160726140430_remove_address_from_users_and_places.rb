class RemoveAddressFromUsersAndPlaces < ActiveRecord::Migration
  def change
    remove_column :users, :address, :string
    remove_column :places, :address, :string
  end
end
