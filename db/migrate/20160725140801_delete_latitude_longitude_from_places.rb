class DeleteLatitudeLongitudeFromPlaces < ActiveRecord::Migration
  def change
    remove_column :places, :latitude
    remove_column :places, :longitude
  end
end
