class ChangePhotosTypeOfViolations < ActiveRecord::Migration
  def self.up
    remove_column :violations, :photos
    add_column :violations, :photos, :json, default: {}
  end

  def self.down
    remove_column :violations, :photos
    add_column :violations, :photos, :string, array: true, default: []
  end
end
