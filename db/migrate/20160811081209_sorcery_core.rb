class SorceryCore < ActiveRecord::Migration
  def change
    create_table :moderators do |t|
      t.string :email,            :null => false
      t.string :crypted_password
      t.string :salt

      t.timestamps
    end

    add_index :moderators, :email, unique: true
  end
end