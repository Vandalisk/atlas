class AddStatusToViolations < ActiveRecord::Migration
  def change
    add_column :violations, :status, :integer, default: 0
  end
end
