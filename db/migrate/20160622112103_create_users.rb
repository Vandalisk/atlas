class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.integer :age, null: false
      t.string :sex, null: false
      t.integer :search_radius_friends, default: 500, null: false
      t.boolean :invisible, default: false, null: false
      t.string :phone_number, null: false

      t.timestamps null: false
      t.index [:phone_number], unique: true
    end
  end
end
