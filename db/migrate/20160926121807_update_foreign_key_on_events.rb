class UpdateForeignKeyOnEvents < ActiveRecord::Migration
  def change
    remove_foreign_key :events, :places

    add_foreign_key :events, :places, on_delete: :cascade
  end
end
