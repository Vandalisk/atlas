class RemoveNullConstraitFromUsersImagePath < ActiveRecord::Migration
  def change
    change_column :users, :image_path, :string, null: true
  end
end
