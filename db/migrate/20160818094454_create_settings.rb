class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :timezone, default: "#{ActiveSupport::TimeZone::MAPPING["Moscow"]}"
      t.integer :max_events_per_day, default: 1
      t.integer :search_radius_friends, default: 500
      t.references :super_user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
