class AddRadiusToModerators < ActiveRecord::Migration
  def change
    add_column :moderators, :radius, :integer, default: 500, null: false
  end
end
