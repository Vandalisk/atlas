class CreateChats < ActiveRecord::Migration
  def change
    create_table :chats do |t|
      t.integer :sender_id, index: true
      t.integer :recipient_id, index: true

      t.timestamps
    end
  end
end
