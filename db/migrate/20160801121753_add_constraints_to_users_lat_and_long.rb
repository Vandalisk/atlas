class AddConstraintsToUsersLatAndLong < ActiveRecord::Migration
  def change
    change_column :users, :latitude, :decimal, precision: 10, scale: 6, null: false
    change_column :users, :longitude, :decimal, precision: 10, scale: 6, null: false
  end
end
