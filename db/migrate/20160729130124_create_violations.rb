class CreateViolations < ActiveRecord::Migration
  def change
    create_table :violations do |t|
      t.string :title, null: false
      t.text :description, null: false, limit: 1000
      t.decimal :latitude, precision: 10, scale: 6
      t.decimal :longitude, precision: 10, scale: 6
      t.integer :category, default: 1
      t.string :photos, array: true, default: []

      t.timestamps null: false
    end
  end
end
