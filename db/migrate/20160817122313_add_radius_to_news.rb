class AddRadiusToNews < ActiveRecord::Migration
  def change
    add_column :news, :radius, :integer, default: 500, null: false
  end
end
