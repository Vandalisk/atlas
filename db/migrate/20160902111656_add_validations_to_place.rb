class AddValidationsToPlace < ActiveRecord::Migration
  def change
    change_column :places, :description, :text, null: false, default: ''
    change_column_null :places, :name, false
    change_column_null :places, :additional_phone_number, false
    change_column_null :places, :user_id, false
  end
end
