class AddAverageRatingForPlace < ActiveRecord::Migration
  def change
    add_column :places, :average_rating, :float, default: 0
  end
end
