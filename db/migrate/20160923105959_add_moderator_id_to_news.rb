class AddModeratorIdToNews < ActiveRecord::Migration
  def change
    add_reference :news, :moderator, null: false, index: true
  end
end
