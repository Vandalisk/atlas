class AddSocialIdSocialMarkerToUsers < ActiveRecord::Migration
  def change
    add_column :users, :social_id, :string
    add_column :users, :social_marker, :integer, null: false, default: 3

    add_index :users, [:social_id, :social_marker], unique: true
    add_index :users, [:phone_number, :social_marker], unique: true
  end
end
