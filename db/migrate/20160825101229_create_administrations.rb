class CreateAdministrations < ActiveRecord::Migration
  def change
    create_table :administrations do |t|
      t.string :title
      t.text :description
      t.string :email

      t.timestamps null: false
    end
  end
end
