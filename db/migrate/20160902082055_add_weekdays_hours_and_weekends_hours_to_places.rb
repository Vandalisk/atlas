class AddWeekdaysHoursAndWeekendsHoursToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :weekdays_hours, :string
    add_column :places, :weekends_hours, :string
  end
end
