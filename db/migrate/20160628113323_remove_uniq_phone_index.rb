class RemoveUniqPhoneIndex < ActiveRecord::Migration
  def up
    remove_index :users, :phone_number
  end

  def down
    add_index :users, :phone_number unless index_exists?(:users, :phone_number)
  end
end
