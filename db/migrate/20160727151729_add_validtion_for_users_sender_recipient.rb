class AddValidtionForUsersSenderRecipient < ActiveRecord::Migration
  def self.up
    change_column :chats, :sender_id, :integer, null: false
    change_column :chats, :recipient_id, :integer, null: false
  end
  def self.down
    change_column :chats, :sender_id, :integer, null: true
    change_column :chats, :recipient_id, :integer, null: true
  end
end
