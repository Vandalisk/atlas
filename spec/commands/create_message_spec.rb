require 'rails_helper'

describe CreateMessage do
  let(:recipient) { create(:user) }
  let(:sender) { create(:user) }
  let(:chat) { create(:chat, recipient: recipient, sender: sender) }
  let(:create_params) do
    {
      chat_id: chat.id,
      sender: sender,
      body: Faker::Lorem.sentence
    }
  end
  let(:invalid_params) do
    create_params.merge({body: ""})
  end

  it "returns chat" do
    expect{
      CreateMessage.new(chat, create_params.except(:chat_id)).execute
    }.to change{ Message.count }.by(1)

    expect(CreateMessage.new(chat, create_params.except(:chat_id)).execute).to be_a(Message)
  end

  it "returns errors if attrs invalid" do
    expect{
      CreateMessage.new(chat, invalid_params.except(:chat_id)).execute
    }.to raise_error(ActiveRecord::RecordInvalid)
  end
end
