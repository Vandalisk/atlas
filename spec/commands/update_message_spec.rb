require 'rails_helper'

describe UpdateMessage do
  let(:recipient) { create(:user) }
  let(:sender) { create(:user) }
  let(:chat) { create(:chat, recipient: recipient, sender: sender) }
  let(:message) { create(:message, chat: chat, user_id: sender.id) }

  let(:update_params) do
    {
      chat_id: chat.id,
      id: message.id,
    }
  end

  let(:invalid_params) do
    update_params.merge(id: rand(100..1000))
  end

  it "reads recipient's message." do
    expect{ UpdateMessage.new(chat, update_params.except(:chat_id)).execute }.
      to change{ recipient.have_read?(message) }.from(false).to(true)
  end

  it "should call push_to_broadcast method with correct params" do
    subject = UpdateMessage.new(chat, update_params.except(:chat_id))
    allow(subject).to receive(:push_to_broadcast)
    expect(subject).to receive(:push_to_broadcast).with(:successful_update, message, "chat_#{chat.id}")
    subject.execute
  end

  it "returns errors if attrs invalid" do
    expect{ UpdateMessage.new(chat, invalid_params.except(:chat_id)).execute }.to raise_error(ActiveRecord::RecordNotFound)
  end

  it "returns errors when message is already read." do
    message.mark_as_read!(for: message.recipient)

    expect{ UpdateMessage.new(chat, update_params.except(:chat_id)).execute }.to raise_error(ActiveRecord::RecordInvalid)
  end
end
