require 'rails_helper'

describe CreateChat do
  let(:recipient) { create(:user) }
  let(:sender) { create(:user) }
  let(:message) { create(:message) }
  let(:params) do
    {
      recipient_phone_number: recipient.phone_number,
      sender_phone_number: sender.phone_number,
      body: Faker::Lorem.sentence
    }
  end

  let(:invalid_params) do
    params.merge({recipient_phone_number: 123})
  end

  describe "Should create new chat." do
    it "count +1" do
      expect{ CreateChat.new(params).execute }.to change{ Chat.count }.by(1)
    end

    it "result is a chat" do
      expect(CreateChat.new(params).execute).to be_a(Chat)
    end

    it "should call push_to_broadcast method with correct params" do
      subject = CreateChat.new(params)
      chat = double(:chat)
      channel_name = "#{sender.phone_number[1..-1]}_#{recipient.phone_number[1..-1]}_chats"

      allow(subject).to receive(:push_to_broadcast)
      allow(Chat).to receive(:create!).and_return(chat)
      allow(chat).to receive_message_chain(:messages, :create!)
      expect(subject).to receive(:push_to_broadcast).with(:successful_create, chat, channel_name)

      subject.execute
    end
  end

  it "returns errors if attrs invalid" do
    expect{ CreateChat.new(invalid_params).execute }.to raise_error(ActiveRecord::RecordNotFound)
  end
end
