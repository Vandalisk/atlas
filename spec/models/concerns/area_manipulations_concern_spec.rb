require 'rails_helper'

describe AreaManipulationsConcern do
  context "users nearby" do
    let!(:far_user) { create(:user, latitude: 40.000000,longitude: -77.000000) }
    let!(:close_user) { create(:user, latitude: 39.010000,longitude: -75.990000) }
    let!(:user) { create(:user, latitude: 39.010300, longitude: -75.990050) }

    it 'should return all users nearby' do
      users_nearby = User.users_nearby(user)

      expect(users_nearby.size).to eq 1
      expect(users_nearby).to eq [close_user]
    end
  end

  context "places nearby" do
    let!(:far_user_owner) { create(:user, latitude: 40.000000,longitude: -77.000000, owner: true) }
    let!(:far_place) { create(:place, user: far_user_owner) }
    let!(:close_user_owner) { create(:user, latitude: 39.010000,longitude: -75.990000, owner: true) }
    let!(:close_place) { create(:place, user: close_user_owner) }
    let!(:user) { create(:user, latitude: 39.010300, longitude: -75.990050) }

    it 'should return all places nearby' do
      places_nearby = Place.places_nearby(user)

      expect(places_nearby.size).to eq 1
      expect(places_nearby).to eq [close_place]
    end
  end

  context "news nearby" do
    let!(:place1) { create(:place, user: create(:user, owner: true, latitude: 39.004300, longitude: -75.990200)) }
    let!(:place2) { create(:place, user: create(:user, owner: true, latitude: 39.000540, longitude: -75.990090)) }
    let!(:far_place) { create(:place, user: create(:user, owner: true, latitude: 38.000540, longitude: -74.990090)) }
    let(:news1) { create_list(:news, 2, place: place1) }
    let(:news2) { create_list(:news, 2, place: place2) }
    let!(:news) { news1 + news2 }
    let!(:far_news) { create_list(:news, 2, place: far_place) }
    let!(:user) { create(:user, latitude: 39.000400, longitude: -75.9900010) }

    it 'should return news nearby' do
      news_nearby = News.news_nearby(user)

      expect(news_nearby.size).to eq 4
      expect(news_nearby).to eq news.reverse
    end

    it 'return with far news if theirs raidus is huge' do
      far_news.each { |far_n| far_n.update(radius: 5000000) }
      news_nearby = News.news_nearby(user)

      expect(news_nearby.size).to eq 6
      expect(news_nearby).to eq(far_news.reverse + news.reverse)
    end
  end

  context "violations nearby" do
    let(:close_point) {{ latitude: 55.765591, longitude: 49.149769 }}
    let(:far_point) {{ latitude: 50.768591, longitude: 40.165269 }}
    let!(:close_violations) { create_list(:violation, 3, close_point) }
    let!(:far_violations) { create_list(:violation, 3, far_point) }
    let!(:user) { create(:user, latitude: 55.765494, longitude: 49.149921 ) }

    it 'finds all violations nearby' do
      violations = Violation.violations_nearby(user)
      expect(violations).to eq close_violations
    end
  end

  context "closest places to user with order by distance" do
    let!(:user1) { create(:user, latitude: 39.009981,longitude: -75.989996, owner: true) }
    let!(:user2) { create(:user, latitude: 39.010001,longitude: -75.990000, owner: true) }
    let!(:user3) { create(:user, latitude: 39.010101,longitude: -75.990020, owner: true) }
    let!(:user4) { create(:user, latitude: 39.010201,longitude: -75.990040, owner: true) }
    let!(:place1) { create(:place, user: user1) }
    let!(:place2) { create(:place, user: user2) }
    let!(:place3) { create(:place, user: user3) }
    let!(:place4) { create(:place, user: user4) }
    let!(:user) { create(:user, latitude: 39.010300, longitude: -75.990050) }
    let(:ordered_array) { [place4, place3, place2, place1] }

    it 'should return all places nearby by order' do
      places_nearby = Place.places_nearby(user).closest_to(user)

      expect(places_nearby.size).to eq 4
      expect(places_nearby).to eq ordered_array
    end
  end

  context "closest users to user with order by distance" do
    let!(:user1) { create(:user, latitude: 39.009981,longitude: -75.989996) }
    let!(:user2) { create(:user, latitude: 39.010001,longitude: -75.990000) }
    let!(:user3) { create(:user, latitude: 39.010101,longitude: -75.990020) }
    let!(:user4) { create(:user, latitude: 39.010201,longitude: -75.990040) }
    let!(:user) { create(:user, latitude: 39.010300, longitude: -75.990050) }
    let(:ordered_array) { [user4, user3, user2, user1] }

    it 'should return all places nearby by order' do
      users_nearby = User.users_nearby(user).closest_to(user)

      expect(users_nearby.size).to eq 4
      expect(users_nearby).to eq ordered_array
    end
  end

  context "closest events to user with order by distance" do
    let!(:user) { create(:user, latitude: 39.010300, longitude: -75.990050) }
    let!(:users) do
      [
        create(:user, latitude: 39.009981,longitude: -75.989996, owner: true),
        create(:user, latitude: 39.010001,longitude: -75.990000, owner: true),
        create(:user, latitude: 39.010101,longitude: -75.990020, owner: true),
        create(:user, latitude: 39.010201,longitude: -75.990040, owner: true)
      ]
    end

    let!(:places) { users.inject([]) { |memo, user| memo.push(create(:place, user: user)) } }
    let!(:events) { places.inject([]) { |memo, place| memo.push(create(:event, place: place)) } }
    let(:ordered_array) { events.reverse }

    it 'should return all places nearby by order' do
      events_nearby = Event.events_nearby(user).closest_to(user)

      expect(events_nearby.size).to eq 4
      expect(events_nearby).to eq ordered_array
    end
  end
end
