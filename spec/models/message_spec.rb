require 'rails_helper'

RSpec.describe Message, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  let(:sender) { create(:user) }
  let(:recipient) { create(:user) }
  let(:chat) { create(:chat, sender: sender, recipient: recipient) }
  let!(:new_message) { create(:message, chat: chat, sender: sender) }

  it "mark message as read for sender after creating" do
    expect(sender.have_read?(new_message)).to eq true
  end
end
