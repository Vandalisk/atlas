require 'rails_helper'

RSpec.describe Place, type: :model do
  it { is_expected.to have_many(:news).dependent(:destroy) }

  let!(:place) { create(:place) }
  let!(:comments) { create_list(:comment, 5, rating: 3, place: place) }

  describe 'check rating' do
    it 'scope returns average_rating' do
      expect(Float(Place.avg_rating(place.id)['average'])).to eq(place.average_rating)
    end

    it 'average rating is correct' do
      expect(place.average_rating).to eq(3)
    end
  end

  describe "blocked?" do
    let!(:place) { create(:place) }

    describe "should detect if user blocked" do
      let!(:user) { create(:user) }

      context "blocked" do
        let!(:black_list) { create(:black_list, place: place, user: user) }

        it "returns true" do
          expect(place.blocked?(user)).to eq(true)
        end
      end

      context "not blocked" do
        it "returns false" do
          expect(place.blocked?(user)).to eq(false)
        end
      end
    end

    describe "should detect if place blocked" do
      let!(:place2) { create(:place) }

      context "blocked" do
        let!(:black_list) { create(:black_list, place: place, user: place2.user) }

        it "returns true" do
          expect(place.blocked?(place2)).to eq(true)
        end
      end

      context "not blocked" do
        it "returns false" do
          expect(place.blocked?(place2)).to eq(false)
        end
      end
    end

    describe "#search_by_category_name" do
      let(:categories) { ['поесть', 'гостиницы'] }
      let(:eat_category) { create(:category, title: categories.first) }
      let(:hotel_category) { create(:category, title: categories.last) }
      let!(:eat_places) { create_list(:place, 2, category: eat_category) }
      let!(:hotel_places) { create_list(:place, 2, category: hotel_category) }

      it "return by eat category" do
        expect(Place.search_by_category_names([eat_category.title])).to eq(eat_places.reverse)
      end

      it "return by hotel category" do
        expect(Place.search_by_category_names([hotel_category.title])).to eq(hotel_places.reverse)
      end

      it "return by hotel and eat categories" do
        expect(Place.search_by_category_names(categories)).to eq(eat_places + hotel_places)
      end
    end
  end
end
