require 'rails_helper'

RSpec.describe News, type: :model do
  it { is_expected.to have_db_index(:place_id) }
  it { is_expected.to validate_presence_of :title }
  it { is_expected.to validate_presence_of :description }
  it { is_expected.to belong_to(:place) }
end
