require 'rails_helper'

RSpec.describe Comment, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:place) }
  it { is_expected.to validate_presence_of(:rating) }
  it { is_expected.to validate_numericality_of(:rating).is_less_than_or_equal_to(5) }
  it { is_expected.to validate_numericality_of(:rating).is_greater_than(0) }
  it { is_expected.to validate_length_of(:body).is_at_most(300) }

  describe 'triggers callbacks' do
    let!(:place) { create(:place) }

    context 'after create' do
      let(:comment) { build(:comment, place: place, user: place.user) }

      it 'changes average rating on place' do
        expect{ comment.save }.to change{ place.average_rating }
      end
    end

    context 'after destroy' do
      let!(:comment) { create(:comment, place: place, user: place.user) }

      it 'changes average rating on place' do
        expect{ comment.destroy }.to change{ place.average_rating }
      end
    end
  end
end
