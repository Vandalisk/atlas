require 'rails_helper'

RSpec.describe BlackList, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:place) }

  describe "self.add_by_phone_number!" do
    describe "Should add user to black list by phone number" do
      let!(:place) { create(:place) }
      let!(:blocked_user) { create(:user) }

      it 'count of black_list +1' do
        expect{ subject.class.add_by_phone_number!(place, blocked_user.phone_number) }.
          to change(place.black_list, :count).by(1)
      end

      it 'last black object in list is blocked user' do
        subject.class.add_by_phone_number!(place, blocked_user.phone_number)

        expect(place.black_list.last.user).to eq(blocked_user)
      end
    end

    describe "Should add place to black list by phone number" do
      let!(:place) { create(:place) }
      let!(:blocked_place) { create(:place) }

      it 'count of black_list +1' do
        expect{ subject.class.add_by_phone_number!(place, blocked_place.phone_number) }.
          to change(place.black_list, :count).by(1)
      end

      it 'last black object in list is blocked place' do
        subject.class.add_by_phone_number!(place, blocked_place.phone_number)

        expect(place.black_list.last.user.place).to eq(blocked_place)
      end
    end
  end

  describe "self.destroy_by_phone_number!" do
    let!(:place) { create(:place) }
    let!(:black_list) { create(:black_list, place: place) }

    describe "Should destroy user from black list by phone number" do
      let!(:unblocked_user) { create(:user) }

      before(:each) { BlackList.create(place: place, user: unblocked_user) }

      it 'count of black_list -1' do
        expect{ subject.class.destroy_by_phone_number!(place, unblocked_user.phone_number) }.
          to change(place.black_list, :count).by(-1)
      end

      it 'last black object in list is not an unblocked user' do
        subject.class.destroy_by_phone_number!(place, unblocked_user.phone_number)

        expect(place.black_list.last.user).not_to eq(unblocked_user)
      end
    end

    describe "Should destroy place from black list by phone number" do
      let!(:unblocked_place) { create(:place) }

      before(:each) { BlackList.create(place: place, user: unblocked_place.user) }

      it 'count of black_list -1' do
        expect{ subject.class.destroy_by_phone_number!(place, unblocked_place.phone_number) }.
          to change(place.black_list, :count).by(-1)
      end

      it 'last black object in list is not an unblocked place' do
        subject.class.destroy_by_phone_number!(place, unblocked_place.phone_number)

        expect(place.black_list.last.user).not_to eq(unblocked_place)
      end
    end
  end
end
