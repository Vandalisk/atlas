require 'rails_helper'

RSpec.describe Chat, type: :model do
  it { is_expected.to validate_presence_of :sender }
  it { is_expected.to validate_presence_of :recipient }
  it { is_expected.to have_db_index(%i(recipient_id sender_id)).unique(true) }

  describe "can't create chat between users, that already exists" do
    let(:sender) { create(:user) }
    let(:recipient) { create(:user) }
    let!(:chat) { create(:chat, recipient: recipient, sender: sender) }

    it "tries to create chat(sender: sender, recipient: recipient)" do
      expect{ Chat.create!(sender: sender, recipient: recipient) }.to raise_error(ActiveRecord::RecordInvalid)
    end

    it "tries to create chat(sender: recipient, recipient: sender)" do
      expect{ Chat.create!(sender: recipient, recipient: sender) }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
