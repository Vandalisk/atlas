require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user) { create(:user) }
  let!(:unformatted_phone_with_plus) { "+694.383.5068" }
  let!(:unformatted_phone_without_plus) { "694.383.5068" }

  it { is_expected.to have_db_index(%i(phone_number social_marker)).unique(true) }
  it { is_expected.to have_db_index(%i(social_id social_marker)).unique(true) }

  it { is_expected.to validate_presence_of :first_name }
  it { is_expected.to validate_presence_of :last_name }
  it { is_expected.to validate_presence_of :age }
  it { is_expected.to validate_presence_of :sex }
  it { is_expected.to validate_presence_of :search_radius_friends }
  it { is_expected.to validate_presence_of :phone_number }
  it { is_expected.to validate_presence_of :social_marker }

  it do
    is_expected.to validate_inclusion_of(:age).
      in_range(1..100).
      with_message("value should be in range from 1 to 100")
  end

  it do
    is_expected.to validate_inclusion_of(:sex).
      in_array(%w[Male Female]).
      with_message("value should equal Male or Female")
  end

  describe "Uniqueness validation" do
    context "Attempt to create user with non-unique phone number & social marker" do
      let!(:invalid_user) do
        build(:user, phone_number: user.phone_number, social_marker: user.social_marker)
      end

      it "returns true if user invalid" do
        expect(invalid_user).to be_invalid
      end
    end

    context "Attempt to create user with non-unique social_id & social marker" do
      let!(:invalid_user) do
        build(:user, social_id: user.social_id, social_marker: user.social_marker)
      end

      it "returns true if user invalid" do
        expect(invalid_user).to be_invalid
      end
    end
  end

  describe "Format phone" do
    context "with number which begins + character" do
      it "returns formated phone_number" do
        expect(
          user.format_phone(unformatted_phone_with_plus)
        ).to eq(normalize_number(unformatted_phone_with_plus))
      end
    end

    context "with number without + character" do
      it "returns formated phone_number" do
        expect(
          user.format_phone(unformatted_phone_without_plus)
        ).to eq(normalize_number(unformatted_phone_with_plus))
      end
    end
  end

  def normalize_number(number)
    PhonyRails.normalize_number(number).prepend("+")
  end
end
