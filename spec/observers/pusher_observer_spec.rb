require 'rails_helper'

describe PusherObserver do
  context "chats" do
    let(:recipient) { create(:user) }
    let(:sender) { create(:user) }
    let(:params) do
      {
        recipient_phone_number: recipient.phone_number,
        sender_phone_number: sender.phone_number,
        body: Faker::Lorem.sentence
      }
    end
    let(:command) { CreateChat.new(params) }

    it "subscribes object to all published events" do
      listener = double('listener')

      expect(listener).to receive(:success).with(Broadcast::QUOTE)
      command.subscribe(listener)
      command.execute
    end
  end

  context "messages" do
    let(:recipient) { create(:user) }
    let(:sender) { create(:user) }
    let(:chat) { create(:chat, recipient: recipient, sender: sender)}
    let(:create_params) do
      {
        chat_id: chat.id,
        sender: sender,
        body: Faker::Lorem.sentence
      }
    end
    let(:command) { CreateMessage.new(chat, create_params.except(:chat_id)) }

    it "subscribes object to all published events" do
      listener = double('listener')

      expect(listener).to receive(:success).with(Broadcast::QUOTE)
      command.subscribe(listener)
      command.execute
    end
  end
end
