require 'rails_helper'

RSpec.describe Authenticate do
  context "Decode user token" do
    let!(:user) { create(:user) }
    let!(:payload) do
      {
        phone_number: Normalizer.phone_number(user.phone_number),
        social_id: user.social_id,
        social_marker: User.social_markers[user.social_marker]
      }
    end
    let!(:token) { Token.encode(payload) }

    it "returns user instance" do
      expect(Authenticate.by(token)).to eq(user)
    end
  end

  # TODO: Uncomment after adding places table
  # context "Decode place token" do
  #   it "returns place instance" do
  #   end
  # end
end
