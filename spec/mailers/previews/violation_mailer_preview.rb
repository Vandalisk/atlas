# Preview all emails at http://localhost:3000/rails/mailers/violation_mailer
class ViolationMailerPreview < ActionMailer::Preview
  def complain_email
    ViolationMailer.complain_email("Really really bad thing!", ["1@1.com", "2@2.com"], "http://localhost:3000/admins/violations/2", Violation.last)
  end
end
