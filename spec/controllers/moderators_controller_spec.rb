# require 'rails_helper'

# describe ModeratorsController, type: :controller do
#   let(:moderator_attrs) { attributes_for(:moderator) }

#   context "allowed actions" do
#     describe "GET #new" do
#       before(:each) { get :new }

#       it "200 status" do
#         expect(response).to have_http_status(:success)
#       end

#       it "render new" do
#         expect(response).to render_template(:new)
#       end
#     end

#     describe "POST #create" do
#       let(:password) { moderator_attrs[:password] }
#       let(:valid_params) do
#         {
#           moderator: { email: moderator_attrs[:email], password: password, password_confirmation: password }
#         }
#       end

#       it "302 status" do
#         get :create, valid_params

#         expect(response).to have_http_status(:found)
#       end

#       it "render to admin root" do
#         get :create, valid_params

#         expect(response).to redirect_to(admin_root_url)
#       end

#       it "notification message" do
#         get :create, valid_params

#         expect(flash[:notice]).to eq("Signed up!")
#       end

#       it "count +1" do
#         expect{ get :create, valid_params }.to change(Moderator, :count).by(1)
#       end
#     end
#   end

#   context "forbidden actions" do
#     describe "POST #create" do
#       context "wrond password confirmation" do
#         let(:password) { moderator_attrs[:password] }
#         let(:invalid_params) do
#           {
#             moderator: { email: moderator_attrs[:email], password: password, password_confirmation: 'adsfassdfa' }
#           }
#         end

#         it "200 status" do
#           get :create, invalid_params

#           expect(response).to have_http_status(:success)
#         end

#         it "render new" do
#           get :create, invalid_params

#           expect(response).to render_template(:new)
#         end

#         it "count the same" do
#           expect{ get :create, invalid_params }.to change(Moderator, :count).by(0)
#         end
#       end

#       context "email is already taken" do
#         let(:password) { moderator_attrs[:password] }
#         let!(:moderator) { create(:moderator, email: moderator_attrs[:email]) }

#         let(:invalid_params) do
#           {
#             moderator: { email: moderator_attrs[:email], password: password, password_confirmation: password }
#           }
#         end

#         before(:each) { get :create, invalid_params }

#         it "200 status" do
#           get :create, invalid_params

#           expect(response).to have_http_status(:success)
#         end

#         it "render new" do
#           get :create, invalid_params

#           expect(response).to render_template(:new)
#         end

#         it "count the same" do
#           expect{ get :create, invalid_params }.to change(Moderator, :count).by(0)
#         end
#       end
#     end
#   end
# end