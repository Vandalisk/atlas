require 'rails_helper'

describe Api::V1::FriendsController, type: :controller do
  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:new_user_friend) { create(:user) }
  let!(:new_place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }
  let!(:user_token) { Token.generate(user) }
  let!(:place_token) { Token.generate(place) }
  let(:user_attrs) { %w(id first_name last_name image_path) }
  let(:place_attrs) { %w(id name description) }

  before(:each) do
    HasFriendship::Friendship.destroy_all
  end

  context "Find user friends if current user - User" do
    let!(:friendship) do
      user.friend_request(user_friend); user.friend_request(place_friend)
      user_friend.accept_request(user); place_friend.accept_request(user)
    end

    it "returns current user friends" do
      request.headers["Authorization"] = "Bearer #{user_token}"

      get :index, format: :json

      expect(response.status).to eq 200

      expect(JSON(response.body)["users_friends"]).to eq(
        ResponseHelper.object_render(friends.first, 'api/v1/friends/index',
          users_friends: [user_friend], places_friends: [place_friend])["users_friends"])

      expect(JSON(response.body)["places_friends"]).to eq(
        ResponseHelper.object_render(friends.first, 'api/v1/friends/index',
          users_friends: [user_friend], places_friends: [place_friend])["places_friends"])
    end
  end

  context "Find user friends if current user - Place" do
    let!(:friendship) do
      place.friend_request(user_friend); place.friend_request(place_friend)
      user_friend.accept_request(place); place_friend.accept_request(place)
    end

    it "returns current user friends" do
      request.headers["Authorization"] = "Bearer #{place_token}"

      get :index, format: :json

      expect(response.status).to eq 200

      expect(JSON(response.body)["users_friends"]).to eq(
        ResponseHelper.object_render(friends.first, 'api/v1/friends/index',
          users_friends: [user_friend], places_friends: [place_friend])["users_friends"])

      expect(JSON(response.body)["places_friends"]).to eq(
        ResponseHelper.object_render(friends.first, 'api/v1/friends/index',
          users_friends: [user_friend], places_friends: [place_friend])["places_friends"])
    end
  end

  context "Create new friend request if current user - User" do
    it "returns 201 if user became a pending friend" do
      request.headers["Authorization"] = "Bearer #{user_token}"

      post :create,  { id: new_place_friend.id, type: 1, format: :json }

      expect(user.my_pending_friends.first).to eq(new_place_friend)
      expect(response.status).to eq(201)
    end

    context "black list check" do
      let!(:black_list) { create(:black_list, place: new_place_friend, user: user) }

      it "returns 403 if user is in the black list" do
        request.headers["Authorization"] = "Bearer #{user_token}"

        post :create, { id: new_place_friend.id, type: 1, format: :json }

        expect(response.status).to eq(403)
      end
    end
  end

  context "Create new friend request if current user - Place" do
    it "returns 201 if user became a pending friend" do
      request.headers["Authorization"] = "Bearer #{place_token}"

      post :create, { id: new_place_friend.id, type: 1, format: :json }

      expect(place.my_pending_friends.first).to eq(new_place_friend)
      expect(response.status).to eq(201)
    end

    context "black list check" do
      let!(:black_list) { create(:black_list, place: new_place_friend, user: place.user) }

      it "returns 403 if place is in the black list" do
        request.headers["Authorization"] = "Bearer #{place_token}"

        post :create, { id: new_place_friend.id, type: 1, format: :json }

        expect(response.status).to eq(403)
      end
    end
  end

  context "Remove friend if current user - User" do
    let!(:friendship) do
      user.friend_request(user_friend); user.friend_request(place_friend)
      user_friend.accept_request(user); place_friend.accept_request(user)
    end

    it "returns 200 if friend removed from friends list" do
      request.headers["Authorization"] = "Bearer #{user_token}"

      delete :destroy, { id: place_friend.id, type: 1, format: :json }

      expect(place.my_friends.include?(place_friend)).to eq(false)
      expect(response.status).to eq(200)
    end
  end

  context "Remove friend if current user - Place" do
    let!(:friendship) do
      place.friend_request(user_friend); place.friend_request(place_friend)
      user_friend.accept_request(place); place_friend.accept_request(place)
    end

    it "returns 200 if friend removed from friends list" do
      request.headers["Authorization"] = "Bearer #{place_token}"

      delete :destroy, { id: place_friend.id, type: 1, format: :json }

      expect(place.my_friends.include?(place_friend)).to eq(false)
      expect(response.status).to eq(200)
    end
  end
end
