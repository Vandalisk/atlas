require 'rails_helper'

RSpec.describe Api::V1::SearchController, type: :controller do
  context 'auth' do
    let!(:user) { create(:user, latitude: 39.000000, longitude: -76.000000) }
    let!(:token) { Token.generate(user) }
    let!(:category_names) { ["поесть", "бары-клубы", "гостиницы", "турестические развлечения", "супорты"] }
    before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

    let!(:categories) do
      category_names.inject([]) { |memo, title| memo.push(create(:category, title: title)); memo }
    end

    let!(:places) do
      categories.inject([]) do |memo, category|
        memo.push(
          [
            create(:place, category: category,
              user: create(:user, latitude: 39.000200, longitude: -76.000000, owner: true)
            ),
            create(:place, category: category)
          ]
        )
        memo
      end.flatten
    end

    let!(:eat_places) { Place.search_by_category_names(category_names.first) }
    let!(:bars_and_clubs) { Place.search_by_category_names(category_names.second) }
    let!(:not_owners) { User.not_owners }
    let(:all_places) { Place.all }

    let!(:males) do
      [
        create(:user, sex: 'Male'),
        create(:user, sex: 'Male', latitude: 39.000200, longitude: -76.000000)
      ].flatten
    end

    let!(:females) do
      [
        create(:user, sex: 'Female'),
        create(:user, sex: 'Female', latitude: 39.000200, longitude: -76.000000)
      ].flatten
    end

    context 'allowed actions' do
      describe 'GET #index' do
        describe "search places" do
          context "by 'поесть' category without name" do
            let(:valid_params) {{ categories: ['поесть'] }}

            it "code 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "return all nearby places by this category and users" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)[:places]).
                to eq(ResponseHelper.collection_render(nil,
                  "api/v1/search/index", users: [] , places: [eat_places.first])[:places]
                )
            end
          end

          context "by 'поесть' and 'бары-клубы' category without name" do
            let(:valid_params) {{ categories: category_names[0..1] }}

            it "code 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "return all nearby places by these categories and users" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)[:places]).
                to eq(ResponseHelper.collection_render(nil, "api/v1/search/index", users: [],
                  places: [eat_places.first, bars_and_clubs.first])[:places]
                )
            end
          end

          context "by name" do
            let(:valid_params) {{ name: Place.first.name }}

            it "code 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "return places by name and users" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)[:places]).
                to eq(ResponseHelper.collection_render(nil, "api/v1/search/index", users: [],
                  places: [Place.first])[:places]
                )
            end
          end

          context "by 'поесть' and name" do
            let(:valid_params) {{ name: Place.first.name, category: 'поесть' }}

            it "code 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "return all places by this category and this name" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)[:places]).
                to eq(ResponseHelper.collection_render(nil, "api/v1/search/index", users: [],
                  places: [Place.first])[:places]
                )
            end
          end
        end

        describe "search users" do
          context "by 'Male' sex without name" do
            let(:valid_params) {{ sex: 'Male' }}

            it "code 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "return male users nearby and places" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)[:users]).
                to eq(ResponseHelper.collection_render(nil,
                  "api/v1/search/index", users: [males.second], places: [])[:users]
                )
            end
          end

          context "by 'Female' sex without name" do
            let(:valid_params) {{ sex: 'Female' }}

            it "code 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "return male users nearby and places" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)[:users]).
                to eq(ResponseHelper.collection_render(nil,
                  "api/v1/search/index", users: [females.second], places: [])[:users]
                )
            end
          end

          context "by name without sex" do
            let(:valid_params) {{ name: User.last.first_name }}

            it "code 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "return all users by name and places" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)[:users]).
                to eq(ResponseHelper.collection_render(nil,
                  "api/v1/search/index", users: [User.last], places: [])[:users]
                )
            end
          end

          context "by name and sex" do
            let(:valid_params) {{ name: females.last.first_name, sex: females.last }}

            it "code 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "return all users by name with this name and places" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)[:users]).
                to eq(ResponseHelper.collection_render(nil,
                  "api/v1/search/index", users: [females.last], places: [])[:users]
                )
            end
          end
        end
      end
    end

    context 'forbidden actions' do
      describe 'GET #index' do
      end
    end
  end

  context 'without auth' do
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) { request.headers['Authorization'] = "Bearer #{invalid_token}" }
      let(:valid_attributes) {{}}

    describe 'GET #index' do
      let(:valid_attributes) {{}}

      it 'return 403' do
        get :index, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq 403
      end
    end
  end
end
