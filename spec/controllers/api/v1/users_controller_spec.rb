require 'rails_helper'

describe Api::V1::UsersController, type: :controller do
  let!(:user) { create(:user) }
  let!(:another_social_marker) do
    {
      social_marker: (User.social_markers.values - [User.social_markers[user.social_marker]]).sample
    }
  end
  let(:user_second_account) do
    attributes = user.attributes.except("id", "cretead_at", "updated_at")
    attributes.merge!(another_social_marker)
    User.create(attributes)
  end
  let!(:attributes) { attributes_for(:user, :registration_attributes) }
  let!(:user_token) { Token.generate(user) }

  describe "Find user" do
    context "Finding user by find params" do
      let!(:token) { Token.generate(user) }
      let!(:exist_user_find_params) do
        {
          social_id: user.social_id,
          social_marker: User.social_markers[user.social_marker],
          phone_number: user.phone_number
        }
      end

      it "returns user first account if get params for this account" do
        get :index, { user: exist_user_find_params, format: :json }

        expect(JSON(response.body)['user']['token']).to eq token
        expect(response.status).to eq 200
      end

      let!(:exist_user_find_params_with_another_social_marker_and_without_phone) do
        {
          social_id: user.social_id,
          social_marker: User.social_markers[user_second_account.social_marker],
        }.merge(social_id_for_digits(user_second_account))
      end

      it "returns user first account if get params for second account" do
        get :index, { user: exist_user_find_params_with_another_social_marker_and_without_phone, format: :json }

        expect(JSON(response.body)['user']['token']).to eq Token.generate(user_second_account)
        expect(response.status).to eq 200
      end

      let!(:user_find_params) { attributes_for(:user, :find_params) }

      it "returns 404 if user doesn't exist into db" do
        get :index, { user: user_find_params, format: :json }

        expect(response.status).to eq 404
      end

      describe 'dog-nail search' do
        context 'by token' do
          let(:attrs) { User::JSON_ATTRS.map(&:to_s) }
          before { user_second_account.destroy }

          it 'returns user' do
            request.headers["Authorization"] = "Bearer #{user_token}"
            get :index, ResponseHelper.add_json_format_to({})

            expect(ResponseHelper.get_parsed_body(response)['user']).
              to eq(ResponseHelper.expected_item(user, attrs))
          end
        end

        context 'by first and last name' do
          let(:valid_attributes) {{ first_name: user.first_name, last_name: user.last_name}}
          let(:attrs) { User::JSON_ATTRS.map(&:to_s) }
          before { user_second_account.destroy }
          it 'returns user' do
            request.headers["Authorization"] = "Bearer #{user_token}"
            get :index, ResponseHelper.add_json_format_to(valid_attributes)

            expect(ResponseHelper.get_parsed_body(response)['user']).
              to eq(ResponseHelper.expected_item(user, attrs))
          end
        end

        context 'by phone number' do
          let(:valid_attributes) {{ phone_number: user.phone_number }}
          let(:attrs) { User::JSON_ATTRS.map(&:to_s) }
          before { user_second_account.destroy }

          it 'returns user' do
            request.headers["Authorization"] = "Bearer #{user_token}"
            get :index, ResponseHelper.add_json_format_to(valid_attributes)

            expect(ResponseHelper.get_parsed_body(response)['user']).
              to eq(ResponseHelper.expected_item(user, attrs))
          end
        end
      end
    end
  end

  describe 'GET #show' do
    context 'get user' do
      let(:valid_attributes) {{ id: user.id }}
      let(:attrs) { User::JSON_ATTRS.map(&:to_s) }

      it 'returns user' do
        get :show, ResponseHelper.add_json_format_to(valid_attributes)

        expect(ResponseHelper.get_parsed_body(response)['user']).
          to eq(ResponseHelper.expected_item(user, attrs))
      end
    end
  end

  describe "Create new user" do
    context "Creating user with valid and invalid attributes" do
      it 'returns user token' do
        post :create, { user: attributes, format: :json }

        expect(JSON(response.body)['user']['token']).to eq Token.generate(User.last)
        expect(response.status).to eq 201
      end

      context 'social marker digits' do
        let(:attributes1) { attributes_for(:user, :registration_attributes, social_marker: 3) }
        let(:attributes2) { attributes_for(:user, :registration_attributes, social_marker: 3) }

        it 'multiple users' do
          expect {
            post :create, { user: attributes1, format: :json }
            post :create, { user: attributes2, format: :json }
          }.to change{ User.count }.by(2)
        end

        it 'social marker equal phone number' do
          post :create, { user: attributes1, format: :json }
          post :create, { user: attributes2, format: :json }

          users = User.last(2)

          expect(users[0].social_id).to eq(users[0].phone_number)
          expect(users[1].social_id).to eq(users[1].phone_number)
        end
      end

      let!(:invalid_attributes) { attributes_for(:user, :invalid_phone_number) }
      let!(:errors) { User.create(invalid_attributes).errors.full_messages }

      it "returns errors messages and status 400 when attributes are nil or invalid" do
        post :create, { user: invalid_attributes, format: :json }

        expect(JSON(response.body)["errors"]).to eq errors
        expect(response.status).to eq 400
      end
    end

    context "Creating user with not uniq attributes" do
      let!(:not_uniq_with_phone_number) do
        attributes.merge(phone_number: user.phone_number, social_marker: user.social_marker)
      end
      let!(:not_uniq_with_phone_number_errors) do
        User.create(not_uniq_with_phone_number).errors.full_messages
      end

      it "returns errors messages and status 400 when phone_number & social_marker not uniq" do
        post :create, { user: not_uniq_with_phone_number, format: :json }

        expect(JSON(response.body)["errors"]).to eq not_uniq_with_phone_number_errors
        expect(response.status).to eq 400
      end

      let!(:not_uniq_with_social_id) do
        attributes.merge(phone_number: user.phone_number, social_marker: user.social_marker)
      end
      let!(:not_uniq_with_social_id_errors) do
        User.create(not_uniq_with_phone_number).errors.full_messages
      end

      it "returns errors messages and status 400 when social_id & social_marker not uniq" do
        post :create, { user: not_uniq_with_social_id, format: :json }

        expect(JSON(response.body)["errors"]).to eq not_uniq_with_social_id_errors
        expect(response.status).to eq 400
      end
    end
  end

  context "DELETE #destroy" do
    context 'without auth' do
      describe 'allowed actions' do
        describe 'by phone number and social marker' do
          let!(:user2) do
            create(:user,
              phone_number: user.phone_number,
              social_marker: ((0..3).to_a - [User.social_markers[user.social_marker]]).sample
            )
          end

          let(:valid_attributes) do
            {
              mobile_test: true,
              phone_number: user.phone_number,
            }
          end

          it 'status 200' do
            delete :destroy, valid_attributes.merge(format: :json)

            expect(response.status).to eq(200)
          end

          it 'count -2' do
            expect { delete :destroy, valid_attributes.merge(format: :json) }.to change(User, :count).by(-2)
          end

          it 'successful message' do
            delete :destroy, valid_attributes.merge(format: :json)

            expect(ResponseHelper.get_parsed_body(response)['message']).
              to eq("Successfully deleted all accounts by this phone number")
          end
        end
      end

      describe 'forbidden actions' do
        describe 'by phone number nil' do
          let(:valid_attributes) {{ mobile_test: true }}

          it 'status 400' do
            delete :destroy, valid_attributes.merge(format: :json)

            expect(response.status).to eq(400)
          end

          it "count didn't change" do
            expect { delete :destroy, valid_attributes.merge(format: :json) }.to change(User, :count).by(0)
          end
        end

        describe 'no such user in database' do
          let(:virtual_user) { build(:user) }
          let(:valid_attributes) do
            {
              mobile_test: true,
              phone_number: virtual_user.phone_number,
              social_marker: User.social_markers[virtual_user.social_marker]
            }
          end

          it 'status 404' do
            delete :destroy, valid_attributes.merge(format: :json)

            expect(response.status).to eq(404)
          end

          it "count didn't change" do
            expect { delete :destroy, valid_attributes.merge(format: :json) }.to change(User, :count).by(0)
          end
        end
      end
    end
  end

  def social_id_for_digits(user)
    if User.social_markers[user.social_marker].equal?(3)
      { social_id: user.phone_number }
    else
      { social_id: user.social_id }
    end
  end
end
