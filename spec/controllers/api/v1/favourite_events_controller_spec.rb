require 'rails_helper'

RSpec.describe Api::V1::FavouriteEventsController, type: :controller do
  render_views

  context 'auth' do
    let!(:user) { create(:user) }
    let!(:place) { create(:place) }
    let!(:favourite_events) { create_list(:event, 3) }

    context 'as a user' do
      let!(:token) { Token.generate(user) }

      before(:each) do
        request.headers['Authorization'] = "Bearer #{token}"
        user.favourite_events << favourite_events
      end

      context 'allowed actions' do
        describe 'GET #index' do
          it "Should return all favourite events of the user" do
            get :index, format: :json

            expect(ResponseHelper.get_parsed_body(response)).
              to eq(ResponseHelper.object_render(favourite_events, 'api/v1/favourite_events/index'))
          end
        end

        describe 'POST #create' do
          let(:event) { create(:event) }
          let(:valid_params) {{ id: event.id }}

          it "code 201" do
            get :create, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq(201)
          end

          it "Should add favourite event to user's favourite_events" do
            get :create, ResponseHelper.add_json_format_to(valid_params)

            expect(user.favourite_events.last).to eq(event)
          end
        end
      end
    end

    context 'as a place' do
      let!(:token) { Token.generate(place) }

      before(:each) do
        request.headers['Authorization'] = "Bearer #{token}"
        place.user.favourite_events << favourite_events
      end

      context 'forbidden actions' do
        describe 'GET #index' do
          it "Place can't retrieve favourite events." do
            get :index, format: :json

            expect(response.status).to eq(403)
          end
        end
      end
    end
  end

  context 'without auth' do
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) { request.headers['Authorization'] = "Bearer #{invalid_token}" }

    describe 'GET #index' do
      it "can't retrieve events" do
        get :index, format: :json

        expect(response.status).to eq(403)
      end
    end
  end
end
