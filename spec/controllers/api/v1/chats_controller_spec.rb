require 'rails_helper'

RSpec.describe Api::V1::ChatsController, type: :controller do
  context 'auth' do
    context 'signed in as a user' do
      let!(:sender) { create(:user) }

      let!(:token) { Token.generate(sender) }
      before(:each) { request.headers["Authorization"] = "Bearer #{token}" }

      context "allowed actions" do
        describe "GET chats" do
          describe "where user is a participant" do
            let!(:recipient) { create(:user) }
            let!(:recipient2) { create(:user) }
            let!(:chats) do
              [
                create(:chat, sender: sender, recipient: recipient),
                create(:chat, sender: sender, recipient: recipient2)
              ]
            end

            let!(:messages) do
              create_list(:message, 3, chat: chats.first, sender: sender) +
              create_list(:message, 3, chat: chats.last, sender: sender)
            end

            let(:valid_params) { Hash.new }

            it "returns chats and status 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq 200
              expect(ResponseHelper.get_parsed_body(response))
                .to eq(
                  JSON(
                    RablRails.render(
                      chats,
                      "api/v1/chats/index",
                      locals: { chats_counter: chats.count, chats: chats.reverse, current_user: sender }
                    )
                  ).first
                )
            end
          end
        end

        describe "GET chat" do
          let!(:recipient) { create(:user) }
          let!(:chat) { create(:chat, sender: sender, recipient: recipient) }

          describe "by id" do
            let(:valid_params) { { id: chat.id } }

            it "status 200" do
              get :show, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "returns chat by valid id" do
              get :show, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)).
                to eq(JSON RablRails.render(chat, "api/v1/chats/show", locals: { current_user: sender }))
            end
          end
        end

        describe "POST chat" do
          context "between users" do
            let!(:recipient) { create(:user) }
            let(:valid_params) do
              {
                chat:
                  {
                    recipient_phone_number: recipient.phone_number,
                    body: "Hasta la vista, baby"
                  }
              }
            end

            it "returns chat json and status 201" do
              post :create, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq 201
              expect(ResponseHelper.get_parsed_body(response))
              .to eq(JSON(RablRails.render(Chat.last, "api/v1/chats/create", locals: { current_user: sender })))
            end

            it 'count +1' do
              expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
              .to change{ Chat.count }.by(1)
            end
          end

          context "between user and place" do
            let!(:recipient) { create(:place) }
            let(:valid_params) do
              {
                chat:
                  {
                    recipient_phone_number: recipient.phone_number,
                    body: "Hasta la vista, baby"
                  }
              }
            end

            it "returns chat json and status 201" do
              post :create, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq 201
              expect(ResponseHelper.get_parsed_body(response))
              .to eq(JSON(RablRails.render(Chat.last, "api/v1/chats/create", locals: { current_user: sender })))
            end

            it 'count +1' do
              expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
                .to change{ Chat.count }.by(1)
            end
          end

          describe "Users are friends and account is private" do
            let!(:sender) { create(:user, private: true) }

            let!(:token) { Token.generate(sender) }
            before(:each) { request.headers["Authorization"] = "Bearer #{token}" }

            context "between users" do
              let!(:recipient) { create(:user) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              let!(:friendship) do
                sender.friend_request(recipient); sender.friend_request(recipient)
                recipient.accept_request(sender); recipient.accept_request(sender)
              end

              it "returns chat json and status 201" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 201
                expect(ResponseHelper.get_parsed_body(response))
                .to eq(JSON(RablRails.render(Chat.last, "api/v1/chats/create", locals: { current_user: sender })))
              end

              it 'count +1' do
                expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
                .to change{ Chat.count }.by(1)
              end
            end

            context "between user and place" do
              let!(:recipient) { create(:place) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              let!(:friendship) do
                sender.friend_request(recipient); sender.friend_request(recipient)
                recipient.accept_request(sender); recipient.accept_request(sender)
              end

              it "returns chat json and status 201" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 201
                expect(ResponseHelper.get_parsed_body(response))
                .to eq(JSON(RablRails.render(Chat.last, "api/v1/chats/create", locals: { current_user: sender })))
              end

              it 'count +1' do
                expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
                  .to change{ Chat.count }.by(1)
              end
            end
          end
        end
      end

      context "forbidden actions" do
        describe "GET chat" do
          let!(:recipient) { create(:user) }
          let!(:chat) { create(:chat, sender: sender, recipient: recipient) }

          describe "by id" do
            let(:invalid_params) { { id: rand(50..100) } }

            it "status 404" do
              get :show, ResponseHelper.add_json_format_to(invalid_params)

              expect(response.status).to eq(404)
            end
          end
        end

        describe "POST chat" do
          let!(:sender) { create(:user) }

          let!(:token) { Token.generate(sender) }
          before(:each) { request.headers["Authorization"] = "Bearer #{token}" }

          describe "Only friends can have chat if private account." do
            context "users are not friends" do
              let!(:recipient) { create(:user, private: true) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              it "returns status 403" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 403
              end
            end

            context "place blocked user" do
              let!(:recipient) { create(:place) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              let!(:black_list) { create(:black_list, place: recipient, user: sender) }

              it "returns status 403" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 403
              end
            end
          end
        end
      end
    end

    context 'signed in as a place' do
      let!(:sender) { create(:place) }
      let!(:token) { Token.generate(sender) }

      before(:each) { request.headers["Authorization"] = "Bearer #{token}" }

      context "allowed actions" do
        describe "GET chats" do
          let!(:recipient) { create(:user) }
          let!(:recipient2) { create(:user) }

          describe "where place is a participant" do
            let!(:chats) do
              [
                create(:chat, sender: sender.user, recipient: recipient),
                create(:chat, sender: sender.user, recipient: recipient2)
              ]
            end

            let!(:messages) do
              create_list(:message, 3, chat: chats.first, sender: sender.user) +
              create_list(:message, 3, chat: chats.last, sender: sender.user)
            end

            let(:valid_params) { Hash.new }

            it "returns chats and status 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq 200
              expect(ResponseHelper.get_parsed_body(response))
                .to eq(
                  JSON(
                    RablRails.render(
                      chats,
                      "api/v1/chats/index",
                      locals: { chats_counter: chats.count, chats: chats.reverse, current_user: sender.user }
                    )
                  ).first
                )
            end
          end
        end

        describe "GET chat" do
          let!(:recipient) { create(:user) }
          let!(:chat) { create(:chat, sender: sender.user, recipient: recipient) }

          describe "by id" do
            let(:valid_params) { { id: chat.id } }

            it "status 200" do
              get :show, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "returns chat by valid id" do
              get :show, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)).
                to eq(JSON RablRails.render(chat, "api/v1/chats/show", locals: { current_user: sender.user }))
            end
          end
        end

        describe "POST chat" do
          describe "Account is not private" do
            context "between places" do
              let!(:recipient) { create(:place) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              it "returns chat json and status 201" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 201
                expect(ResponseHelper.get_parsed_body(response))
                .to eq(JSON(RablRails.render(Chat.last, "api/v1/chats/create", locals: { current_user: sender.user })))

                expect(
                  ResponseHelper.get_parsed_body(
                    response
                  )["chat"]["last_message"]["sender"]["description"]
                ).to eq(sender.description)
              end

              it "count +1" do
                expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
                  .to change{ Chat.count }.by(1)
              end
            end

            context "between place and user" do
              let!(:recipient) { create(:user) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              it "returns chat json and status 201" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 201
                expect(ResponseHelper.get_parsed_body(response))
                .to eq(JSON(RablRails.render(Chat.last, "api/v1/chats/create", locals: { current_user: sender.user })))

                expect(
                  ResponseHelper.get_parsed_body(
                    response
                  )["chat"]["last_message"]["sender"]["description"]
                ).to eq(sender.description)
              end

              it 'count +1' do
                expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
                  .to change{ Chat.count }.by(1)
              end
            end
          end

          describe "Places are friends and account is private" do
            let!(:sender) { create(:place, user: create(:user, owner: true, private: true)) }

            let!(:token) { Token.generate(sender) }
            before(:each) { request.headers["Authorization"] = "Bearer #{token}" }

            context "between places" do
              let!(:recipient) { create(:place) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              let!(:friendship) do
                sender.friend_request(recipient); sender.friend_request(recipient)
                recipient.accept_request(sender); recipient.accept_request(sender)
              end

              it "returns chat json and status 201" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 201
                expect(ResponseHelper.get_parsed_body(response))
                .to eq(JSON(RablRails.render(Chat.last, "api/v1/chats/create", locals: { current_user: sender.user })))

                expect(
                  ResponseHelper.get_parsed_body(
                    response
                  )["chat"]["last_message"]["sender"]["description"]
                ).to eq(sender.description)
              end

              it "count +1" do
                expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
                  .to change{ Chat.count }.by(1)
              end
            end

            context "between place and user" do
              let!(:recipient) { create(:user) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              let!(:friendship) do
                sender.friend_request(recipient); sender.friend_request(recipient)
                recipient.accept_request(sender); recipient.accept_request(sender)
              end

              it "returns chat json and status 201" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 201
                expect(ResponseHelper.get_parsed_body(response))
                .to eq(JSON(RablRails.render(Chat.last, "api/v1/chats/create", locals: { current_user: sender.user })))

                expect(
                  ResponseHelper.get_parsed_body(
                    response
                  )["chat"]["last_message"]["sender"]["description"]
                ).to eq(sender.description)
              end

              it 'count +1' do
                expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
                  .to change{ Chat.count }.by(1)
              end
            end
          end
        end
      end

      context "forbidden actions" do
        describe "POST chat" do
          describe "Place wouldn't have chat with user from black list." do
            let!(:sender) { create(:place, user: create(:user, owner: true)) }
            let!(:token) { Token.generate(sender) }

            before(:each) { request.headers["Authorization"] = "Bearer #{token}" }

            context "place is in place's black list" do
              let!(:recipient) { create(:place) }
              let!(:black_list) { create(:black_list, place: recipient, user: sender.user) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              it "returns status 403" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 403
              end
            end

            context "place and user are not friends" do
              let!(:recipient) { create(:user, private: true) }
              let(:valid_params) do
                {
                  chat:
                    {
                      recipient_phone_number: recipient.phone_number,
                      body: "Hasta la vista, baby"
                    }
                }
              end

              it "returns status 403" do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq 403
              end
            end
          end
        end
      end
    end
  end

  context "without auth" do
    let!(:user) { create(:user) }
    let!(:chat) { create(:chat, sender: user) }
    let!(:messages) { create_list(:message, 2, chat: chat, sender: user) }
    let!(:invalid_token) { Faker::Code.ean }
    before(:each) { request.headers["Authorization"] = "Bearer #{invalid_token}" }

    describe "GET chats" do
      let(:valid_params) { Hash.new }

      it "status 403" do
        get :index, ResponseHelper.add_json_format_to(valid_params)

        expect(response.status).to eq 403
      end
    end

    describe "POST chat" do
      let(:chat_params) {{ name: Faker::Lorem.word }}
      let(:valid_params) { chat_params.merge(format: :json) }

      it "status 403" do
        post :create, valid_params
        expect(response.status).to eq 403
      end
    end
  end
end
