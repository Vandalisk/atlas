require 'rails_helper'

describe Api::V1::PlacesController, type: :controller do
  let(:place) { create(:place) }
  let(:user) { create(:user, owner: true, place: place) }
  let(:another_social_marker) do
    {
      "social_marker" => (User.social_markers.values - [User.social_markers[place.social_marker]]).sample
    }
  end
  let(:place_second_account) do
    attributes = place.user.attributes.except("id", "cretead_at", "updated_at")
    attributes.merge!(another_social_marker)
    create(:place, user: User.create(attributes))
  end
  let!(:attributes) { attributes_for(:place, :place_registration_attributes) }
  let!(:place_token) { Token.generate(place) }

  describe "Find place" do
    context "Finding place by find params" do
      let!(:exist_user_find_params) do
        {
          social_id: user.social_id,
          social_marker: User.social_markers[user.social_marker],
          phone_number: user.phone_number
        }
      end

      it "returns place first account if get params for this account" do
        get :index, { place: exist_user_find_params, format: :json }

        expect(JSON(response.body)['place']['token']).to eq Token.generate(place)
        expect(response.status).to eq 200
      end

      let!(:exist_user_find_params_with_another_social_marker_and_without_phone) do
        {
          social_id: place.social_id,
          social_marker: User.social_markers[place_second_account.social_marker],
        }.merge(social_id_for_digits(place_second_account))
      end

      it "returns place first account if get params for second account" do
        get :index, { place: exist_user_find_params_with_another_social_marker_and_without_phone, format: :json }

        expect(JSON(response.body)['place']['token']).to eq Token.generate(place_second_account)
        expect(response.status).to eq 200
      end

      let!(:user_find_params) { attributes_for(:user, :find_params) }

      it "returns 404 if place doesn't exist into db" do
        get :index, { place: user_find_params, format: :json }

        expect(response.status).to eq 404
      end
    end

    describe "search" do
      context 'by name' do
        let(:valid_attributes) {{ name: place.name }}
        let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }

        it 'returns place' do
          request.headers["Authorization"] = "Bearer #{place_token}"
          get :index, ResponseHelper.add_json_format_to(valid_attributes)

          expect(ResponseHelper.get_parsed_body(response)).
            to eq JSON(RablRails.render(place, 'api/v1/places/item'))
        end
      end

      context "by description" do
        let(:valid_attributes) {{ description: place.description }}
        let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }

        it 'returns place' do
          request.headers["Authorization"] = "Bearer #{place_token}"
          get :index, ResponseHelper.add_json_format_to(valid_attributes)

          expect(ResponseHelper.get_parsed_body(response)).
            to eq JSON(RablRails.render(place, 'api/v1/places/item'))
        end
      end

      context 'by phone number' do
        let(:valid_attributes) {{ phone_number: place.phone_number }}
        let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }

        it "returns place" do
          request.headers["Authorization"] = "Bearer #{place_token}"
          get :index, ResponseHelper.add_json_format_to(valid_attributes)

          expect(ResponseHelper.get_parsed_body(response)).
            to eq JSON(RablRails.render(place, 'api/v1/places/item'))
        end
      end

      context 'by token' do
        let(:valid_attributes) {{ phone_number: place.phone_number }}
        let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }

        it "returns place" do
          request.headers["Authorization"] = "Bearer #{place_token}"
          get :index, ResponseHelper.add_json_format_to(valid_attributes)

          expect(ResponseHelper.get_parsed_body(response)).
            to eq JSON(RablRails.render(place, 'api/v1/places/item'))
        end
      end
    end
  end

  describe 'GET #show' do
    context 'get place' do
      let(:valid_attributes) {{ id: place.id }}
      let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }

      it 'returns place' do
        get :show, ResponseHelper.add_json_format_to(valid_attributes)

        expect(ResponseHelper.get_parsed_body(response)).
          to eq JSON(RablRails.render(place, 'api/v1/places/item'))
      end
    end
  end

  describe "GET #show" do
    context 'allowed actions' do
      let(:valid_attributes) {{ id: place.id }}
      let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }

      it 'status 200' do
        get :show, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq 200
      end

      it 'returns place' do
        get :show, ResponseHelper.add_json_format_to(valid_attributes)

        expect(ResponseHelper.get_parsed_body(response)).
          to eq JSON(RablRails.render(place, 'api/v1/places/item'))
      end
    end

    context 'forbidden actions' do
      describe "place doensn't exist in database" do
        let(:invalid_attributes) {{ id: rand(1..10) }}

        it 'status 404' do
          get :show, ResponseHelper.add_json_format_to(invalid_attributes)

          expect(response.status).to eq 404
        end
      end
    end
  end

  describe "Create new place" do
    context "Creating place with valid and invalid attributes" do
      it 'returns place token' do
        post :create, { place: attributes, format: :json }

        expect(JSON(response.body)['place']['token']).to eq Token.generate(Place.last)
        expect(response.status).to eq 201
      end

      let!(:invalid_attributes) do
        attributes = attributes_for(:place, :invalid_user);
        attributes[:user_attributes] = attributes[:user].attributes
        attributes
      end

      let!(:errors) do
        Place.create(invalid_attributes).errors.full_messages
      end

      it "returns errors messages and status 400 when attributes are nil or invalid" do
        post :create, { place: invalid_attributes, format: :json }

        expect(JSON(response.body)["errors"]).to eq errors
        expect(response.status).to eq 400
      end
    end

    context "Creating place with not uniq attributes" do
      let!(:not_uniq_with_phone_number) do
        { phone_number: user.phone_number, social_marker: user.social_marker }
      end

      let(:invalid_attributes) { attributes.deep_merge(user_attributes: not_uniq_with_phone_number) }

      let!(:not_uniq_with_phone_number_errors) do
        [ Place.create(invalid_attributes).errors.full_messages.first ]
      end

      it "returns errors messages and status 400 when phone_number & social_marker not uniq" do
        post :create, { place: invalid_attributes, format: :json }

        expect(JSON(response.body)["errors"]).to eq not_uniq_with_phone_number_errors
        expect(response.status).to eq 400
      end

      let!(:not_uniq_with_social_id) do
        { phone_number: user.phone_number, social_marker: user.social_marker }
      end

      let(:invalid_attributes) { attributes.deep_merge(user_attributes: not_uniq_with_social_id) }

      let!(:not_uniq_with_social_id_errors) do
        [ Place.create(invalid_attributes).errors.full_messages.first ]
      end

      it "returns errors messages and status 400 when social_id & social_marker not uniq" do
        post :create, { place: invalid_attributes, format: :json }

        expect(JSON(response.body)["errors"]).to eq not_uniq_with_social_id_errors
        expect(response.status).to eq 400
      end
    end
  end

  describe "PUT #update" do
    describe "allowed actions" do
      let(:valid_attributes) do
        { place: attributes_for(:place, :place_registration_attributes), id: place.id }
      end

      before(:each) { request.headers["Authorization"] = "Bearer #{place_token}" }

      describe "owner can update place" do
        it "status 200" do
          put :update, ResponseHelper.add_json_format_to(valid_attributes)

          expect(response.status).to eq(200)
        end
      end
    end

    describe "forbidden actions" do
      let(:place2) { create(:place) }
      let(:valid_attributes) do
        { place: attributes_for(:place, :place_registration_attributes), id: place2.id }
      end

      describe "not owner can't update place" do
        it "status 403" do
          put :update, ResponseHelper.add_json_format_to(valid_attributes)

          expect(response.status).to eq(403)
        end
      end

      describe "invalid place attributes" do
        #TODO: Add specs
      end

      describe "invalid user attributes" do
        #TODO: Add specs
      end
    end
  end

  def social_id_for_digits(user)
    if User.social_markers[user.social_marker].equal?(3)
      { social_id: user.phone_number }
    else
      { social_id: user.social_id }
    end
  end
end
