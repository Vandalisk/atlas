require 'rails_helper'

RSpec.describe Api::V1::ViolationsController, type: :controller do
  context 'with auth' do
    context 'user login' do
      let(:user) { create(:user, latitude: 55.765494, longitude: 49.149921) }
      let(:token) { Token.generate(user) }
      before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

      context 'allowed actions' do
        let!(:violation) { create(:violation) }

        describe 'GET #index' do
          let(:attrs) { Violation::JSON_ATTRS.map(&:to_s) }

          context 'should get violations by latitude and longitude' do
            let(:close_point) {{ latitude: 55.765591, longitude: 49.149769 }}
            let(:far_point) {{ latitude: 50.768591, longitude: 40.165269 }}
            let!(:close_violations) { create_list(:violation, 3, close_point) }
            let!(:far_violations) { create_list(:violation, 3, far_point) }
            let(:valid_attributes) {{}}

            it 'returns violations' do
              get :index, ResponseHelper.add_json_format_to(valid_attributes)

              expect(ResponseHelper.get_parsed_body(response)['violations']).
                to eq ResponseHelper.expected_collection(close_violations, attrs)
            end
          end
        end

        describe 'GET #show' do
          let(:valid_attributes) {{ id: violation.id }}
          let(:attrs) { Violation::JSON_ATTRS.map(&:to_s) }

          context 'should get violation' do
            it 'returns violation' do
              get :show, ResponseHelper.add_json_format_to(valid_attributes)

              expect(ResponseHelper.get_parsed_body(response)).
                to eq JSON(RablRails.render(violation, 'api/v1/violations/show'))
            end
          end
        end

        describe 'POST #create' do
          let(:valid_attributes) {{ violation: attributes_for(:violation) }}

          context 'user should create violation' do
            it 'code 201' do
              post :create, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq(201)
            end

            it 'count +1' do
              expect { post :create, ResponseHelper.add_json_format_to(valid_attributes) }
                .to change{ Violation.count }.by(1)
            end
          end
        end

        describe 'DELETE #destroy' do
          let(:valid_attributes) {{ id: violation.id }}

          context 'user should create violation' do
            it 'code 200' do
              delete :destroy, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq(200)
            end

            it 'count +1' do
              expect { delete :destroy, ResponseHelper.add_json_format_to(valid_attributes) }
                .to change{ Violation.count }.by(-1)
            end
          end
        end
      end

      context 'forbidden actions' do
        describe 'GET #show' do
          let(:invalid_attributes) {{ id: rand(100) }}

          it "doesn't exist in database" do
            get :show, ResponseHelper.add_json_format_to(invalid_attributes)

            expect(response.status).to eq(404)
          end
        end

        describe 'POST #create' do
          describe 'invalid params' do
            context 'title nil' do
              let(:invalid_attributes) {{ violation: attributes_for(:violation, title: nil) }}

              let(:error_message) do
                violation = Violation.new(invalid_attributes[:violation])
                violation.validate
                violation.errors.full_messages.first
              end

              before(:each) { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }

              it '400 code' do
                expect(response.status).to eq(400)
              end

              it 'error message' do
                expect(ResponseHelper.get_parsed_body(response)['errors'].first).to eq(error_message)
              end
            end

            context 'description nil' do
              let(:invalid_attributes) {{ violation: attributes_for(:violation, description: nil) }}

              let(:error_message) do
                violation = Violation.new(invalid_attributes[:violation])
                violation.validate
                violation.errors.full_messages.first
              end

              before(:each) { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }

              it '400 code' do
                post :create, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'error message' do
                expect(ResponseHelper.get_parsed_body(response)['errors'].first).to eq(error_message)
              end
            end

            context 'description and title nil' do
              let(:invalid_attributes) {{ violation: attributes_for(:violation, title: nil, description: nil) }}

              let(:error_messages) do
                violation = Violation.new(invalid_attributes[:violation])
                violation.validate
                violation.errors.full_messages
              end

              before(:each) { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }

              it '400 code' do
                post :create, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'error messages' do
                expect(ResponseHelper.get_parsed_body(response)['errors']).to eq(error_messages)
              end
            end
          end
        end

        describe 'DELETE #destroy' do
          let(:invalid_attributes) {{ id: rand(100) }}

          it "doesn't exist in database" do
            delete :destroy, ResponseHelper.add_json_format_to(invalid_attributes)

            expect(response.status).to eq(404)
          end
        end
      end
    end

    context 'place login' do
      let!(:place) { create(:user, latitude: 55.765494, longitude: 49.149921) }
      let(:token) { Token.generate(place) }
      before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

      context 'allowed actions' do
        let!(:violation) { create(:violation) }

        describe 'GET #index' do
          let(:attrs) { Violation::JSON_ATTRS.map(&:to_s) }

          context 'should get violations by latitude and longitude' do
            let(:close_point) {{ latitude: 55.765591, longitude: 49.149769 }}
            let(:far_point) {{ latitude: 50.768591, longitude: 40.165269 }}
            let!(:close_violations) { create_list(:violation, 3, close_point) }
            let!(:far_violations) { create_list(:violation, 3, far_point) }
            let(:valid_attributes) {{ latitude: 55.765494, longitude: 49.149921 }}

            it 'returns violations' do
              get :index, ResponseHelper.add_json_format_to(valid_attributes)

              expect(ResponseHelper.get_parsed_body(response)['violations']).
                to eq ResponseHelper.expected_collection(close_violations, attrs)
            end
          end
        end

        describe 'GET #show' do
          let(:valid_attributes) {{ id: violation.id }}
          let(:attrs) { Violation::JSON_ATTRS.map(&:to_s) }

          context 'should get violation' do
            it 'returns violation' do
              get :show, ResponseHelper.add_json_format_to(valid_attributes)

              expect(ResponseHelper.get_parsed_body(response)['violation']).
                to eq ResponseHelper.expected_item(violation, attrs)
            end
          end
        end

        describe 'POST #create' do
          let(:valid_attributes) {{ violation: attributes_for(:violation) }}

          context 'user should create violation' do
            it 'code 201' do
              post :create, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq(201)
            end

            it 'count +1' do
              expect { post :create, ResponseHelper.add_json_format_to(valid_attributes) }
                .to change{ Violation.count }.by(1)
            end
          end
        end

        describe 'DELETE #destroy' do
          let(:valid_attributes) {{ id: violation.id }}

          context 'user should create violation' do
            it 'code 200' do
              delete :destroy, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq(200)
            end

            it 'count +1' do
              expect { delete :destroy, ResponseHelper.add_json_format_to(valid_attributes) }
                .to change{ Violation.count }.by(-1)
            end
          end
        end
      end

      context 'forbidden actions' do
        describe 'GET #show' do
          let(:invalid_attributes) {{ id: rand(100) }}

          it "doesn't exist in database" do
            get :show, ResponseHelper.add_json_format_to(invalid_attributes)

            expect(response.status).to eq(404)
          end
        end

        describe 'POST #create' do
          describe 'invalid params' do
            context 'title nil' do
              let(:invalid_attributes) {{ violation: attributes_for(:violation, title: nil) }}

              let(:error_message) do
                violation = Violation.new(invalid_attributes[:violation])
                violation.validate
                violation.errors.full_messages.first
              end

              before(:each) { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }

              it '400 code' do
                expect(response.status).to eq(400)
              end

              it 'error message' do
                expect(ResponseHelper.get_parsed_body(response)['errors'].first).to eq(error_message)
              end
            end

            context 'description nil' do
              let(:invalid_attributes) {{ violation: attributes_for(:violation, description: nil) }}

              let(:error_message) do
                violation = Violation.new(invalid_attributes[:violation])
                violation.validate
                violation.errors.full_messages.first
              end

              before(:each) { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }

              it '400 code' do
                post :create, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'error message' do
                expect(ResponseHelper.get_parsed_body(response)['errors'].first).to eq(error_message)
              end
            end

            context 'description and title nil' do
              let(:invalid_attributes) {{ violation: attributes_for(:violation, title: nil, description: nil) }}

              let(:error_messages) do
                violation = Violation.new(invalid_attributes[:violation])
                violation.validate
                violation.errors.full_messages
              end

              before(:each) { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }

              it '400 code' do
                post :create, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'error messages' do
                expect(ResponseHelper.get_parsed_body(response)['errors']).to eq(error_messages)
              end
            end
          end
        end

        describe 'DELETE #destroy' do
          let(:invalid_attributes) {{ id: rand(100) }}

          it "doesn't exist in database" do
            delete :destroy, ResponseHelper.add_json_format_to(invalid_attributes)

            expect(response.status).to eq(404)
          end
        end
      end
    end
  end

  context 'without auth' do
    let!(:invalid_token) { Faker::Code.ean }
    let!(:violation) { create(:violation) }

    before(:each) { request.headers['Authorization'] = "Bearer #{invalid_token}" }

    describe 'GET #index' do
      let(:valid_attributes) { attributes_for(:violation).slice(:latitude, :longitude) }

      it 'code 403' do
        get :index, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq(403)
      end
    end
    describe 'GET #show' do
      let(:valid_attributes) {{ id: violation.id }}

      it 'code 403' do
        get :show, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq(403)
      end
    end
    describe 'POST #create' do
      let(:valid_attributes) {{ violation: attributes_for(:violation) }}

      it 'code 403' do
        post :create, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq(403)
      end
    end
    describe 'DELETE #destroy' do
      let(:valid_attributes) {{ id: violation.id }}

      it 'code 403' do
        delete :destroy, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq(403)
      end
    end
  end
end
