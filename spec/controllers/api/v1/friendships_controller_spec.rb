require 'rails_helper'

describe Api::V1::FriendshipsController, type: :controller do
  let!(:user) { create(:user) }
  let!(:place) { create(:user) }
  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }
  let!(:new_user_friend) { create(:user) }
  let!(:new_place_friend) { create(:place) }
  let!(:new_friends) { [new_user_friend, new_place_friend] }
  let!(:user_token) { Token.generate(user) }
  let!(:place_token) { Token.generate(place) }
  let(:user_attrs) { User::JSON_ATTRS.map(&:to_s) }
  let(:place_attrs) { Place::JSON_ATTRS.map(&:to_s) }

  before(:each) do
    HasFriendship::Friendship.destroy_all
  end

  describe "Find requested friends" do
    context "Find current user requested friends if current user - User" do
      let!(:requested_friends) do
        user_friend.friend_request(user); place_friend.friend_request(user)
      end

      let!(:pending_friends) do
        user.friend_request(new_user_friend); user.friend_request(new_place_friend)
      end

      it "returns user friends" do
        request.headers["Authorization"] = "Bearer #{user_token}"

        get :index, { requested: 1, format: :json }

        expected_hash = ResponseHelper.object_render(friends.first, 'api/v1/friendships/index',
            requested_users: [user_friend], requested_places: [place_friend],
            pending_users: [new_user_friend], pending_places: [new_place_friend]
        )
        requested_hash = expected_hash.slice("requested_users", "requested_places")

        expect(JSON(response.body)).to eq(requested_hash)
        expect(response.status).to eq 200
      end
    end

    context "Find current user requested friends if current user - Place" do
      let!(:requested_friends) do
        user_friend.friend_request(place); place_friend.friend_request(place)
      end

      let!(:pending_friends) do
        place.friend_request(new_user_friend); place.friend_request(new_place_friend)
      end

      it "returns user friends" do
        request.headers["Authorization"] = "Bearer #{place_token}"

        get :index, { requested: 1, format: :json }

        expected_hash = ResponseHelper.object_render(friends.first, 'api/v1/friendships/index',
            requested_users: [user_friend], requested_places: [place_friend],
            pending_users: [new_user_friend], pending_places: [new_place_friend]
        )
        requested_hash = expected_hash.slice("requested_users", "requested_places")

        expect(JSON(response.body)).to eq(requested_hash)
        expect(response.status).to eq 200
      end
    end
  end

  describe "Find pending friends" do
    context "Find current user pending friends if current user - User" do
      let!(:requested_friends) do
        user_friend.friend_request(user); place_friend.friend_request(user)
      end

      let!(:pending_friends) do
        user.friend_request(new_user_friend); user.friend_request(new_place_friend)
      end

      it "returns user friends" do
        request.headers["Authorization"] = "Bearer #{user_token}"

        get :index, { pending: 1, format: :json }

        expected_hash = ResponseHelper.object_render(friends.first, 'api/v1/friendships/index',
            requested_users: [user_friend], requested_places: [place_friend],
            pending_users: [new_user_friend], pending_places: [new_place_friend]
        )
        requested_hash = expected_hash.slice("pending_users", "pending_places")

        expect(JSON(response.body)).to eq(requested_hash)
        expect(response.status).to eq 200
      end
    end

    context "Find current user pending friends if current user - Place" do
      let!(:requested_friends) do
        user_friend.friend_request(place); place_friend.friend_request(place)
      end

      let!(:pending_friends) do
        place.friend_request(new_user_friend); place.friend_request(new_place_friend)
      end

      it "returns user friends" do
        request.headers["Authorization"] = "Bearer #{place_token}"

        get :index, { pending: 1, format: :json }

        expected_hash = ResponseHelper.object_render(friends.first, 'api/v1/friendships/index',
            requested_users: [user_friend], requested_places: [place_friend],
            pending_users: [new_user_friend], pending_places: [new_place_friend]
        )
        requested_hash = expected_hash.slice("pending_users", "pending_places")

        expect(JSON(response.body)).to eq(requested_hash)
        expect(response.status).to eq 200
      end
    end
  end

  describe "Find pending & requested friends" do
    context "Find current user requested & pending friends if current user - User" do
      let!(:requested_friends) do
        user_friend.friend_request(user); place_friend.friend_request(user)
      end

      let!(:pending_friends) do
        user.friend_request(new_user_friend); user.friend_request(new_place_friend)
      end

      it "returns user friends" do
        request.headers["Authorization"] = "Bearer #{user_token}"

        get :index, { pending: 1, requested: 1, format: :json }

        expect(JSON(response.body)).to eq(ResponseHelper.object_render(friends.first, 'api/v1/friendships/index',
            requested_users: [user_friend], requested_places: [place_friend],
            pending_users: [new_user_friend], pending_places: [new_place_friend])
        )
        expect(response.status).to eq 200
      end
    end

    context "Find current user requested & pending friends if current user - Place" do
      let!(:requested_friends) do
        user_friend.friend_request(place); place_friend.friend_request(place)
      end

      let!(:pending_friends) do
        place.friend_request(new_user_friend); place.friend_request(new_place_friend)
      end

      it "returns user friends" do
        request.headers["Authorization"] = "Bearer #{place_token}"

        get :index, { pending: 1, requested: 1, format: :json }

        expect(JSON(response.body)).to eq(ResponseHelper.object_render(friends.first, 'api/v1/friendships/index',
            requested_users: [user_friend], requested_places: [place_friend],
            pending_users: [new_user_friend], pending_places: [new_place_friend])
        )
        expect(response.status).to eq 200
      end
    end
  end

  describe "Create new friend" do
    context "Acceept new friend request if current user - User" do
      let!(:requested_friends) do
        new_user_friend.friend_request(user); new_place_friend.friend_request(user)
      end

      it "returns 201 if requested friend became a friend" do
        request.headers["Authorization"] = "Bearer #{user_token}"

        post :create, { id: new_place_friend.id, type: 1, format: :json }

        expect(user.my_friends.first).to eq(new_place_friend)
        expect(response.status).to eq 201
      end
    end

    context "Acceept new friend request if current user - Place" do
      let!(:requested_friends) do
        new_user_friend.friend_request(place); new_place_friend.friend_request(place)
      end

      it "returns 201 if requested friend became a friend" do
        request.headers["Authorization"] = "Bearer #{place_token}"

        post :create, { id: new_place_friend.id, type: 1, format: :json }

        expect(place.my_friends.first).to eq(new_place_friend)
        expect(response.status).to eq 201
      end
    end
  end

  describe "Delete friend" do
    context "Decline new friend request if current user - User" do
      let!(:requested_friends) do
        new_user_friend.friend_request(user); new_place_friend.friend_request(user)
      end

      it "returns 200 if friend request was rejected" do
        request.headers["Authorization"] = "Bearer #{user_token}"

        delete :destroy, { id: new_place_friend.id, type: 1, format: :json }

        expect(user.my_friends.include?(place_friend)).to eq(false)
        expect(response.status).to eq 200
      end
    end

    context "Decline new friend request if current user - Place" do
      let!(:requested_friends) do
        new_user_friend.friend_request(place); new_place_friend.friend_request(place)
      end

      it "returns 200 if friend request was rejected" do
        request.headers["Authorization"] = "Bearer #{place_token}"

        delete :destroy, { id: new_place_friend.id, type: 1, format: :json }

        expect(place.my_friends.include?(place_friend)).to eq(false)
        expect(response.status).to eq 200
      end
    end
  end
end
