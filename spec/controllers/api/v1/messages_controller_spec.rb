require 'rails_helper'

RSpec.describe Api::V1::MessagesController, type: :controller do
  context "auth user" do
    let!(:user) { create(:user) }
    let!(:token) { Token.generate(user) }
    before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

    context "allowed actions" do
      describe "GET messages" do
        describe "user is participant in chats" do
          let!(:chat) { create(:chat, sender: user) }
          let!(:messages) { create_list(:message, 2, chat: chat, sender: user) }
          let!(:limit_messages) do
            Message.limit_messages_from_chat(user, chat.id, 10, messages.last.id)
          end
          let(:valid_params) do
            {
              start_message_id: messages.last.id,
              count: 10,
              chat_id: chat.id
            }
          end

          it "returns messages and status 200" do
            get :index, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq 200
            expect(ResponseHelper.get_parsed_body(response))
              .to eq(
                JSON(
                  RablRails.render(
                    limit_messages,
                    "api/v1/messages/index",
                    locals: {
                      :messages_counter => limit_messages.count, :messages => limit_messages
                    }
                  )
                ).first
              )
          end

          context "pagination" do
            let!(:messages) { create_list(:message, 20, chat: chat, sender: user) }
            let!(:limit_messages) do
              Message.limit_messages_from_chat(user, chat.id, 10, messages.last.id)
            end

            it "returns the same messages number as count param" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response))
              .to eq(
                JSON(
                  RablRails.render(
                    limit_messages,
                    "api/v1/messages/index",
                    locals: {
                      :messages_counter => limit_messages.count, :messages => limit_messages
                    }
                  )
                ).first
              )

              expect(ResponseHelper.get_parsed_body(response)['messages'].last)
              .to eq(JSON(RablRails.render(limit_messages[9], "api/v1/messages/item")))
            end
          end

          context "last n messages" do
            let!(:messages) { create_list(:message, 20, chat: chat, sender: user) }
            let(:valid_params) { { count: 10, chat_id: chat.id } }

            it "returns last n messages." do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)).
                to eq(ResponseHelper.collection_render('', 'api/v1/messages/index',
                  messages: Message.last(10), messages_counter: 10)
                )
            end
          end

          context "last 20 messages" do
            let!(:messages) { create_list(:message, 20, chat: chat, sender: user) }
            let(:valid_params) { { chat_id: chat.id } }

            it "without params should return last 20 messages" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)).
                to eq(ResponseHelper.collection_render('', 'api/v1/messages/index',
                  messages: Message.all, messages_counter: 20)
                )
            end
          end
        end
      end

      describe 'POST message' do
        describe 'is participant in the chat' do
          let!(:chat) { create(:chat, sender: user)}
          let!(:messages) { create_list(:message, 2, sender: user, chat: chat) }
          let(:message_params) {{ message: { chat_id: chat.id, body: messages.first.body } }}
          let(:valid_params) { message_params.merge(chat_id: chat.id) }

          it "returns chat json and status 201" do
            post :create, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq 201
            expect(ResponseHelper.get_parsed_body(response))
            .to eq(JSON(RablRails.render(Message.last, 'api/v1/messages/create')))
          end

          it 'count +1' do
            expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
            .to change{ Message.count }.by(1)
          end
        end
      end

      describe "Current user has private option" do
        let!(:user) { create(:user, private: true) }
        let!(:token) { Token.generate(user) }

        before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

        context "only friend can have chat with user" do
          let!(:chat) { create(:chat, sender: user)}
          let(:message_params) {{ message: { chat_id: chat.id, body: Faker::Lorem.sentence } }}
          let(:valid_params) { message_params.merge(chat_id: chat.id) }

          let!(:friendship) do
            user.friend_request(chat.recipient); chat.recipient.accept_request(user);
          end

          it "returns chat json and status 201" do
            post :create, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq 201
            expect(ResponseHelper.get_parsed_body(response))
            .to eq(JSON(RablRails.render(Message.last, 'api/v1/messages/create')))
          end

          it 'count +1' do
            expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
            .to change{ Message.count }.by(1)
          end
        end
      end

      describe 'PUT message' do
        #TODO: Add spec for place
        describe "user's message" do
          it "return false" do
            false
          end
        end

        describe "user's message" do
          let!(:recipient) { create(:user) }
          let!(:chat) { create(:chat, sender: user, recipient: recipient) }
          let!(:message) { create(:message, sender: user, chat: chat) }

          let(:valid_params) do
            { chat_id: chat.id, id: message.id }
          end

          it 'status 200' do
            put :update, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq 200
          end

          it 'change message status' do
            expect(message.read).to eq(false)
            expect { put :update, ResponseHelper.add_json_format_to(valid_params) }
            .to change{ message.read }.to eq(true)
          end
        end
      end
    end

    context 'forbidden actions' do
      let!(:user2) { create(:user) }
      let!(:user3) { create(:user) }

      describe "GET messages" do
        describe "in chats where user is not a participant" do
          let!(:chat) { create(:chat, sender: user2, recipient: user3)}
          let!(:messages) { create_list(:message, 2, chat: chat, sender: user2) }
          let(:valid_params) do
            {
              start_message_id: messages.last.id,
              count: 10,
              chat_id: chat.id
            }
          end

          it 'status 404' do
            get :index, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq 404
          end
        end
      end

      describe "POST message" do
        describe "isn't participant in the chat" do
          let!(:chat) { create(:chat, sender: user2, recipient: user3)}
          let!(:messages) { create_list(:message, 2, chat: chat, sender: user2) }
          let(:valid_params) do
            {
              message: {
                body: messages.first.body,
                chat_id: chat.id,
                user_id: user.id
              },
              chat_id: chat.id
            }
          end

          it 'code 403' do
            post :create, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq 403
          end

          it 'count is the same' do
            expect { post :create, ResponseHelper.add_json_format_to(valid_params) }.not_to change{ Message.count }
          end
        end

        describe "Recipient has private option" do
          let!(:user) { create(:user) }
          let!(:token) { Token.generate(user) }

          before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

          context "only friend can have chat with user" do
            let!(:recipient) { create(:user, private: true) }
            let!(:chat) { create(:chat, sender: user, recipient: recipient) }
            let(:message_params) {{ message: { chat_id: chat.id, body: Faker::Lorem.sentence } }}
            let(:valid_params) { message_params.merge(chat_id: chat.id) }

            it "returns chat json and status 403" do
              post :create, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq 403
            end
          end
        end
      end
    end
  end

  context "auth place" do
    let!(:place) { create(:place) }
    let!(:token) { Token.generate(place) }

    before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

    context "allowed actions" do
      describe "POST #create" do
        describe "can chat if interlocutor is not in a black list" do
          let!(:chat) { create(:chat, sender: place.user)}
          let(:message_params) {{ message: { chat_id: chat.id, body: Faker::Lorem.sentence } }}
          let(:valid_params) { message_params.merge(chat_id: chat.id) }

          it "returns chat json and status 201" do
            post :create, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq 201
            expect(ResponseHelper.get_parsed_body(response))
            .to eq(JSON(RablRails.render(Message.last, 'api/v1/messages/create')))
          end

          it 'count +1' do
            expect { post :create, ResponseHelper.add_json_format_to(valid_params) }
            .to change{ Message.count }.by(1)
          end
        end
      end
    end

    context "forbidden actions" do
      describe "POST #create" do
        describe "can't chat if interlocutor is in a black list" do
          let!(:recipient) { create(:place) }
          let!(:chat) { create(:chat, sender: place.user, recipient: recipient.user) }
          let!(:black_list) { create(:black_list, place: recipient, user: place.user) }
          let(:message_params) {{ message: { chat_id: chat.id, body: Faker::Lorem.sentence } }}
          let(:valid_params) { message_params.merge(chat_id: chat.id) }

          it "returns chat json and status 403" do
            post :create, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq 403
          end
        end
      end
    end
  end

  context 'without auth' do
    let!(:chat) { create(:chat)}
    let!(:user) { create(:user) }
    let!(:messages) { create_list(:message, 2, chat: chat, sender: user) }
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) {
      request.headers['Authorization'] = "Bearer #{invalid_token}"
    }

    describe "GET messages" do
      let(:valid_params) {{ chat_id: chat.id, user_id: user.id }}

      it 'status 403' do
        get :index, ResponseHelper.add_json_format_to(valid_params)
        expect(response.status).to eq 403
      end
    end

    describe "POST #create" do
      let(:valid_params) {{ body: messages.first.body, chat_id: chat.id, user_id: user.id }}

      it 'status 403' do
        post :create, ResponseHelper.add_json_format_to(valid_params)
        expect(response.status).to eq 403
      end
    end

    describe "PUT #update" do
      let(:valid_params) {{ id: messages.first.id, chat_id: chat.id, user_id: user.id }}

      it 'status 403' do
        put :update, ResponseHelper.add_json_format_to(valid_params)
        expect(response.status).to eq 403
      end
    end
  end
end
