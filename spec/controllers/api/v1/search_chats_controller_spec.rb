require 'rails_helper'

RSpec.describe Api::V1::SearchChatsController, type: :controller do
  context 'auth' do
    let!(:sender) { create(:user) }
    let!(:recipient) { create(:user) }
    let!(:recipient2) { create(:user) }

    let!(:token) { Token.generate(sender) }
    before(:each) { request.headers["Authorization"] = "Bearer #{token}" }

    context "allowed actions" do
      let!(:chat) { create(:chat, sender: sender, recipient: recipient) }

      describe "by recipient_phone_number" do
        let(:valid_params) { { recipient_phone_number: recipient.phone_number } }

        it "status 200" do
          get :index, ResponseHelper.add_json_format_to(valid_params)

          expect(response.status).to eq(200)
        end

        it "returns chat by valid recipient_phone_number" do
          get :index, ResponseHelper.add_json_format_to(valid_params)

          expect(ResponseHelper.get_parsed_body(response)).
            to eq(JSON RablRails.render(chat, "api/v1/chats/show", locals: { current_user: sender }))
        end
      end
    end

    context "forbidden acitons" do
      let!(:chat) { create(:chat, sender: sender, recipient: recipient) }

      describe "by recipient_phone_number" do
        describe "no recipient with such number" do
          let(:invalid_params) { { recipient_phone_number: Faker::PhoneNumber.cell_phone } }

          it "status 404" do
            get :index, ResponseHelper.add_json_format_to(invalid_params)

            expect(response.status).to eq(404)
          end
        end

        describe "no chat between users" do
          let(:valid_params) { { recipient_phone_number: recipient2.phone_number } }

          it "status 404" do
            get :index, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq(404)
          end
        end
      end
    end
  end
end
