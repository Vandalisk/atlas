require 'rails_helper'

RSpec.describe Api::V1::EventsController, type: :controller do
  let!(:user) { create(:user) }
  let!(:place) { create(:place) }

  context 'auth' do
    context 'as a user' do
      let(:token) { Token.generate(user) }
      before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

      context 'allowed actions' do
        describe 'GET #index' do
          let(:attrs) { Event::JSON_ATTRS.map(&:to_s) }
          let!(:events) { create_list(:event, 2, place: place) }
          let(:valid_attributes) {{ place_id: place.id }}

          it 'return 200' do
            get :index, ResponseHelper.add_json_format_to(valid_attributes)

            expect(response.status).to eq 200
          end

          it "should return place's events" do
            get :index, ResponseHelper.add_json_format_to(valid_attributes)
            json = ResponseHelper.get_parsed_body(response)

            expect(json['events'].size).to eq events.size
            expect(json).to eq(ResponseHelper.collection_render(nil, 'api/v1/events/index', { place: place, events: events }))
          end
        end

        describe 'GET #show' do
          let(:attrs) { Event::JSON_ATTRS.map(&:to_s) }
          let!(:event) { create(:event, place: place) }
          let(:valid_attributes) {{ place_id: place.id, id: event.id }}

          it 'return 200' do
            get :show, ResponseHelper.add_json_format_to(valid_attributes)

            expect(response.status).to eq 200
          end

          it "should return event" do
            get :show, ResponseHelper.add_json_format_to(valid_attributes)

            expect(ResponseHelper.get_parsed_body(response)).
              to eq(ResponseHelper.object_render(nil, 'api/v1/events/show', place: place, event: event))
          end
        end
      end

      context 'forbidden actions' do
        describe 'GET #index' do
          context 'place is not found' do
            describe "tries to get place's events" do
              let(:invalid_attributes) {{ place_id: place.id }}
              before { place.destroy }

              it 'code 404' do
                get :index, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(404)
              end
            end
          end
        end

        describe 'GET #show' do
          context 'place is not found' do
            describe "tries to get event" do
              let!(:event) { create(:event, place: place) }
              let(:invalid_attributes) {{ place_id: place.id, id: event.id }}
              before { place.destroy }

              it 'code 404' do
                get :show, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(404)
              end
            end
          end

          context 'event is not found' do
            describe "tries to get event" do
              let(:invalid_attributes) {{ place_id: place.id, id: rand(1..50) }}

              it 'code 404' do
                get :show, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(404)
              end
            end
          end
        end

        describe 'POST #create' do
          context "user can't create an event."  do
            describe "tries to create event" do
              let(:valid_attributes) {{ place_id: place.id, event: attributes_for(:event) }}

              it 'code 404' do
                post :create, ResponseHelper.add_json_format_to(valid_attributes)

                expect(response.status).to eq(404)
              end

              it 'events count is the same' do
                expect { post :create, ResponseHelper.add_json_format_to(valid_attributes) }
                  .not_to change(Event,:count)
              end
            end
          end
        end

        describe 'PUT #update' do
          let!(:event) { create(:event, place: place) }

          describe "user can't update event" do
            let!(:event) { create(:event, place: place) }
            let(:attrs) { Event::JSON_ATTRS.map(&:to_s) }
            let(:valid_attributes) do
              {
                place_id: place.id,
                id: event.id,
                event:
                { title: 'New title', description: 'New description', time: Time.now().utc}
              }
            end

            it 'code 404' do
              put :update, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq 404
            end
          end
        end

        describe 'DELETE #destroy' do
          describe "user can't delete event" do
            let!(:event) { create(:event, place: place) }
            let(:valid_attributes) {{ place_id: place.id, id: event.id }}

            it 'code 404' do
              delete :destroy, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq 404
            end
          end
        end
      end
    end

    context 'as a place' do
      let(:token) { Token.generate(place) }
      before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

      context 'allowed actions' do
        describe 'GET #index' do
          let(:attrs) { Event::JSON_ATTRS.map(&:to_s) }
          let!(:events) { create_list(:event, 2, place: place) }
          let(:valid_attributes) {{ place_id: place.id }}

          it 'return 200' do
            get :index, ResponseHelper.add_json_format_to(valid_attributes)

            expect(response.status).to eq 200
          end

          it "should return place's events" do
            get :index, ResponseHelper.add_json_format_to(valid_attributes)
            json = ResponseHelper.get_parsed_body(response)

            expect(json['events'].size).to eq events.size
            expect(json).to eq(ResponseHelper.collection_render(nil, 'api/v1/events/index', { place: place, events: events }))
          end
        end

        describe 'GET #show' do
          let(:attrs) { Event::JSON_ATTRS.map(&:to_s) }
          let!(:event) { create(:event, place: place) }
          let(:valid_attributes) {{ place_id: place.id, id: event.id }}

          it 'return 200' do
            get :show, ResponseHelper.add_json_format_to(valid_attributes)

            expect(response.status).to eq 200
          end

          it "should return event" do
            get :show, ResponseHelper.add_json_format_to(valid_attributes)

            expect(ResponseHelper.get_parsed_body(response)).
              to eq(ResponseHelper.object_render(nil, 'api/v1/events/show', place: place, event: event))
          end
        end

        describe 'POST #create' do
          let(:valid_attributes) {{ place_id: place.id, event: attributes_for(:event) }}
          let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['id', 'created_at'] }

          context 'place can create event' do
            it 'code 200' do
              post :create, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq 200
            end

            it 'create new event' do
              expect { post :create, ResponseHelper.add_json_format_to(valid_attributes) }.to change{ Event.count }.by(1)
            end
          end
        end

        describe 'PUT #update' do
          describe 'user can update event' do
            let!(:event) { create(:event, place: place) }
            let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['created_at'] }
            let(:valid_attributes) do
              {
                place_id: place.id,
                id: event.id,
                event:
                { title: 'New title', description: 'New description', time: Time.now().utc}
              }
            end

            it 'code 200' do
              put :update, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq 200
            end

            it 'attributes are equal' do
              put :update, ResponseHelper.add_json_format_to(valid_attributes)

              expect(Event.find(event.id).title).to eq (valid_attributes[:event][:title])
            end
          end
        end

        describe 'DELETE #destroy' do
          describe 'user can delete event' do
            let!(:event) { create(:event, place: place) }
            let(:valid_attributes) {{ place_id: place.id, id: event.id }}
            let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['id', 'created_at'] }

            it 'code 200' do
              delete :destroy, ResponseHelper.add_json_format_to(valid_attributes)

              expect(response.status).to eq 200
            end

            it 'deletes event' do
              expect { delete :destroy, ResponseHelper.add_json_format_to(valid_attributes) }.
                to change{ Event.count }.by(-1)
            end
          end
        end
      end

      context 'forbidden actions' do
        describe 'GET #index' do
          context 'place is not found' do
            describe "tries to get place's events" do
              let(:invalid_attributes) {{ place_id: place.id }}
              before { place.destroy }

              it 'code 404' do
                get :index, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(404)
              end
            end
          end
        end

        describe 'GET #show' do
          context 'place is not found' do
            describe "tries to get event" do
              let!(:event) { create(:event, place: place) }
              let(:invalid_attributes) {{ place_id: place.id, id: event.id }}
              before { place.destroy }

              it 'code 404' do
                get :show, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(404)
              end
            end
          end

          context 'event is not found' do
            describe "tries to get event" do
              let(:invalid_attributes) {{ place_id: place.id, id: rand(1..50) }}

              it 'code 404' do
                get :show, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(404)
              end
            end
          end
        end

        describe 'POST #create' do
          context 'place is not found' do
            describe "tries to create event" do
              let(:invalid_attributes) {{ place_id: place.id, event: attributes_for(:event) }}
              before { place.destroy }

              it 'code 404' do
                post :create, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(404)
              end

              it 'events count is the same' do
                expect { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }
                  .not_to change(Event,:count)
              end
            end
          end

          context 'invalid params' do
            context 'invalid title' do
              let(:valid_event_attributes) {{ place_id: place.id, event: attributes_for(:event) }}
              let(:invalid_attributes) do
                valid_event_attributes[:event][:title] =  nil; valid_event_attributes
              end

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'events count is the same' do
                expect { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }
                  .not_to change(Event,:count)
              end
            end

            context 'invalid description' do
              let(:valid_event_attributes) {{ place_id: place.id, event: attributes_for(:event) }}
              let(:invalid_attributes) do
                valid_event_attributes[:event][:description] =  nil; valid_event_attributes
              end

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'events count is the same' do
                expect { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }
                  .not_to change(Event,:count)
              end
            end

            context 'invalid time' do
              let(:valid_event_attributes) {{ place_id: place.id, event: attributes_for(:event) }}
              let(:invalid_attributes) do
                valid_event_attributes[:event][:time] =  nil; valid_event_attributes
              end

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'events count is the same' do
                expect { post :create, ResponseHelper.add_json_format_to(invalid_attributes) }
                  .not_to change(Event,:count)
              end
            end
          end
        end

        describe 'PUT #update' do
          let!(:event) { create(:event, place: place) }

          context 'invalid attributes' do
            context 'invalid title' do
              let(:valid_event_attributes) {{ place_id: place.id, id: event.id, event: attributes_for(:event) }}
              let(:invalid_attributes) do
                valid_event_attributes[:event][:title] =  nil; valid_event_attributes
              end

              it 'code 400' do
                put :update, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'attributes are equal' do
                put :update, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(Event.find(event.id)).to eq event
              end
            end

            context 'invalid description' do
              let(:valid_event_attributes) {{ place_id: place.id, id: event.id, event: attributes_for(:event) }}
              let(:invalid_attributes) do
                valid_event_attributes[:event][:description] =  nil; valid_event_attributes
              end

              it 'code 400' do
                put :update, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'attributes are equal' do
                put :update, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(Event.find(event.id)).to eq event
              end
            end

            context 'invalid time' do
              let(:valid_event_attributes) {{ place_id: place.id, id: event.id, event: attributes_for(:event) }}
              let(:invalid_attributes) do
                valid_event_attributes[:event][:time] =  nil; valid_event_attributes
              end

              it 'code 400' do
                put :update, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(response.status).to eq(400)
              end

              it 'attributes are equal' do
                put :update, ResponseHelper.add_json_format_to(invalid_attributes)

                expect(Event.find(event.id)).to eq event
              end
            end
          end

          context "event doesn't exist" do
            let(:invalid_attributes) {{ place_id: place.id, id: event.id }}
            before { event.destroy }

            it 'code 404' do
              put :update, ResponseHelper.add_json_format_to(invalid_attributes)

              expect(response.status).to eq(404)
            end
          end
        end

        describe 'DELETE #destroy' do
          context "place doesn't exist" do
            let!(:event) { create(:event, place: place) }
            let(:invalid_attributes) {{ place_id: place.id, id: event.id }}
            before { place.destroy }

            it 'code 404' do
              delete :destroy, ResponseHelper.add_json_format_to(invalid_attributes)

              expect(response.status).to eq(404)
            end

            it 'events count is the same' do
              expect { delete :destroy, ResponseHelper.add_json_format_to(invalid_attributes) }
                .not_to change(Event,:count)
            end
          end

          context "event doesn't exist" do
            let!(:event) { create(:event, place: place) }
            let(:invalid_attributes) {{ place_id: place.id, id: event.id }}
            before { event.destroy }

            it 'code 404' do
              delete :destroy, ResponseHelper.add_json_format_to(invalid_attributes)

              expect(response.status).to eq(404)
            end

            it 'events count is the same' do
              expect { delete :destroy, ResponseHelper.add_json_format_to(invalid_attributes) }
                .not_to change(Event,:count)
            end
          end
        end
      end
    end
  end

  context 'without auth' do
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) { request.headers['Authorization'] = "Bearer #{invalid_token}" }

    describe 'GET #index' do
      let(:attrs) { Event::JSON_ATTRS.map(&:to_s) }
      let!(:events) { create_list(:event, 2, place: place) }
      let(:valid_attributes) {{ place_id: place.id }}

      it 'return 403' do
        get :index, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq 403
      end
    end

    describe 'GET #show' do
      let(:attrs) { Event::JSON_ATTRS.map(&:to_s) }
      let!(:event) { create(:event, place: place) }
      let(:valid_attributes) {{ place_id: place.id, id: event.id }}

      it 'return 403' do
        get :show, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq 403
      end
    end

    describe 'POST #create' do
      let(:valid_attributes) {{ place_id: place.id, event: attributes_for(:event) }}
      let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['id', 'created_at'] }

      context "can't create event" do
        it 'code 403' do
          post :create, ResponseHelper.add_json_format_to(valid_attributes)

          expect(response.status).to eq 403
        end

        it 'count is the same' do
          expect { post :create, ResponseHelper.add_json_format_to(valid_attributes) }.
            not_to change(Event, :count)
        end
      end
    end

    describe 'PUT #update' do
      let!(:event) { create(:event, place: place) }
      let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['created_at'] }
      let(:current_uttrs) { event.attributes }
      let(:valid_attributes) do
        {
          place_id: place.id,
          id: event.id,
          event:
            { title: 'New title', description: 'New description', time: Time.now().utc }
        }
      end

      it 'code 403' do
        put :update, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq 403
      end

      it 'attributes are equal' do
        post :create, ResponseHelper.add_json_format_to(valid_attributes)

        expect(current_uttrs).to eq event.reload.attributes
      end
    end

    describe 'DELETE #destroy' do
      let!(:event) { create(:event, place: place) }
      let(:valid_attributes) {{ place_id: place.id, id: event.id}}

      it 'code 403' do
        delete :destroy, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq 403
      end

      it 'count is the same' do
        expect { delete :destroy, ResponseHelper.add_json_format_to(valid_attributes) }.
          not_to change(Event, :count)
      end
    end
  end
end

def event_attrs(valid_attributes)
  result = valid_attributes.merge(valid_attributes[:event]).stringify_keys
  result.delete('event')
  result.delete('format')
  result['time'] = result['time'].strftime('%d/%m/%Y %H:%M:%S')
  result
end
