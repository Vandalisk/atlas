require 'rails_helper'

RSpec.describe Api::V1::CommentsController, type: :controller do
  let!(:user) { create(:user) }
  let(:place_owner) { create(:user, owner: true) }
  let!(:place) { create(:place, user: place_owner) }
  let(:place_attr) {{ place_id: place.id }}

  context 'with auth' do
    context 'as a user' do
      let(:token) { Token.generate(user) }
      before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

      context 'allowed actions' do
        describe 'GET #index' do
          describe "request all place's comments" do
            let(:valid_params) { place_attr }
            let!(:comments) { create_list(:comment, 10, user: user, place: place) }
            let(:attrs) { Comment::JSON_ATTRS.map(&:to_s) }

            before(:each) { get :index, ResponseHelper.add_json_format_to(valid_params) }

            it 'code 200' do
              expect(response.status).to eq(200)
            end

            it 'returns comments' do
              expect(ResponseHelper.get_parsed_body(response)).
                to eq(ResponseHelper.collection_render(comments, 'api/v1/comments/index'))
            end
          end
        end

        describe 'POST #create' do
          context 'common params' do
            let(:valid_params) do
              place_attr.merge(comment: attributes_for(:comment, user_id: user.id, place_id: place.id))
            end

            describe 'creates new comment' do
              it 'code 201' do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq(201)
              end

              it 'count +1' do
                expect{ post :create, ResponseHelper.add_json_format_to(valid_params) }.
                  to change(Comment, :count).by(1)
              end
            end
          end

          context 'body is empty' do
            let(:valid_params) do
              place_attr.merge(
                comment: attributes_for(:comment, :empty_body, user_id: user.id, place_id: place.id)
              )
            end

            describe 'creates new comment' do
              it 'code 201' do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq(201)
              end

              it 'count +1' do
                expect{ post :create, ResponseHelper.add_json_format_to(valid_params) }.
                  to change(Comment, :count).by(1)
              end
            end
          end
        end

        describe 'DELETE #destroy' do
          describe 'owner tries to delete comment' do
            let!(:comment) { create(:comment, user: user, place: place) }
            let(:valid_params) { place_attr.merge(id: comment.id) }

            describe 'deletes comment' do
              it 'code 200' do
                delete :destroy, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq(200)
              end

              it 'count -1' do
                expect{ delete :destroy, ResponseHelper.add_json_format_to(valid_params) }.
                  to change(Comment, :count).by(-1)
              end
            end
          end
        end
      end

      context 'forbidden actions' do
        describe 'POST #create' do
          let(:valid_params) do
            place_attr.merge(comment: attributes_for(:comment, user_id: user.id, place_id: place.id))
          end

          describe 'tries to create with invalid params' do
            context 'rating > 5' do
              let(:invalid_params) { valid_params[:comment][:rating] = rand(6..100); valid_params }

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_params)

                expect(response.status).to eq(400)
              end

              it 'count the same' do
                expect{ post :create, ResponseHelper.add_json_format_to(invalid_params) }.
                  not_to change(Comment, :count)
              end
            end

            context 'rating < 0' do
              let(:invalid_params) { valid_params[:comment][:rating] = rand(-10..-1); valid_params }

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_params)

                expect(response.status).to eq(400)
              end

              it 'count the same' do
                expect{ post :create, ResponseHelper.add_json_format_to(invalid_params) }.
                  not_to change(Comment, :count)
              end
            end

            context 'without rating' do
              let(:invalid_params) { valid_params[:comment][:rating] = nil; valid_params }

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_params)

                expect(response.status).to eq(400)
              end

              it 'count the same' do
                expect{ post :create, ResponseHelper.add_json_format_to(invalid_params) }.
                  not_to change(Comment, :count)
              end
            end

            context 'body.length > 400 symbols' do
              let(:invalid_params) { valid_params[:comment][:body] = 'c' * 400; valid_params }

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_params)

                expect(response.status).to eq(400)
              end

              it 'count the same' do
                expect{ post :create, ResponseHelper.add_json_format_to(invalid_params) }.
                  not_to change(Comment, :count)
              end
            end
          end
        end

        describe 'DELETE #destroy' do
          describe "not owner tries to delete comment" do
            let(:user2) { create(:user) }
            let!(:comment) { create(:comment, user: user2, place: place) }
            let(:valid_params) { place_attr.merge(id: comment.id) }

            describe "doesn't delete a comment" do
              it 'code 403' do
                delete :destroy, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq(403)
              end

              it 'count the same' do
                expect{ delete :destroy, ResponseHelper.add_json_format_to(valid_params) }.
                  to change(Comment, :count).by(0)
              end
            end
          end
        end
      end
    end

    context 'as a place' do
      let(:token) { Token.generate(place) }
      before(:each) { request.headers['Authorization'] = "Bearer #{token}" }
      let(:place_attr) {{ place_id: place.id }}

      context 'allowed actions' do
        describe 'GET #index' do
          describe "request all place's comments" do
            let(:valid_params) { place_attr }
            let!(:comments) { create_list(:comment, 10, user: user, place: place) }
            let(:attrs) { Comment::JSON_ATTRS.map(&:to_s) }

            before(:each) { get :index, ResponseHelper.add_json_format_to(valid_params) }

            it 'code 200' do
              expect(response.status).to eq(200)
            end

            it 'returns comments' do
              expect(ResponseHelper.get_parsed_body(response)).
                to eq(ResponseHelper.collection_render(comments, 'api/v1/comments/index'))
            end
          end
        end

        describe 'POST #create' do
          context 'common params' do
            let(:valid_params) do
              place_attr.merge(comment: attributes_for(:comment, user_id: place_owner.id, place_id: place.id))
            end

            describe 'creates new comment' do
              it 'code 201' do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq(201)
              end

              it 'count +1' do
                expect{ post :create, ResponseHelper.add_json_format_to(valid_params) }.
                  to change(Comment, :count).by(1)
              end
            end
          end

          context 'body is empty' do
            let(:valid_params) do
              place_attr.merge(
                comment: attributes_for(:comment, :empty_body, user_id: place_owner.id, place_id: place.id)
              )
            end

            describe 'creates new comment' do
              it 'code 201' do
                post :create, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq(201)
              end

              it 'count +1' do
                expect{ post :create, ResponseHelper.add_json_format_to(valid_params) }.
                  to change(Comment, :count).by(1)
              end
            end
          end
        end

        describe 'DELETE #destroy' do
          describe 'owner tries to delete comment' do
            let!(:comment) { create(:comment, user: place_owner, place: place) }
            let(:valid_params) { place_attr.merge(id: comment.id) }

            describe 'deletes comment' do
              it 'code 200' do
                delete :destroy, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq(200)
              end

              it 'count -1' do
                expect{ delete :destroy, ResponseHelper.add_json_format_to(valid_params) }.
                  to change(Comment, :count).by(-1)
              end
            end
          end
        end
      end

      context 'forbidden actions' do
        describe 'POST #create' do
          let(:valid_params) do
            place_attr.merge(comment: attributes_for(:comment, user_id: user.id, place_id: place.id))
          end

          describe 'tries to create with invalid params' do
            context 'rating > 5' do
              let(:invalid_params) { valid_params[:comment][:rating] = rand(6..100); valid_params }

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_params)

                expect(response.status).to eq(400)
              end

              it 'count the same' do
                expect{ post :create, ResponseHelper.add_json_format_to(invalid_params) }.
                  not_to change(Comment, :count)
              end
            end

            context 'rating < 0' do
              let(:invalid_params) { valid_params[:comment][:rating] = rand(-10..-1); valid_params }

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_params)

                expect(response.status).to eq(400)
              end

              it 'count the same' do
                expect{ post :create, ResponseHelper.add_json_format_to(invalid_params) }.
                  not_to change(Comment, :count)
              end
            end

            context 'without rating' do
              let(:invalid_params) { valid_params[:comment][:rating] = nil; valid_params }

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_params)

                expect(response.status).to eq(400)
              end

              it 'count the same' do
                expect{ post :create, ResponseHelper.add_json_format_to(invalid_params) }.
                  not_to change(Comment, :count)
              end
            end

            context 'body.length > 400 symbols' do
              let(:invalid_params) { valid_params[:comment][:body] = 'c' * 400; valid_params }

              it 'code 400' do
                post :create, ResponseHelper.add_json_format_to(invalid_params)

                expect(response.status).to eq(400)
              end

              it 'count the same' do
                expect{ post :create, ResponseHelper.add_json_format_to(invalid_params) }.
                  not_to change(Comment, :count)
              end
            end
          end
        end

        describe 'DELETE #destroy' do
          describe "not owner tries to delete comment" do
            let(:user2) { create(:user) }
            let!(:comment) { create(:comment, user: user2, place: place) }
            let(:valid_params) { place_attr.merge(id: comment.id) }

            describe "doesn't delete a comment" do
              it 'code 403' do
                delete :destroy, ResponseHelper.add_json_format_to(valid_params)

                expect(response.status).to eq(403)
              end

              it 'count the same' do
                expect{ delete :destroy, ResponseHelper.add_json_format_to(valid_params) }.
                  to change(Comment, :count).by(0)
              end
            end
          end
        end
      end
    end
  end

  context 'without auth' do
    describe 'GET #index' do
      context 'tries to get comments' do
        let(:valid_params) { place_attr }
        let!(:comments) { create_list(:comment, 2, user: user, place: place) }
        let(:attrs) { Comment::JSON_ATTRS.map(&:to_s) }

        it 'code 403' do
          get :index, ResponseHelper.add_json_format_to(valid_params)

          expect(response.status).to eq(403)
        end
      end
    end

    describe 'POST #create' do
      context 'tries to create comments' do
        let(:valid_params) do
          place_attr.merge(comment: attributes_for(:comment, user_id: user.id, place_id: place.id))
        end

        it 'code 403' do
          post :create, ResponseHelper.add_json_format_to(valid_params)

          expect(response.status).to eq(403)
        end
      end
    end

    describe 'DELETE #destroy' do
      context 'tries to destroy comment' do
        let!(:comment) { create(:comment, user: user, place: place) }
        let(:valid_params) { place_attr.merge(id: comment.id) }

        it 'code 403' do
          delete :destroy, ResponseHelper.add_json_format_to(valid_params)

          expect(response.status).to eq(403)
        end
      end
    end
  end
end
