require 'rails_helper'

RSpec.describe Api::V1::NewsController, type: :controller do
  context 'auth' do
    let!(:user) { create(:user, latitude: 39.000400, longitude: -75.9900010) }
    let!(:place) { create(:place) }
    let!(:place_token) { Token.generate(place) }
    let!(:user_token) { Token.generate(user) }

    context 'allowed actions' do
      describe 'GET #index' do
        before(:each) { request.headers['Authorization'] = "Bearer #{user_token}" }

        let!(:place1) { create(:place, user: create(:user, owner: true, latitude: 39.004300, longitude: -75.990200)) }
        let!(:place2) { create(:place, user: create(:user, owner: true, latitude: 39.000540, longitude: -75.990090)) }
        let(:news1) { create_list(:news, 2, place: place1) }
        let(:news2) { create_list(:news, 2, place: place2) }
        let!(:news) { news1 + news2 }
        let(:valid_attributes) {{}}
        let(:attrs) { News::JSON_ATTRS.map(&:to_s) + ['created_at', 'updated_at'] }

        it 'return 200' do
          get :index, ResponseHelper.add_json_format_to(valid_attributes)

          expect(response.status).to eq 200
        end

        it 'should return news near user' do
          get :index, ResponseHelper.add_json_format_to(valid_attributes)

          json = ResponseHelper.get_parsed_body(response)

          expect(json['news'].size).to eq news.size
          expect(json).to eq(ResponseHelper.collection_render(news.reverse, 'api/v1/news/index'))
        end
      end
    end
  end

  context 'without auth' do
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) { request.headers['Authorization'] = "Bearer #{invalid_token}" }

    describe 'GET #index' do
      let(:point) {{ news: {latitude: 39.000001, longitude: -76.000001} }}
      let(:valid_attributes) { point }

      it 'return 403' do
        get :index, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq 403
      end
    end
  end
end
