require 'rails_helper'

RSpec.describe Api::V1::BlackListsController, type: :controller do
  context 'auth' do
    context 'user is a user' do
      let!(:user) { create(:user) }
      let!(:token) { Token.generate(user) }
      let!(:user2) { create(:user) }

      before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

      context 'forbidden actions' do
        describe 'GET #index' do
          let(:valid_params) {{}}

          it "user can't get the black list" do
            get :index, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq(404)
          end
        end

        describe 'POST #create' do
          let(:valid_params) {{ black_list: { phone_number: user2.phone_number } }}

          it "user can't add record to the black list" do
            post :create, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq(404)
          end
        end

        describe 'DELETE #destroy' do
          let(:valid_params) {{ phone_number: user2.phone_number }}

          it "user can't destroy record from the black list" do
            post :create, ResponseHelper.add_json_format_to(valid_params)

            expect(response.status).to eq(404)
          end
        end
      end
    end

    context 'user is a place' do
      let!(:place) { create(:place) }
      let!(:token) { Token.generate(place) }

      before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

      context 'allowed actions' do
        describe 'GET #index' do
          let!(:black_list) do
            create_list(:black_list, 2, place: place, user: create(:user, owner: true)) +
            create_list(:black_list, 2, place: place)
          end

          let!(:blocked_users) { place.users_in_black_list }
          let!(:blocked_places) { place.places_in_black_list }

          describe "get all users with 'users' param" do
            let(:valid_params) {{ users: true }}

            it "status 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "should return users" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)).
                to eq(
                  ResponseHelper.collection_render(nil, "api/v1/black_lists/users",
                    { users: blocked_users }
                  )
                )
            end
          end

          describe "get all places with 'places' param" do
            let(:valid_params) {{ places: true }}

            it "status 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "should return places" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)).
                to eq(
                  ResponseHelper.collection_render(nil, "api/v1/black_lists/places",
                    { places: blocked_places }
                  )
                )
            end
          end

          describe "get all users and places without param" do
            let(:valid_params) {{}}

            it "status 200" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end

            it "should return places" do
              get :index, ResponseHelper.add_json_format_to(valid_params)

              expect(ResponseHelper.get_parsed_body(response)).
                to eq(
                  ResponseHelper.collection_render(nil, "api/v1/black_lists/index",
                    { places: blocked_places, users: blocked_users }
                  )
                )
            end
          end
        end

        describe 'GET #show' do
          let!(:place2) { create(:place, user: create(:user, owner: true)) }
          let!(:black_list) { create(:black_list, place: place, user: place2.user) }
          let!(:user) { create(:user) }

          let!(:blocked_user) { place.users_in_black_list.first }

          describe "check that user is blocked by phone number" do
            let(:valid_params) {{ phone_number: place2.phone_number }}

            it "status 200" do
              get :show, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end
          end

          describe "check that place is blocked by phone number" do
            let(:valid_params) {{ phone_number: user.phone_number }}

            it "status 200" do
              get :show, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end
          end
        end

        describe 'POST #create' do
          let!(:user) { create(:user) }
          let!(:place2) { create(:place) }

          describe "should add user to a black list" do
            let(:valid_params) {{ black_list: { phone_number: user.phone_number } }}

            it "status 200" do
              post :create, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end
          end

          describe "should add place to a black list" do
            let(:valid_params) {{ black_list: { phone_number: place2.phone_number } }}

            it "status 200" do
              post :create, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end
          end
        end

        describe 'DELETE #destroy' do
          let!(:user) { create(:user) }
          let!(:place2) { create(:place) }

          describe "should delete user from a black list" do
            let(:valid_params) {{ phone_number: user.phone_number }}

            before { create(:black_list, place: place, user: user) }

            it "status 200" do
              delete :destroy, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end
          end

          describe "should add place to a black list" do
            let(:valid_params) {{ phone_number: place2.phone_number }}

            before { create(:black_list, place: place, user: place2.user) }

            it "status 200" do
              delete :destroy, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(200)
            end
          end
        end
      end

      context 'forbidden actions' do
        describe 'POST #create' do
          describe "no such user in database with this phone number" do
            let(:valid_params) {{ black_list: { phone_number: Faker::PhoneNumber.cell_phone } }}

            it "status 404" do
              post :create, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(404)
            end
          end
        end

        describe 'DELETE #destroy' do
          describe "no such user in database with this phone number" do
            let(:valid_params) {{ phone_number: Faker::PhoneNumber.cell_phone }}

            it "status 404" do
              delete :destroy, ResponseHelper.add_json_format_to(valid_params)

              expect(response.status).to eq(404)
            end
          end
        end
      end
    end
  end

  context 'without auth' do
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) { request.headers['Authorization'] = "Bearer #{invalid_token}" }
  end
end
