require 'rails_helper'

RSpec.describe Api::V1::ServicesController, type: :controller do
  context 'auth' do
    let!(:user) { create(:user, latitude: 39.010300, longitude: -75.990050) }
    let!(:token) { Token.generate(user) }

    before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

    context 'allowed actions' do
      describe 'GET #index' do
        let!(:close_user_owner1) { create(:user, latitude: 39.010000,longitude: -75.990000, owner: true) }
        let!(:close_user_owner2) { create(:user, latitude: 39.010201,longitude: -75.990040, owner: true) }
        let!(:place1) { create(:place, user: close_user_owner1) }
        let!(:place2) { create(:place, user: close_user_owner2) }

        let!(:events) { [create(:event, place: place2), create(:event, place: place1)] }

        let(:valid_attributes) {{}}

        it 'return 200' do
          get :index, ResponseHelper.add_json_format_to(valid_attributes)

          expect(response.status).to eq 200
        end

        it 'return services nearby' do
          get :index, ResponseHelper.add_json_format_to(valid_attributes)

          expect(ResponseHelper.get_parsed_body(response)).
            to eq ResponseHelper.collection_render(events, 'api/v1/services/index')
        end
      end
    end
  end

  context 'without auth' do
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) { request.headers['Authorization'] = "Bearer #{invalid_token}" }

    describe 'GET #index' do
      let(:valid_attributes) {{}}

      it 'return 403' do
        get :index, ResponseHelper.add_json_format_to(valid_attributes)

        expect(response.status).to eq 403
      end
    end
  end
end
