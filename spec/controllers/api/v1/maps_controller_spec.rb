require 'rails_helper'

describe Api::V1::MapsController, type: :controller do
  context 'auth' do
    let!(:user_with_place) { create(:user, latitude: 39.000000, longitude: -76.000000, owner: true) }
    let!(:user) { create(:user, latitude: 39.000000, longitude: -76.000000) }

    let!(:far_user) { create(:user, latitude: 40.000000, longitude: -77.000000) }
    let!(:far_user_owner) { create(:user, latitude: 40.000000,longitude: -77.000000, owner: true) }
    let!(:far_place) { create(:place, user: far_user_owner) }

    let!(:close_user) { create(:user, latitude: 39.000500, longitude: -76.000000) }
    let!(:close_user_owner) { create(:user, latitude: 39.000500,longitude: -76.000000, owner: true) }
    let!(:close_place) { create(:place, user: close_user_owner) }

    let(:place_friend) { create(:place) }
    let(:user_friend) { create(:user) }
    let(:requested_place) { create(:place) }
    let(:requested_user) { create(:user) }
    let(:pending_place) { create(:place) }
    let(:pending_user) { create(:user) }
    let!(:violations) { create_list(:violation, 3, user: close_user) }

    let!(:place) { create(:place, user: user_with_place) }
    let!(:user_token) { Token.generate(user) }
    let!(:place_token) { Token.generate(place) }

    context "allowed actions" do
      describe "GET #index" do
        context "place tries to receive places and users nearby" do
          let(:valid_attributes) { {} }
          context "place with friends" do
            before(:each) do
              HasFriendship::Friendship.destroy_all
              request.headers["Authorization"] = "Bearer #{place_token}"

              #place_friends
              place.friend_request(place_friend); place_friend.accept_request(place)

              #user_friends
              place.friend_request(user_friend); user_friend.accept_request(place)

              #requested_places
              requested_place.friend_request(place)

              #requested_users
              requested_user.friend_request(place)

              #pending_places
              place.friend_request(pending_place)

              #pending_users
              place.friend_request(pending_user)
            end

            it "return status 200 with users and places nearby by default and all friends of current user" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes)

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(place, "api/v1/maps/index",
                  {
                    users: [user, close_user],
                    places: [close_place]
                  }
                )
              )

              expect(response.status).to eq 200
            end

            it "return places" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes.merge(places: 1))

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(place, "api/v1/maps/places", { places: [close_place] })
              )
              expect(response.status).to eq 200
            end

            it "return users" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes.merge(users: 1))

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(place, "api/v1/maps/users", { users: [user, close_user] })
              )
              expect(response.status).to eq 200
            end
          end

          context "place without friends" do
            before(:each) do
              request.headers["Authorization"] = "Bearer #{place_token}"
            end

            it "return status 200 with users and places nearby by default and all friends of current user" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes)

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(close_place, "api/v1/maps/index",
                  {
                    users: [user, close_user], places: [close_place]
                  }
                )
              )

              expect(response.status).to eq 200
            end

            it "return places" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes.merge(places: 1))

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(close_place, "api/v1/maps/places", { places: [close_place] })
              )
              expect(response.status).to eq 200
            end

            it "return users" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes.merge(users: 1))

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(close_place, "api/v1/maps/users", { users: [user, close_user] })
              )
              expect(response.status).to eq 200
            end
          end
        end

        context "user with friends" do
          context "user tries to receive places and users nearby" do
            let(:valid_attributes) { {} }

            before(:each) do
              HasFriendship::Friendship.destroy_all
              request.headers["Authorization"] = "Bearer #{user_token}"

              #place_friends
              user.friend_request(place_friend); place_friend.accept_request(user)

              #user_friends
              user.friend_request(user_friend); user_friend.accept_request(user)

              #requested_places
              requested_place.friend_request(user)

              #requested_users
              requested_user.friend_request(user)

              #pending_places
              user.friend_request(pending_place)

              #pending_users
              user.friend_request(pending_user)
            end

            it "return status 200 with users and places nearby by default and all friends of current user" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes)

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(user, "api/v1/maps/index",
                  {
                    users: [close_user], places: [place, close_place]
                  }
                )
              )
              expect(response.status).to eq (200)
            end

            it "return places" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes.merge(places: 1))

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(user, "api/v1/maps/places",
                  {
                    places: [place, close_place]
                  }
                )
              )
              expect(response.status).to eq 200
            end

            it "return users" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes.merge(users: 1))

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(user, "api/v1/maps/users",
                  {
                    users: [close_user]
                  }
                )
              )
            end
          end
        end

        context "invisible" do
          let!(:user) { create(:user, latitude: 39.000000, longitude: -76.000000, invisible: true) }
          let!(:user_token) { Token.generate(user) }
          let(:valid_attributes) {{}}

          before(:each) do
            HasFriendship::Friendship.destroy_all
            request.headers["Authorization"] = "Bearer #{user_token}"

            #place_friends
            place.friend_request(place_friend); place_friend.accept_request(place)

            #user_friends
            place.friend_request(user_friend); user_friend.accept_request(place)

            #requested_places
            requested_place.friend_request(place)

            #requested_users
            requested_user.friend_request(place)

            #pending_places
            place.friend_request(pending_place)

            #pending_users
            place.friend_request(pending_user)
          end

          it "return only places and violations" do
            get :index, ResponseHelper.add_json_format_to(valid_attributes)

            expect(ResponseHelper.get_parsed_body(response)).to eq(
              rabl_helper(place, "api/v1/maps/invisible",
                {
                  places: [place, close_place], violations: violations, requested_places: [],
                  pending_places: [], places_friends: []
                }
              )
            )
            expect(response.status).to eq 200
          end
        end

        context "user without friends" do
          context "user tries to receive places and users nearby" do
            let(:valid_attributes) { {} }

            before(:each) { request.headers["Authorization"] = "Bearer #{user_token}" }

            it "return status 200 with users and places nearby by default and all friends of current user" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes)

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(close_user, "api/v1/maps/index",
                  {
                    users: [close_user], places: [place, close_place]
                  }
                )
              )
              expect(response.status).to eq (200)
            end

            it "return places" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes.merge(places: 1))

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(close_user, "api/v1/maps/places",
                  {
                    places: [place, close_place]
                  }
                )
              )
              expect(response.status).to eq 200
            end

            it "return users" do
              get :index, ResponseHelper.add_json_format_to(valid_attributes.merge(users: 1))

              expect(ResponseHelper.get_parsed_body(response)).to eq(
                rabl_helper(close_user, "api/v1/maps/users",
                  {
                    users: [close_user]
                  }
                )
              )
            end
          end
        end
      end
    end
  end

  context "without auth" do
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) { request.headers["Authorization"] = "Bearer #{invalid_token}" }

    it "returns 403" do
      get :index, format: :json

      expect(response.status).to eq 403
    end
  end

  def rabl_helper(record, template_path, hash)
    JSON(
      RablRails.render(record, template_path,
        locals: {
          :requested_users => record.user_requested_friends,
          :requested_places => record.place_requested_friends,

          :pending_users => record.user_pending_friends,
          :pending_places => record.place_pending_friends,

          :users_friends => record.user_friends,
          :places_friends => record.place_friends,
          :violations => violations
        }.merge(hash)
      )
    )
  end
end
