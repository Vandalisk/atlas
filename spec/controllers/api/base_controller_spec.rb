require 'rails_helper'

describe Api::BaseController, type: :controller do
  context 'auth' do
    let!(:user) { create(:user) }
    let!(:token) { Token.generate(user) }

    before(:each) { request.headers['Authorization'] = "Bearer #{token}" }

    context 'allowed actions' do
      describe 'GET #index' do
      end

      describe 'POST #create' do
      end
    end

    context 'forbidden actions' do
      describe 'GET #index' do
      end

      describe 'POST #create' do
      end
    end
  end

  context 'without auth' do
    let!(:invalid_token) { Faker::Code.ean }

    before(:each) { request.headers['Authorization'] = "Bearer #{invalid_token}" }
  end
end
