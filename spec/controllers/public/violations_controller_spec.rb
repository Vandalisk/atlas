require 'rails_helper'

RSpec.describe Public::ViolationsController, type: :controller do
  describe "Every user can see violation" do
    let!(:violation) { create(:violation) }
    let(:valid_params) {{ id: violation.id }}

    describe "should return violation by id" do

      it "code 200" do
        get :show, valid_params

        expect(response.status).to eq(200)
      end

      it "correct violation" do
        get :show, valid_params

        expect(response).to render_template(:show)
      end
    end
  end
end
