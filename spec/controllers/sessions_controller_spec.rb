# require 'rails_helper'

# describe SessionsController, type: :controller do
#   let!(:moderator) { create(:moderator) }

#   context "allowed actions" do
#     describe "GET #new" do
#       before(:each) { get :new }

#       it "200 status" do
#         expect(response).to have_http_status(:success)
#       end

#       it "render new" do
#         expect(response).to render_template(:new)
#       end
#     end

#     describe "POST #create" do
#       let(:valid_params) {{ email: moderator.email, password: attributes_for(:moderator)[:password] }}
#       before(:each) { get :create, valid_params }

#       it "302 status" do
#         expect(response).to have_http_status(:found)
#       end

#       it "render to admin root" do
#         expect(response).to redirect_to(admin_root_url)
#       end

#       it "notification message" do
#         expect(flash[:notice]).to eq("Logged in!")
#       end
#     end

#     describe "DELETE #destroy" do
#       before(:each) { login_user(moderator); delete :destroy }

#       it "302 status" do
#         expect(response).to have_http_status(:found)
#       end

#       it "redirect to admin root" do
#          expect(response).to redirect_to(admin_root_url)
#       end

#       it "notification message" do
#         expect(flash[:notice]).to eq("Logged out!")
#       end
#     end
#   end

#   context "forbidden actions" do
#     describe "POST #create" do
#       context "wrond password" do
#         let(:invalid_params) {{ email: moderator.email, password: 'password' }}
#         before(:each) { get :create }

#         it "200 status" do
#           expect(response).to have_http_status(:success)
#         end

#         it "render new" do
#           expect(response).to render_template(:new)
#         end
#       end

#       context "wrond email" do
#         let(:invalid_params) {{ email: 'email@email.com', password: attributes_for(:moderator)[:password] }}
#         before(:each) { get :create }

#         it "200 status" do
#           expect(response).to have_http_status(:success)
#         end

#         it "render new" do
#           expect(response).to render_template(:new)
#         end
#       end
#     end
#   end
# end