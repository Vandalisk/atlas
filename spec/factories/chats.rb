FactoryGirl.define do
  factory :chat do
    association :recipient, factory: :user
    association :sender, factory: :user
  end
end
