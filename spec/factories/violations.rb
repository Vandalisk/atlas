FactoryGirl.define do
  factory :violation do
    title { Faker::Book.title }
    description { Faker::Lorem.paragraph }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    category { rand(0..3) }
    photos { 3.times.collect { Faker::Company.logo } }
    user
  end
end
