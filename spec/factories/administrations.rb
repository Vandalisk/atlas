FactoryGirl.define do
  factory :administration do
    sequence(:title) { |n| "Administration #{n}" }
    description { Faker::Lorem.paragraph }
    sequence(:email) { |n| "person#{n}@example.com" }
  end
end
