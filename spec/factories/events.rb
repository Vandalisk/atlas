FactoryGirl.define do
  factory :event do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    time { Faker::Time.forward(20, :all).utc }
    place
  end
end
