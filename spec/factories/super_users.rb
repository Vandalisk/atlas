FactoryGirl.define do
  factory :super_user do
    sequence(:email) { |n| "person#{n}@example.com" }
    password { 111111 }
    password_confirmation { password }
    setting
  end
end
