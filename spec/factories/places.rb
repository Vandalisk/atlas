FactoryGirl.define do
  factory :place do
    name { Faker::Address.street_name }
    description { Faker::Company.name }
    additional_phone_number { Faker::PhoneNumber.phone_number }
    weekdays_hours { "7:30 - 17:00" }
    weekends_hours { "10:00 - 17:00, 0" }
    address { Faker::Address.street_address }
    user { create(:user, owner: true) }
    max_events_per_day 3
    image_path { Faker::Company.logo }

    trait :invalid_user do
      user { build(:user, phone_number: nil, owner: true) }
    end

    trait :place_registration_attributes do
      name { Faker::Name.name }
      description { Faker::Company.name }
      image_path { Faker::Company.logo }
      additional_phone_number { Faker::PhoneNumber.phone_number }
      user { nil }
      user_attributes {
        attributes_for(:user, :registration_attributes).
        slice(:phone_number, :social_id, :social_marker, :latitude, :longitude)
      }
    end

    trait :place_registration_attributes_with_user do
      name { Faker::Name.name }
      description { Faker::Company.name }
      image_path { Faker::Company.logo }
      additional_phone_number { Faker::PhoneNumber.phone_number }
      user { build(:user) }
    end
  end
end
