FactoryGirl.define do
  factory :comment do
    body { Faker::Lorem.paragraph }
    rating { rand(1..5) }
    user
    place
  end

  trait :empty_body do
    body nil
  end
end
