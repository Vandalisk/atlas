FactoryGirl.define do
  factory :message do
    body { Faker::Lorem.sentence }
    chat
    user_id { chat.sender.id }

    after(:create) { |message| message.send(:mark_read_for_sender) }
  end
end
