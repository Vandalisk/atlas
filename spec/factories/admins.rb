FactoryGirl.define do
  factory :admin, class: Admin do
    sequence(:email) { |n| "person#{n}@example.com" }
    password { 111111 }
    password_confirmation { password }
  end
end
