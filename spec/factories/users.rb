FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    age { Random.rand(18..60) }
    sex { %i[Male Female].sample }
    search_radius_friends { Random.rand(500..1000) }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    invisible { false }
    phone_number { Faker::PhoneNumber.cell_phone }
    social_id { Faker::Code.ean }
    social_marker { Random.rand(0..3) }
    image_path { Faker::Company.logo }
    owner false
  end

  trait :invalid_phone_number do
    phone_number nil
  end

  trait :registration_attributes do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    age { Random.rand(18..60) }
    sex { %i[Male Female].sample }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    phone_number { Faker::PhoneNumber.cell_phone }
    image_path { Faker::Company.logo }
    social_id { Faker::Code.ean }
    social_marker { Random.rand(0..3) }
  end

  trait :find_params do
    phone_number { Faker::PhoneNumber.cell_phone }
    social_id { Faker::Code.ean }
    social_marker { Random.rand(0..3) }
  end
end
