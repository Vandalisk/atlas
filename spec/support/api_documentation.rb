RspecApiDocumentation.configure do |config|
  config.format = :json
  config.curl_host = "sochiatlas.herokuapp.com"
  config.curl_headers_to_filter = %w(Host Cookie)
  config.api_name = 'Atlas API'
end
