module ResponseHelper
  module_function

  def get_parsed_body(response)
    response.body.empty? ? "" : JSON.parse(response.body)
  end

  def expected_item(item, attrs)
    prepare_item(item, attrs)
  end

  def expected_collection(collection, attrs)
    collection.map { |item| prepare_item(item, attrs) }
  end

  def add_json_format_to(params)
    params.merge!(format: :json)
  end

  def object_render(object, template, locals = {})
    JSON(RablRails.render(object, template, locals: locals))
  end

  def collection_render(collection, template, locals = {})
    JSON(RablRails.render(collection, template, locals: locals))
  end

  private

  def self.prepare_item(item, attrs)
    entry = item.attributes.slice(*attrs)
    entry['created_at'] = entry['created_at'].strftime('%d/%m/%Y %H:%M:%S') if entry['created_at']
    entry['updated_at'] = entry['updated_at'].strftime('%d/%m/%Y %H:%M:%S') if entry['updated_at']
    entry['social_marker'] = User.social_markers.key(entry['social_marker']) if entry['social_marker']
    entry['latitude'] =
      if entry['latitude']
        entry['latitude'].to_s
      elsif item.respond_to?(:latitude)
        item.latitude.to_s
      end
    entry['longitude'] =
      if entry['longitude']
        entry['longitude'].to_s
      elsif item.respond_to?(:longitude)
        item.longitude.to_s
      end
    entry['phone_number'] = item.phone_number if item.respond_to?(:phone_number)
    entry['time'] = entry['time'].strftime('%d/%m/%Y %H:%M:%S') if entry['time']
    entry['image_path_url'] = entry['image_path_url'] || item.image_path_url if entry['image_path_url'] || item.respond_to?(:image_path_url)
    entry
  end
end
