require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Users - Find - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:user) { FactoryGirl.create(:user, :registration_attributes) }
  let(:token) { Token.generate(user) }
  let(:new_user) { FactoryGirl.build(:user, :registration_attributes) }

  get "/api/v1/users" do
    parameter :user, required: true, "Type" => "JSON object"
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user,
      "Type" => "String"
    parameter :social_id, "User id from social network", scope: :user, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :user,
      "Type" => "Integer"

    let(:social_id) { new_user.social_id }
    let(:social_marker) { User.social_markers[new_user.social_marker] }

    let(:raw_post) { params.to_json }

    example_request "Finding a user token by social_id & social_marker" do
      explanation "Client can find user token by social_id & social_marker pair or phone_number & social_id.
        This endpoint returns errors if social_id & social_marker pair are invalid or was not found.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"
      expect(status).to eq(404)
    end
  end

  get "/api/v1/users" do
    parameter :user, required: true, "Type" => "JSON object"
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user,
      "Type" => "String"
    parameter :social_id, "User id from social network", scope: :user, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :user,
      "Type" => "Integer"

    let(:phone_number) { new_user.phone_number }
    let(:social_id) { new_user.social_id }

    let(:raw_post) { params.to_json }

    example_request "Finding a user token by phone number & social_id" do
      explanation "Client can find user token by social_id & social_marker pair or phone_number & social_id.
        This endpoint returns errors if phone_number & social_id pair are invalid or was not found.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"
      expect(status).to eq(404)
    end
  end

  get "/api/v1/users" do
    header "Authorization", :token

    response_field :user, "Type" => "JSON object"

    let(:raw_post) { params.to_json }

    example_request "Finding a user by token" do
      explanation "Client can find user by token.
        This endpoint returns 403 if token is invalid or was not found."

      expect(status).to eq(403)
    end
  end

  get "/api/v1/users" do
    header "Authorization", :token

    parameter :first_name, "User first_name", "Type" => "String"
    parameter :last_name, "User last_name", "Type" => "String"

    response_field :user, "Type" => "JSON object"

    let(:first_name) { new_user.first_name }
    let(:last_name) { new_user.last_name }
    let(:attrs) { User::JSON_ATTRS.map(&:to_s) }

    let(:raw_post) { params.to_json }

    example_request "Finding a user by first_name & last_name" do
      explanation "Client can find user by first_name & last_name pair.
        This endpoint returns 403 if first_name & last_name pair are valid or was found."

      expect(status).to eq(403)
    end
  end

  get "/api/v1/users" do
    header "Authorization", :token

    parameter :phone_number, "User first_name", "Type" => "String"

    response_field :user, "Type" => "JSON object"

    let(:phone_number) { new_user.phone_number }
    let(:attrs) { User::JSON_ATTRS.map(&:to_s) }

    let(:raw_post) { params.to_json }

    example_request "Finding a user by phone_number" do
      explanation "Client can find user by phone_number.
        This endpoint returns 403 if phone_number is valid or was found."

      expect(status).to eq(403)
    end
  end
end

resource "Users - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:user) { FactoryGirl.create(:user, :registration_attributes) }
  let(:auth_token) { "Bearer #{token}" }
  let(:token) { Token.generate(user) }

  get "/api/v1/users" do
    parameter :user, required: true, "Type" => "JSON object"
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user,
      "Type" => "String"
    parameter :social_id, "User id from social network", scope: :user, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :user,
      "Type" => "Integer"

    response_field :user, "Type" => "JSON object"
    response_field :token, "Auth token", scope: :user, "Type" => "String"

    let(:social_id) { user.social_id }
    let(:social_marker) { User.social_markers[user.social_marker] }

    let(:raw_post) { params.to_json }

    example_request "Finding a user token by social_id & social_marker" do
      explanation "Client can find user token by social_id & social_marker pair or phone_number & social_id.
        This endpoint returns auth token if social_id & social_marker pair are valid or was found.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      user_token = JSON.parse(response_body)['user']['token']
      expect(user_token).to eq token
      expect(status).to eq(200)
    end
  end

  get "/api/v1/users" do
    parameter :user, required: true, "Type" => "JSON object"
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user,
      "Type" => "String"
    parameter :social_id, "User id from social network", scope: :user, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :user,
      "Type" => "Integer"

    response_field :user, "Type" => "JSON object"
    response_field :token, "Auth token", scope: :user, "Type" => "String"

    let(:phone_number) { user.phone_number }
    let(:social_id) { user.social_id }

    let(:raw_post) { params.to_json }

    example_request "Finding a user token by phone number & social_id" do
      explanation "Client can find user token by social_id & social_marker pair or phone_number & social_id.
        This endpoint returns auth token if phone_number & social_id pair are valid or was found.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      user_token = JSON.parse(response_body)['user']['token']
      expect(user_token).to eq token
      expect(status).to eq(200)
    end
  end

  get "/api/v1/users" do
    header "Authorization", :auth_token

    response_field :user, "Type" => "JSON object"
    response_field :first_name, "User first_name", scope: :user, "Type" => "String"
    response_field :last_name, "User last_name",  scope: :user, "Type" => "String"
    response_field :age, "User age. Should be in range from 1 to 100",
      scope: :user, "Type" => "Integer"
    response_field :sex, "User sex. Should equal Male or Female",
      scope: :user, "Type" => "String"
    response_field :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user, "Type" => "String"
    response_field :image_path, "User image url", scope: :user, "Type" => "String"
    #TODO: Add new fields

    let(:attrs) { User::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a user by token" do
      explanation "Client can find user by token.
        This endpoint returns user json if token is valid or was found."

      expect(JSON(response_body)).to eq JSON RablRails.render(user, 'api/v1/users/item')
      expect(status).to eq(200)
    end
  end

  get "/api/v1/users" do
    header "Authorization", :auth_token

    parameter :first_name, "User first_name", "Type" => "String"
    parameter :last_name, "User last_name", "Type" => "String"

    response_field :user, "Type" => "JSON object"
    response_field :first_name, "User first_name", scope: :user, "Type" => "String"
    response_field :last_name, "User last_name",  scope: :user, "Type" => "String"
    response_field :age, "User age. Should be in range from 1 to 100",
      scope: :user, "Type" => "Integer"
    response_field :sex, "User sex. Should equal Male or Female",
      scope: :user, "Type" => "String"
    response_field :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user, "Type" => "String"
    response_field :image_path, "User image url", scope: :user, "Type" => "String"

    let(:first_name) { user.first_name }
    let(:last_name) { user.last_name }
     let(:attrs) { User::JSON_ATTRS.map(&:to_s) }

    let(:raw_post) { params.to_json }

    example_request "Finding a user by first_name & last_name" do
      explanation "Client can find user by first_name & last_name pair.
        This endpoint returns user if first_name & last_name pair are valid or was found."

      expect(JSON(response_body)).to eq JSON RablRails.render(user, 'api/v1/users/item')
      expect(status).to eq(200)
    end
  end

  get "/api/v1/users" do
    header "Authorization", :auth_token

    parameter :phone_number, "User first_name", "Type" => "String"

    response_field :user, "Type" => "JSON object"
    response_field :first_name, "User first_name", scope: :user, "Type" => "String"
    response_field :last_name, "User last_name",  scope: :user, "Type" => "String"
    response_field :age, "User age. Should be in range from 1 to 100",
      scope: :user, "Type" => "Integer"
    response_field :sex, "User sex. Should equal Male or Female",
      scope: :user, "Type" => "String"
    response_field :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user, "Type" => "String"
    response_field :image_path, "User image url", scope: :user, "Type" => "String"

    let(:phone_number) { user.phone_number }
    let(:attrs) { User::JSON_ATTRS.map(&:to_s) }

     let(:raw_post) { params.to_json }

    example_request "Finding a user by phone_number" do
      explanation "Client can find user by phone_number.
        This endpoint returns user if phone_number is valid or was found."

       expect(JSON(response_body)).to eq JSON RablRails.render(user, 'api/v1/users/item')
       expect(status).to eq(200)
     end
  end
end

resource "Users - Create - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:user) { FactoryGirl.create(:user, :registration_attributes) }

  post "/api/v1/users" do
    parameter :user, required: true, "Type" => "JSON object"
    parameter :first_name, "User first_name", scope: :user, "Type" => "String", required: true
    parameter :last_name, "User last_name",  scope: :user, "Type" => "String", required: true
    parameter :age, "User age. Should be in range from 1 to 100",
      scope: :user, "Type" => "Integer", required: true
    parameter :sex, "User sex. Should equal Male or Female",
      scope: :user, "Type" => "String", required: true
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user, "Type" => "String", required: true
    parameter :image_path, "User upload image", scope: :user, "Type" => "String"
    parameter :remote_image_path_url, "User remote image url", scope: :user, "Type" => "String"
    parameter :social_id, "User id from social network", scope: :user, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :user, "Type" => "Integer", required: true

    response_field :errors, "Array of errors"

    let(:first_name) { nil }
    let(:last_name) { nil }
    let(:age) { nil }
    let(:sex) { nil }
    let(:phone_number) { nil }
    let(:image_path) { nil }
    let(:social_id) { nil }
    let(:social_marker) { nil }

    let(:raw_post) { params.to_json }

    example_request "Creating a user" do
      explanation "Returns errors if required values empty or invalid.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      expect(status).to eq(400)
    end
  end
end

resource "Users - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:user) { FactoryGirl.create(:user, :registration_attributes) }
  let(:token) { Token.generate(user) }
  let(:new_user) { FactoryGirl.attributes_for(:user, :registration_attributes) }

  post "/api/v1/users" do
    parameter :user, required: true, "Type" => "JSON object"
    parameter :first_name, "User first_name", scope: :user, "Type" => "String", required: true
    parameter :last_name, "User last_name",  scope: :user, "Type" => "String", required: true
    parameter :latitude,
      "Place latitude", scope: :user, required: true, "Type" => "String"
    parameter :longitude,
      "Place longitude", scope: :user, required: true, "Type" => "String"
    parameter :age, "User age. Should be in range from 1 to 100",
      scope: :user, "Type" => "Integer", required: true
    parameter :sex, "User sex. Should equal Male or Female",
      scope: :user, "Type" => "String", required: true
    parameter :latitude,
      "Place latitude", scope: :user, required: true, "Type" => "String"
    parameter :longitude,
      "Place longitude", scope: :user, required: true, "Type" => "String"
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user, "Type" => "String", required: true
    parameter :image_path, "User remote image url", scope: :user, "Type" => "String"
    parameter :social_id, "User id from social network", scope: :user, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :user, "Type" => "Integer", required: true

    response_field :user, "Type" => "JSON object"
    response_field :token, "Auth token", :scope => :user, "Type" => "String"

    let(:first_name) { new_user[:first_name] }
    let(:last_name) { new_user[:last_name] }
    let(:latitude) { Faker::Address.latitude }
    let(:longitude) { Faker::Address.longitude }
    let(:age) { new_user[:age] }
    let(:latitude) { new_user[:latitude] }
    let(:longitude) { new_user[:longitude] }
    let(:sex) { new_user[:sex] }
    let(:phone_number) { new_user[:phone_number] }
    let(:image_path) { new_user[:image_path] }
    let(:social_id) { new_user[:social_id] }
    let(:social_marker) { new_user[:social_marker] }

    let(:raw_post) { params.to_json }

    example_request "Create a user with remote url photo." do
      explanation "Create user in db if all server validation passed and returns auth token.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      user_token = JSON.parse(response_body)['user']['token']
      expect(user_token).to eq Token.generate(User.last)
      expect(status).to eq(201)
    end
  end
end

resource "Users - Update - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let(:user) { FactoryGirl.create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:id) { user.id }

  put "/api/v1/users/:id" do
    let(:new_user) { FactoryGirl.attributes_for(:user) }

    parameter :user, required: true, "Type" => "JSON object"
    parameter :first_name, "User first_name", scope: :user, "Type" => "String"
    parameter :last_name, "User last_name",  scope: :user, "Type" => "String"
    parameter :latitude, "Place latitude", scope: :user, "Type" => "String"
    parameter :longitude, "Place longitude", scope: :user, "Type" => "String"
    parameter :age, "User age. Should be in range from 1 to 100", scope: :user, "Type" => "Integer"
    parameter :sex, "User sex. Should equal Male or Female", scope: :user, "Type" => "String"
    parameter :latitude, "Place latitude", scope: :user, "Type" => "String"
    parameter :longitude, "Place longitude", scope: :user, "Type" => "String"
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :user, "Type" => "String"
    parameter :image_path, "User image. Can be uploaded as a remote url.",
      scope: :user, "Type" => "String"
    parameter :social_id, "User id from social network", scope: :user, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :user, "Type" => "Integer"

    let(:first_name) { new_user[:first_name] }
    let(:last_name) { new_user[:last_name] }
    let(:latitude) { Faker::Address.latitude }
    let(:longitude) { Faker::Address.longitude }
    let(:age) { new_user[:age] }
    let(:latitude) { new_user[:latitude] }
    let(:longitude) { new_user[:longitude] }
    let(:sex) { new_user[:sex] }
    let(:phone_number) { new_user[:phone_number] }
    let(:image_path) { new_user[:image_path] }
    let(:social_id) { new_user[:social_id] }
    let(:social_marker) { new_user[:social_marker] }

    let(:raw_post) { params.to_json }

    example_request "Updating a user with remote image url." do
      explanation "Update user with remote image url if all server validation passed."

      expect(status).to eq(200)
    end
  end
end

resource "Users - Update - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let(:user) { FactoryGirl.create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }

  parameter :user, required: true, "Type" => "JSON object"
  parameter :first_name, "User first_name", scope: :user, "Type" => "String"
  parameter :last_name, "User last_name",  scope: :user, "Type" => "String"
  parameter :latitude, "Place latitude", scope: :user, "Type" => "String"
  parameter :longitude, "Place longitude", scope: :user, "Type" => "String"
  parameter :age, "User age. Should be in range from 1 to 100", scope: :user, "Type" => "Integer"
  parameter :sex, "User sex. Should equal Male or Female", scope: :user, "Type" => "String"
  parameter :latitude, "Place latitude", scope: :user, "Type" => "String"
  parameter :longitude, "Place longitude", scope: :user, "Type" => "String"
  parameter :phone_number, "User phone number.
    Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
    scope: :user, "Type" => "String"
  parameter :image_path, "User image. Can be uploaded as a new image or added as remote url.",
    scope: :user, "Type" => "String"
  parameter :social_id, "User id from social network", scope: :user, "Type" => "String"
  parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
    scope: :user, "Type" => "Integer"

  put "/api/v1/users/:id" do
    let(:id) { user.id }
    let(:new_user) { FactoryGirl.attributes_for(:user) }

    let(:first_name) { new_user[:first_name] }
    let(:last_name) { new_user[:last_name] }
    let(:latitude) { Faker::Address.latitude }
    let(:longitude) { Faker::Address.longitude }
    let(:age) { new_user[:age] }
    let(:latitude) { new_user[:latitude] }
    let(:longitude) { new_user[:longitude] }
    let(:sex) { new_user[:sex] }
    let(:phone_number) { new_user[:phone_number] }
    let(:image_path) { new_user[:image_path] }
    let(:social_id) { new_user[:social_id] }
    let(:social_marker) { new_user[:social_marker] }

    let(:raw_post) { params.to_json }

    before { user.destroy }

    example_request "Tries to update deleted user." do
      explanation "Update user with remote image url if all server validation passed."

      expect(status).to eq(404)
    end
  end
end

resource "Users - DELETE - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:user) { FactoryGirl.create(:user, :registration_attributes) }

  delete "/api/v1/users" do
    parameter 'mobile_test',
      "For test purposes. Value of this key can be everything. (example: 1, true, or string).",
      required: true, "Type" => 'Boolean, String, Integer'
    parameter 'phone_number', "User's phone number", required: true, "Type" => "String"

    let(:mobile_test) { true }
    let(:phone_number) { user.phone_number }
    let(:raw_post) { params.to_json }

    example_request "Deleting a user" do
      explanation "Deletes user if all server validation passed.
        Finds user by phone_number"

      expect(status).to eq(200)
    end
  end
end

resource "Users - DELETE - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:user) { create(:user, :registration_attributes) }
  let(:second_user) { build(:user) }

  delete "/api/v1/users" do
    parameter 'mobile_test',
      "For test purposes. Value of this key can be everything. (example: 1, true, or string).",
      required: true, "Type" => 'Boolean, String, Integer'
    parameter 'phone_number', "User's phone number", required: true, "Type" => "String"
    parameter 'social_marker',
      "Social marker's number, which was used during registration(vk: 0, ok: 1, fb: 2, digits: 3)",
      required: true, "Type" => "String, Integer"

    let(:phone_number) { user.phone_number }
    let(:raw_post) { params.to_json }

    example_request "No mobile_test key" do
      explanation "Deletes user if all server validation passed.
        Finds user by phone_number"

      expect(status).to eq(404)
    end
  end

  delete "/api/v1/users" do
    parameter 'mobile_test',
      "For test purposes. Value of this key can be everything. (example: 1, true, or string).",
      required: true, "Type" => 'Boolean, String, Integer'
    parameter 'phone_number', "User's phone number", required: true, "Type" => "String"
    parameter 'social_marker',
      "Social marker's number, which was used during registration(vk: 0, ok: 1, fb: 2, digits: 3)",
      required: true, "Type" => "String, Integer"

    let(:mobile_test) { true }
    let(:social_marker) { User.social_markers[user.social_marker] }
    let(:raw_post) { params.to_json }

    example_request "No phone_number" do
      explanation "Deletes user if all server validation passed.
        Finds user by phone_number & social_marker"

      expect(status).to eq(400)
    end
  end

  delete "/api/v1/users" do
    parameter 'mobile_test',
      "For test purposes. Value of this key can be everything. (example: 1, true, or string).",
      required: true, "Type" => 'Boolean, String, Integer'
    parameter 'phone_number', "User's phone number", required: true, "Type" => "String"
    parameter 'social_marker',
      "Social marker's number, which was used during registration(vk: 0, ok: 1, fb: 2, digits: 3)",
      required: true, "Type" => "String, Integer"

    let(:mobile_test) { true }
    let(:phone_number) { second_user.phone_number }
    let(:social_marker) { User.social_markers[second_user.social_marker] }
    let(:raw_post) { params.to_json }

    example_request "No such user" do
      explanation "Deletes user if all server validation passed.
        Finds user by phone_number & social_marker"

      expect(status).to eq(404)
    end
  end
end


resource "Users - Get - Errors" do
  get "/api/v1/users/:id" do
    example "Show user page" do
      true
    end
  end
end

resource "Users - Get - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:user) { FactoryGirl.create(:user, :registration_attributes) }
  let(:id) { user.id }

  get "/api/v1/users/:id" do
    parameter :id, "User id", "Type" => "String"
    response_field :user, "Type" => "JSON object"

    let(:attrs) { User::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a user by id" do
      explanation "Client can find user by id. This endpoint returns user if id is valid or found."

      expect(JSON(response_body)).to eq JSON RablRails.render(user, 'api/v1/users/item')
      expect(status).to eq(200)
    end
  end
end

resource "Users - Get - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:user) { FactoryGirl.create(:user, :registration_attributes) }
  let(:id) { user.id + 1 }

  get "/api/v1/users/:id" do
    parameter :id, "User id", "Type" => "String"
    response_field :user, "Type" => "JSON object"

    let(:attrs) { User::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a user by id" do
      explanation "Client can find user by id. This endpoint returns user if id is valid or found."

      expect(status).to eq(404)
    end
  end
end
