require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Maps - Find - Success" do
  header "CONTENT_TYPE", "application/json"
  header "ACCEPT", "application/json"
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user, latitude: 39.000000, longitude: -76.000000) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  let(:place_friend) { create(:place) }
  let(:user_friend) { create(:user) }
  let(:requested_place) { create(:place) }
  let(:requested_user) { create(:user) }
  let(:pending_place) { create(:place) }
  let(:pending_user) { create(:user) }

  before(:each) do
    HasFriendship::Friendship.destroy_all
    #place_friends
    user.friend_request(place_friend); place_friend.accept_request(user)

    #user_friends
    user.friend_request(user_friend); user_friend.accept_request(user)

    #requested_places
    requested_place.friend_request(user)

    #requested_users
    requested_user.friend_request(user)

    #pending_places
    user.friend_request(pending_place)

    #pending_users
    user.friend_request(pending_user)
  end

  get "/api/v1/maps" do
    parameter :places, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define retrieving objects. If true return only places nearby current_user,
      requested places, pending places, places friends.",
      "Type" => "Integer, String, Boolean"
    parameter :users, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define retrieving objects. If true return only users nearby current_user,
      requested users, pending users, users friends.",
      "Type" => "Integer, String, Boolean"

    let!(:far_user) { create(:user, latitude: 40.000000,longitude: -77.000000) }
    let!(:far_user_owner) { create(:user, latitude: 40.000000,longitude: -77.000000, owner: true) }
    let!(:far_place) { create(:place, user: far_user_owner) }

    let!(:close_user) { create(:user, latitude: 39.000500,longitude: -76.000000) }
    let!(:close_user_owner) { create(:user, latitude: 39.000500,longitude: -76.000000, owner: true) }
    let!(:close_place) { create(:place, user: close_user_owner) }

    let(:raw_post) { params.to_json }

    response_field :places, "Type" => "JSON object"
    response_field :users, "Type" => "JSON object"

    example_request "Retrieves objects in default radius(500m) near current user and all types of current user friends" do
      explanation "Returns objects in default radius(500m) classified by type within object's area.
      Returns only visible objects.
      If users and places flags are empty returns places and users objects"

      expect(status).to eq(200)
    end
  end


  get "/api/v1/maps" do
    parameter :places, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define retrieving objects. If true return only places nearby current_user,
      requested places, pending places, places friends.",
      "Type" => "Integer, String, Boolean"
    parameter :users, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define retrieving objects. If true return only users nearby current_user,
      requested users, pending users, users friends.",
      "Type" => "Integer, String, Boolean"

    let!(:far_user) { create(:user, latitude: 40.000000,longitude: -77.000000) }
    let!(:far_user_owner) { create(:user, latitude: 40.000000,longitude: -77.000000, owner: true) }
    let!(:far_place) { create(:place, user: far_user_owner) }

    let!(:close_user) { create(:user, latitude: 39.010000,longitude: -75.990000) }
    let!(:close_user_owner) { create(:user, latitude: 39.010000,longitude: -75.990000, owner: true) }
    let!(:close_place) { create(:place, user: close_user_owner) }

    let(:raw_post) { params.to_json }

    response_field :places, "Type" => "JSON object"
    response_field :users, "Type" => "JSON object"

    example_request "Retrieves empty objects in default radius(500m) near current user if near object not present and all types of current user friends" do
      explanation "Returns empty objects in default radius(500m) classified by type within object's area.
      Returns only visible objects.
      If users and places flags are empty returns places and users objects"

      expect(status).to eq(200)
    end
  end
end

resource "Maps - Find - Success" do
  header "CONTENT_TYPE", "application/json"
  header "ACCEPT", "application/json"
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user, latitude: 39.000000, longitude: -76.000000, invisible: true) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  let(:place_friend) { create(:place) }
  let(:user_friend) { create(:user) }
  let(:requested_place) { create(:place) }
  let(:requested_user) { create(:user) }
  let(:pending_place) { create(:place) }
  let(:pending_user) { create(:user) }

  before(:each) do
    HasFriendship::Friendship.destroy_all
    #place_friends
    user.friend_request(place_friend); place_friend.accept_request(user)

    #user_friends
    user.friend_request(user_friend); user_friend.accept_request(user)

    #requested_places
    requested_place.friend_request(user)

    #requested_users
    requested_user.friend_request(user)

    #pending_places
    user.friend_request(pending_place)

    #pending_users
    user.friend_request(pending_user)
  end

  get "/api/v1/maps" do
    parameter :places, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define retrieving objects. If true return only places nearby current_user,
      requested places, pending places, places friends.",
      "Type" => "Integer, String, Boolean"
    parameter :users, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define retrieving objects. If true return only users nearby current_user,
      requested users, pending users, users friends.",
      "Type" => "Integer, String, Boolean"

    let!(:far_user) { create(:user, latitude: 40.000000,longitude: -77.000000) }
    let!(:far_user_owner) { create(:user, latitude: 40.000000,longitude: -77.000000, owner: true) }
    let!(:far_place) { create(:place, user: far_user_owner) }

    let!(:close_user) { create(:user, latitude: 39.000500,longitude: -76.000000) }
    let!(:close_user_owner) { create(:user, latitude: 39.000500,longitude: -76.000000, owner: true) }
    let!(:close_place) { create(:place, user: close_user_owner) }

    let(:raw_post) { params.to_json }

    response_field :places, "Type" => "JSON object"

    example_request "Retrieves objects in default radius(500m) near current user and current_user is invisible" do
      explanation "Returns objects in default radius(500m) classified by type within object's area.
      Returns only visible objects.
      If users and places flags are empty returns places and users objects"

      expect(status).to eq(200)
    end
  end
end
