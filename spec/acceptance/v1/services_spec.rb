require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Services - Find - Success" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:user) { create(:user, latitude: 39.010300, longitude: -75.990050) }
  let!(:users) do
    [
      create(:user, latitude: 39.009981,longitude: -75.989996, owner: true),
      create(:user, latitude: 39.010001,longitude: -75.990000, owner: true),
      create(:user, latitude: 39.010101,longitude: -75.990020, owner: true),
      create(:user, latitude: 39.010201,longitude: -75.990040, owner: true)
    ]
  end

  let!(:places) { users.inject([]) { |memo, user| memo.push(create(:place, user: user)) } }
  let!(:events) { places.inject([]) { |memo, place| memo.push(create(:event, place: place)) } }
  let(:token) { "Bearer #{Token.generate(user)}" }

  get "api/v1/services" do
    response_field :services, "Containes array of events.", "Type" => "JSON object"
      response_field :event, "Current event.", "Type" => "JSON object"
        response_field :place_id, "Event's place id.", scope: [:services, :event], "Type" => "Integer"
        response_field :id, "Event's id.", scope: [:services, :event], "Type" => "Integer"
        response_field :title, "Event's title.", scope: [:services, :event] , "Type" => "String"
        response_field :description, "Event's description.", scope: [:services, :event] , "Type" => "String, Text"
        response_field :time, "Time of event in UTC format.", scope: [:services, :event] , "Type" => "Datetime UTC."
        response_field :created_at, "Event's created_at.", scope: [:services, :event] , "Type" => "Datetime UTC."
      response_field :place, "Event's place.", "Type" => "JSON object"
        response_field :id, "Place's id", scope: [:services, :place], "Type" => "Integer"
        response_field :name, "Place's name", scope: [:services, :place], "Type" => "String"
        response_field :description, "Place's description", scope: [:services, :place], "Type" => "Text"
        response_field :additional_phone_number, "Place's additional phone number.", scope: [:services, :place],
          "Type" => "String"
        response_field :latitude, "Place latitude", scope: [:services, :place], "Type" => "String"
        response_field :longitude, "Place longitude", scope: [:services, :place], "Type" => "String"
        response_field :phone_number, "Phone number used for registration.", scope: [:services, :place],
          "Type" => "String"
        response_field :average_rating, "Avarage rating of place", scope: [:services, :place], "Type" => "Float"
        response_field :image_path, "Place image url", scope: [:services, :place], "Type" => "String"

    example_request "Returns all events nearby within 500 metres." do
      explanation "List of all events nearby within 500 metres"

      expect(status).to eq(200)
    end
  end
end
