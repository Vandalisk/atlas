require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Comments - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }

  get "/api/v1/places/:place_id/comments" do
    response_field :comments, "Type" => "JSON object"

    example_request "Finds place's comments." do
      explanation "Authorized client can receive place's comments.
        This endpoint returns empty if comments are not found."

      expect(status).to eq(200)
    end
  end
end

resource "Comments - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }
  let(:attributes) { attributes_for(:comment) }

  post "/api/v1/places/:place_id/comments" do
    parameter :comment, require: true, 'Type' => 'JSON object'
    parameter :rating, "Current client's estimation. Range: (1..5)",
      require: true, scope: :comment, 'Type' => 'String, Integer'
    parameter :body, "Comment's body. It allows 300 symbols.", scope: :comment, 'Type' => 'String, Text'

    let(:rating) { attributes[:rating] }
    let(:body) { attributes[:body] }

    let(:raw_post) { params.to_json }

    response_field :comments, "Type" => "JSON object"

    example_request "Creates comment." do
      explanation "Authorized client can create comment.
        This endpoint returns errors if comment's params are invalid."

      expect(status).to eq(201)
    end
  end
end

resource "Comments - Create - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let(:place_owner) { create(:user, owner: true) }
  let!(:place) { create(:place, user: place_owner) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }
  let(:attributes) { attributes_for(:comment) }

  post "/api/v1/places/:place_id/comments" do
    parameter :comment, require: true, 'Type' => 'JSON object'
    parameter :rating, "Current client's estimation. Range: (1..5)",
      require: true, scope: :comment, 'Type' => 'String, Integer'
    parameter :body, "Comment's body. It allows 300 symbols.", scope: :comment, 'Type' => 'String, Text'

    let(:rating) { nil }
    let(:body) { attributes[:body] }

    let(:raw_post) { params.to_json }

    response_field :comments, "Type" => "JSON object"

    example_request "Can't create comment." do
      explanation "Authorized client tries create comment.
        This endpoint returns errors if comment's params are invalid."

      expect(status).to eq(400)
    end
  end

  post "/api/v1/places/:place_id/comments" do
    parameter :comment, require: true, 'Type' => 'JSON object'
    parameter :rating, "Current client's estimation. Range: (1..5)",
      require: true, scope: :comment, 'Type' => 'String, Integer'
    parameter :body, "Comment's body. It allows 300 symbols.", scope: :comment, 'Type' => 'String, Text'

    let(:rating) { attributes[:rating] }
    let(:body) { 'c' * 400 }

    let(:raw_post) { params.to_json }

    response_field :comments, "Type" => "JSON object"

    example_request "Can't create comment." do
      explanation "Authorized client tries create comment.
        This endpoint returns errors if comment's params are invalid."

      expect(status).to eq(400)
    end
  end

  post "/api/v1/places/:place_id/comments" do
    parameter :comment, require: true, 'Type' => 'JSON object'
    parameter :rating, "Current client's estimation. Range: (1..5)",
      require: true, scope: :comment, 'Type' => 'String, Integer'
    parameter :body, "Comment's body. It allows 300 symbols.", scope: :comment, 'Type' => 'String, Text'

    let(:rating) { rand(6..50) }
    let(:body) { attributes[:body] }

    let(:raw_post) { params.to_json }

    response_field :comments, "Type" => "JSON object"

    example_request "Can't create comment." do
      explanation "Authorized client tries create comment.
        This endpoint returns errors if comment's params are invalid."

      expect(status).to eq(400)
    end
  end

  post "/api/v1/places/:place_id/comments" do
    parameter :comment, require: true, 'Type' => 'JSON object'
    parameter :rating, "Current client's estimation. Range: (1..5)",
      require: true, scope: :comment, 'Type' => 'String, Integer'
    parameter :body, "Comment's body. It allows 300 symbols.", scope: :comment, 'Type' => 'String, Text'

    let(:rating) { rand(-50..-1) }
    let(:body) { attributes[:body] }

    let(:raw_post) { params.to_json }

    response_field :comments, "Type" => "JSON object"

    example_request "Can't create comment." do
      explanation "Authorized client tries create comment.
        This endpoint returns errors if comment's params are invalid."

      expect(status).to eq(400)
    end
  end
end

resource "Comments - Delete - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let!(:comment) { create(:comment, user: user) }
  let(:place_id) { place.id }
  let(:id) { comment.id }
  let(:attributes) { attributes_for(:comment) }

  delete "/api/v1/places/:place_id/comments/:id" do
    parameter :id, "Comment's id", 'Type' => 'String, Integer'

    example_request "Deletes comment." do
      explanation "Authorized client can delete comment.
        Only owner can delete comment, otherwise it would raise error."

      expect(status).to eq(200)
    end
  end
end

resource "Comments - Delete - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let!(:comment) { create(:comment) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }
  let(:id) { comment.id }
  let(:attributes) { attributes_for(:comment) }

  delete "/api/v1/places/:place_id/comments/:id" do
    parameter :id, "Comment's id", 'Type' => 'String, Integer'

    example_request "Deletes comment." do
      explanation "Authorized client can delete comment.
        Only owner can delete comment, otherwise it would raise error."

      expect(status).to eq(403)
    end
  end
end

resource "Comments - Delete - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }
  let(:id) { rand(1..10) }
  let(:attributes) { attributes_for(:comment) }

  delete "/api/v1/places/:place_id/comments/:id" do
    parameter :id, "Comment's id", 'Type' => 'String, Integer'

    example_request "Deletes comment." do
      explanation "Authorized client can delete comment.
        Only owner can delete comment, if it exists in database. Otherwise it would raise error."

      expect(status).to eq(404)
    end
  end
end