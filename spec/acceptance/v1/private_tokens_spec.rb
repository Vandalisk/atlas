require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Private token" do
  get "/private/tokens/:id" do
    response_field :token, "Type" => "String"
    example "Show token by id" do
      true
    end
  end

  get "/private/tokens" do
    response_field :places, "Type" => "Array of tokens"
    response_field :users, "Type" => "Array of tokens"
    example "Show all tokens" do
      true
    end
  end
end
