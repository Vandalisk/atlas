require 'rails_helper'
require 'rspec_api_documentation/dsl'
# user
resource "Events - Find - Success" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }
  let!(:events) { create_list(:event, 2, place: place) }

  context "Current user is a user" do
    let(:token) { "Bearer #{Token.generate(user)}" }

    get "api/v1/places/:place_id/events" do
      response_field :place, "Place where events are.", "Type" => "JSON object"
      response_field :id, "Place's id", scope: :place, "Type" => "Integer"
      response_field :name, "Place's name", scope: :place, "Type" => "String"
      response_field :description, "Place's description", scope: :place, "Type" => "Text"
      response_field :additional_phone_number, "Place's additional phone number.", scope: :place,
        "Type" => "String"
      response_field :latitude, "Place latitude", scope: :place, "Type" => "String"
      response_field :longitude, "Place longitude", scope: :place, "Type" => "String"
      response_field :phone_number, "Phone number used for registration.", scope: :place,
        "Type" => "String"
      response_field :average_rating, "Avarage rating of place", scope: :place, "Type" => "Float"
      response_field :image_path, "Place image url", scope: :place, "Type" => "String"

      response_field :events, "Object, which contains current place's events", scope: :place,
        'Type' => 'JSON object'
      response_field :id, "Event's id.", scope: [:place, :event], "Type" => "Integer"
      response_field :title, "Event's title.", scope: [:place, :event] , "Type" => "String"
      response_field :description, "Event's description.", scope: [:place, :event] , "Type" => "String, Text"
      response_field :time, "Time of event in UTC format.", scope: [:place, :event] , "Type" => "Datetime UTC."
      response_field :created_at, "Event's created_at.", scope: [:place, :event] , "Type" => "Datetime UTC."

      example_request "User tries to obtain all place's events" do
        explanation "List of all events for place"

        expect(status).to eq(200)
      end
    end
  end
end
resource "Events - Find - Success" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }
  let!(:events) { create_list(:event, 2, place: place) }

  context "Current user is a place" do
    let(:token) { "Bearer #{Token.generate(place)}" }

    get "api/v1/places/:place_id/events" do
      response_field :place, "Place where events are.", "Type" => "JSON object"
      response_field :id, "Place's id", scope: :place, "Type" => "Integer"
      response_field :name, "Place's name", scope: :place, "Type" => "String"
      response_field :description, "Place's description", scope: :place, "Type" => "Text"
      response_field :additional_phone_number, "Place's additional phone number.", scope: :place,
        "Type" => "String"
      response_field :latitude, "Place latitude", scope: :place, "Type" => "String"
      response_field :longitude, "Place longitude", scope: :place, "Type" => "String"
      response_field :phone_number, "Phone number used for registration.", scope: :place,
        "Type" => "String"
      response_field :average_rating, "Avarage rating of place", scope: :place, "Type" => "Float"
      response_field :image_path, "Place image url", scope: :place, "Type" => "String"

      response_field :events, "Object, which contains current place's events", scope: :place,
        'Type' => 'JSON object'
      response_field :id, "Event's id.", scope: [:place, :event], "Type" => "Integer"
      response_field :title, "Event's title.", scope: [:place, :event] , "Type" => "String"
      response_field :description, "Event's description.", scope: [:place, :event] , "Type" => "String, Text"
      response_field :time, "Time of event in UTC format.", scope: [:place, :event] , "Type" => "Datetime UTC."
      response_field :created_at, "Event's created_at.", scope: [:place, :event] , "Type" => "Datetime UTC."

      example_request "Place tries to obtain all place's events" do
        explanation "List of all events for place"

        expect(status).to eq(200)
      end
    end
  end
end

resource "Event - Find - Success" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }
  let!(:event) { create(:event, place: place) }
  let(:id) { event.id }

  context "Current user is a user" do
    let(:token) { "Bearer #{Token.generate(user)}" }

    get "api/v1/places/:place_id/events/:id" do
      response_field :event, "Current event.", "Type" => "JSON object"
      response_field :place_id, "Event's place id.", scope: :event, "Type" => "Integer"
      response_field :id, "Event's id.", scope: :event, "Type" => "Integer"
      response_field :title, "Event's title.", scope: :event , "Type" => "String"
      response_field :description, "Event's description.", scope: :event , "Type" => "String, Text"
      response_field :time, "Time of event in UTC format.", scope: :event , "Type" => "Datetime UTC."
      response_field :created_at, "Event's created_at.", scope: :event , "Type" => "Datetime UTC."

      example_request "User tries to obtain event" do
        explanation "Show current event"

        expect(status).to eq(200)
      end
    end
  end
end

resource "Event - Find - Success" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }
  let!(:event) { create(:event, place: place) }
  let(:id) { event.id }

  context "Current user is a place" do
    let(:token) { "Bearer #{Token.generate(place)}" }

    get "api/v1/places/:place_id/events/:id" do
      response_field :event, "Current event.", "Type" => "JSON object"
      response_field :place_id, "Event's place id.", scope: :event, "Type" => "Integer"
      response_field :id, "Event's id.", scope: :event, "Type" => "Integer"
      response_field :title, "Event's title.", scope: :event , "Type" => "String"
      response_field :description, "Event's description.", scope: :event , "Type" => "String, Text"
      response_field :time, "Time of event in UTC format.", scope: :event , "Type" => "Datetime UTC."
      response_field :created_at, "Event's created_at.", scope: :event , "Type" => "Datetime UTC."

      example_request "Place tries to obtain event" do
        explanation "Show current event"

        expect(status).to eq(200)
      end
    end
  end
end

resource "Events - Create - Success" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }
  let(:event_attributes) { attributes_for(:event) }

  context "Current user is a place" do
    let(:token) { "Bearer #{Token.generate(place)}" }

    post "api/v1/places/:place_id/events" do
      parameter :title, "Title of event", required: true, scope: :event
      parameter :description, "Description of event", required: true, scope: :event
      parameter :time, "Time of event", required: true, scope: :event

      let(:title) { event_attributes[:title] }
      let(:description) { event_attributes[:description] }
      let(:time) { event_attributes[:time] }
      let(:raw_post) { params.to_json }

      example_request "Place tries to create event for place" do
        explanation "Create event for place"

        expect(status).to eq(200)
      end
    end
  end
end

resource "Events - Update - Success" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }
  let!(:event) { create(:event, place: place) }
 let(:token) { "Bearer #{Token.generate(place)}" }

  context "Current user is a place" do
    put "api/v1/places/:place_id/events/:id" do
      parameter :id, "Id of event", required: true
      parameter :title, "Title of event", scope: :event
      parameter :description, "Description of event", scope: :event
      parameter :time, "Time of event", scope: :event

      let(:attributes) { attributes_for(:event) }
      let(:id) { event.id }
      let(:title) { attributes[:title] }
      let(:description) { attributes[:description] }
      let(:time) { attributes[:time] }
      let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['created_at'] }

      let(:raw_post) { params.to_json }

      example_request "Place tries to update event" do
        explanation "Update event"

        expect(status).to eq(200)
      end
    end
  end
end

resource "Events - Delete - Success" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }

  context "Current user is a place" do
    let(:token) { "Bearer #{Token.generate(place)}" }

    delete "api/v1/places/:place_id/events/:id" do
      let!(:event) { create(:event, place: place) }
      let(:id) { event.id }

      parameter :id, "Id of event", required: true

      example_request "Place tries to delete event" do
        explanation "Delete event"

        expect(status).to eq(200)
      end
    end
  end
end

resource "Events - Find - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }

  context "Current user is a user" do
    let(:token) { "Bearer #{Token.generate(user)}" }
    get "api/v1/places/:place_id/events" do
      let!(:events) { create_list(:event, 2, place: place) }

      response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

      before { place.destroy }

      example_request "User tries to get all place's events. No place." do
        explanation "Raise an error, when place is not found."

        expect(status).to eq(404)
      end
    end
  end
end

resource "Events - Find - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }

  context "Current user is a place" do
    let(:token) { "Bearer #{Token.generate(place)}" }
    get "api/v1/places/:place_id/events" do
      let!(:events) { create_list(:event, 2, place: place) }

      response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

      before { place.destroy }

      example_request "Place tries to get all place's events. No place." do
        explanation "Raise an error, when place is not found."

        expect(status).to eq(404)
      end
    end
  end
end

resource "Event - Find - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }

  context "Current user is a user" do
    let(:token) { "Bearer #{Token.generate(user)}" }

    get "api/v1/places/:place_id/events/:id" do
      let!(:event) { create(:event, place: place) }
      let(:id) { event.id }

      response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

      before { place.destroy }

      example_request "User tries to get event. No place." do
        explanation "Raise an error, when place is not found."

        expect(status).to eq(404)
      end
    end
  end
end

resource "Event - Find - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:place_id) { place.id }

  let(:token) { "Bearer #{Token.generate(place)}" }

  get "api/v1/places/:place_id/events/:id" do
    let!(:event) { create(:event, place: place) }
    let(:id) { event.id }

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    before { place.destroy }

    example_request "Place tries to get event. No place." do
      explanation "Raise an error, when place is not found."

      expect(status).to eq(404)
    end
  end
end

resource "Event - Find - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }

  get "api/v1/places/:place_id/events/:id" do
    let(:id) { rand(1..50) }

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    example_request "User tries to get event. No event." do
      explanation "Raise an error, when event is not found."

      expect(status).to eq(404)
    end
  end
end

resource "Events - Create - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }

  post "api/v1/places/:place_id/events" do
    parameter :title, "Title of event", required: true, scope: :event
    parameter :description, "Description of event", required: true, scope: :event
    parameter :time, "Time of event", required: true, scope: :event

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    let(:attributes) { attributes_for(:event) }
    let(:title) { attributes[:title] }
    let(:description) { attributes[:description] }
    let(:time) { attributes[:time] }
    let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['id', 'created_at'] }

    let(:raw_post) { params.to_json }

    example_request "User tries to create event. " do
      explanation "Raise an error, when user tries to create event. Only place can create event."
      expect(status).to eq(404)
    end
  end
end

resource "Events - Create - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }

  post "api/v1/places/:place_id/events" do
    parameter :title, "Title of event", required: true, scope: :event
    parameter :description, "Description of event", required: true, scope: :event
    parameter :time, "Time of event", required: true, scope: :event

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    let(:attributes) { attributes_for(:event) }
    let(:title) { attributes[:title] }
    let(:description) { attributes[:description] }
    let(:time) { attributes[:time] }
    let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['id', 'created_at'] }

    let(:raw_post) { params.to_json }

    before { place.destroy }

    example_request "User tries to create event. No place." do
      explanation "Raise an error, when place is not found."
      expect(status).to eq(404)
    end
  end
end

resource "Events - Create - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(place)}" }
  let(:place_id) { place.id }

  post "api/v1/places/:place_id/events" do
    parameter :title, "Title of event", required: true, scope: :event
    parameter :description, "Description of event", required: true, scope: :event
    parameter :time, "Time of event", required: true, scope: :event

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    let(:attributes) { attributes_for(:event) }
    let(:title) { nil }
    let(:description) { attributes[:description] }
    let(:time) { attributes[:time] }
    let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['id', 'created_at'] }

    let(:raw_post) { params.to_json }

    example_request "Place tries to create event with invalid attributes." do
      explanation "Raise an error, when attributes are invalid."

      expect(status).to eq(400)
    end
  end
end

resource "Events - Update - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }

  put "api/v1/places/:place_id/events/:id" do
    parameter :id, "Id of event", required: true
    parameter :title, "Title of event", scope: :event
    parameter :description, "Description of event", scope: :event
    parameter :time, "Time of event", scope: :event

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    let(:attributes) { attributes_for(:event) }
    let(:id) { rand(1..50) }
    let(:title) { attributes[:title] }
    let(:description) { attributes[:description] }
    let(:time) { attributes[:time] }
    let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['created_at'] }

    let(:raw_post) { params.to_json }

    example_request "User tries to update event for place. No event." do
      explanation "Event doesn't exist in database."
      expect(status).to eq(404)
    end
  end
end

resource "Events - Update - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }

  put "api/v1/places/:place_id/events/:id" do
    let(:event) { create(:event, place: place) }

    parameter :id, "Id of event", required: true
    parameter :title, "Title of event", scope: :event
    parameter :description, "Description of event", scope: :event
    parameter :time, "Time of event", scope: :event

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    let(:attributes) { attributes_for(:event) }
    let(:id) { event.id }
    let(:title) { attributes[:title] }
    let(:description) { attributes[:description] }
    let(:time) { attributes[:time] }
    let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['created_at'] }

    let(:raw_post) { params.to_json }

    example_request "User tries to update event. User can't update event." do
      explanation "Raise an error, when user tries to update event."

      expect(status).to eq(404)
    end
  end
end

resource "Events - Update - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(place)}" }
  let(:place_id) { place.id }

  put "api/v1/places/:place_id/events/:id" do
    let(:event) { create(:event, place: place) }

    parameter :id, "Id of event", required: true
    parameter :title, "Title of event", scope: :event
    parameter :description, "Description of event", scope: :event
    parameter :time, "Time of event", scope: :event

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    let(:attributes) { attributes_for(:event) }
    let(:id) { event.id }
    let(:title) { attributes[:title] }
    let(:description) { nil }
    let(:time) { attributes[:time] }
    let(:attrs) { Event::JSON_ATTRS.map(&:to_s) - ['created_at'] }

    let(:raw_post) { params.to_json }

    example_request "Place tries to update event. Attributes are invalid." do
      explanation "Raise an error, when attributes are invalid."

      expect(status).to eq(400)
    end
  end
end

resource "Events - Delete - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }

  delete "api/v1/places/:place_id/events/:id" do
    let!(:event) { create(:event, place: place) }
    let(:id) { event.id }

    before { place.destroy }

    parameter :id, "Id of event", required: true

    response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

    example_request "User tries to delete event. No place." do
      explanation "Raise an error, when place is not found."

      expect(status).to eq(404)
    end
  end
end

resource "Events - Delete - Errors" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"
  header "Authorization", :token

  let!(:place) { create(:place) }
  let(:user) { create(:user) }
  let(:token) { "Bearer #{Token.generate(user)}" }
  let(:place_id) { place.id }

  response_field :errors, "Explanation of error parameters.", 'Type' => 'Array'

  delete "api/v1/places/:place_id/events/:id" do
    let(:id) { rand(1..50) }

    parameter :id, "Id of event", required: true

    example_request "User tries to delete event. No event." do
      explanation "Raise an error, when event is not found."

      expect(status).to eq(404)
    end
  end
end

def event_attributes(valid_attributes)
  result = valid_attributes.stringify_keys
  result['time'] = result['time'].strftime('%d/%m/%Y %H:%M:%S')
  result
end
