require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "News - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :place_token

  let!(:place) { create(:place, user: create(:user, latitude: 39.010300, longitude: -75.990050)) }
  let!(:place_token) { "Bearer #{Token.generate(place)}" }

  let!(:place1) { create(:place, user: create(:user, owner: true, latitude: 39.010000, longitude: -75.990000)) }
  let!(:place2) { create(:place, user: create(:user, owner: true, latitude: 39.000540, longitude: -75.999990)) }
  let!(:news1) { create_list(:news, 2, place: place1) }
  let!(:news2) { create_list(:news, 2, place: place2) }

  get "/api/v1/news" do
    response_field :news, "Type" => "JSON object"
    response_field :id, "News's id", scope: :news, "Type" => "Integer"
    response_field :title, "News' title", scope: :news, "Type" => "String"
    response_field :description, "News description", scope: :news, "Type" => "Text"
    response_field :latitude, "News latitude", scope: :news, "Type" => "String"
    response_field :longitude, "News longitude", scope: :news, "Type" => "String"
    response_field :place, "Type" => "JSON object"
      response_field :id, "Place's id", scope: [:news, :place], "Type" => "Integer"
      response_field :name, "Place's name", scope: [:news, :place], "Type" => "String"
      response_field :description, "Place's description", scope: [:news, :place], "Type" => "Text"
      response_field :additional_phone_number, "Place's additional phone number.", scope: [:news, :place],
        "Type" => "String"
      response_field :latitude, "Place latitude", scope: [:news, :place], "Type" => "String"
      response_field :longitude, "Place longitude", scope: [:news, :place], "Type" => "String"
      response_field :phone_number, "Phone number used for registration.", scope: [:news, :place],
        "Type" => "String"
      response_field :average_rating, "Avarage rating of place", scope: [:news, :place], "Type" => "Float"
      response_field :image_path, "Place image url", scope: [:news, :place], "Type" => "String"

    example_request "User's news." do
      explanation "User retrieves news, when user is in news' raduis."

      expect(status).to eq(200)
    end
  end
end

resource "News - Find - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'

  let!(:place) { create(:place) }

  let!(:place1) { create(:place, user: create(:user, owner: true, latitude: 39.010000, longitude: -75.990000)) }
  let!(:place2) { create(:place, user: create(:user, owner: true, latitude: 39.000540, longitude: -75.999990)) }
  let!(:news1) { create_list(:news, 2, place: place1) }
  let!(:news2) { create_list(:news, 2, place: place2) }

  get "/api/v1/news" do
    let(:raw_post) { params.to_json }

    example_request "User's news." do
      explanation "User can't retrieve news if not authorized."

      expect(status).to eq(403)
    end
  end
end
