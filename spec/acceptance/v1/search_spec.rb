require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Search - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }
  let!(:category_names) { %w(поесть бары-клубы гостиницы турестические развлечения супорты) }
  let!(:places) { create_list(:place, 2, category: create(:category, title: category_names.first)) }

  get "/api/v1/search" do
    parameter :name, "Name for search", "Type" => "String"
    parameter :categories,
      "Specify categories names. Available: [поесть, бары-клубы, гостиницы, турестические развлечения, супорты]",
      "Type" => "Array"
    parameter :sex, "User's sex. [Male, Female]", "Type" => "String"

    response_field :places, "Type" => "Array"
      response_field :name,
        "Place name(Place title)", scope: :places, "Type" => "String"
      response_field :description,
        "Place description", scope: :places, "Type" => "String"
      response_field :image_path,
        "Place image url",
        scope: :places, "Type" => "String"
      response_field :additional_phone_number,
        "Place official number", scope: :places, "Type" => "String"
      response_field :latitude,
        "Place latitude", scope: :places, "Type" => "String"
      response_field :longitude,
        "Place longitude", scope: :places, "Type" => "String"
      response_field :phone_number,
        "Phone number used for registration.",
        scope: :places, "Type" => "String"
      response_field :address, "Place's address.", scope: :places, "Type" => "String"
      response_field :weekdays_hours, "Place's working hours during the week. Format: (7:00 - 17:30)",
        scope: :places, "Type" => "String"
      response_field :weekends_hours, "Place's working hours during the weekend. Format: (10:00 - 17:30)",
        scope: :places, "Type" => "String"
    response_field :user, "Type" => "JSON object"
      response_field :first_name, "User first_name", scope: :users, "Type" => "String"
      response_field :last_name, "User last_name",  scope: :users, "Type" => "String"
      response_field :age, "User age. Should be in range from 1 to 100",
        scope: :users, "Type" => "Integer"
      response_field :sex, "User sex. Should equal Male or Female",
        scope: :users, "Type" => "String"
      response_field :phone_number, "User phone number.
        Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
        scope: :users, "Type" => "String"
      response_field :image_path, "User image url", scope: :users, "Type" => "String"

    let(:raw_post) { params.to_json }

    example_request "Search users and places by name, sex and categories." do
      explanation "Possibility to search places by name, sex and categories. You can find objects by substring.
      When categories and name are determined - will return all users and places by this name and places by these categories.
      When categories are determined - will return all users and all places nearby by determined categories.
      When name is determined - will return all users and places by this name.
      When sex is determined - will return all users nearby and all places.
      When nothing is determined - will return all places and users."

      expect(status).to eq(200)
    end
  end
end
