require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Violations - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  get "/api/v1/violations" do
    parameter :latitude, "Point's latitude, which we use to determine area of inclusion.",
      required: true,
      "Type" => "String"
    parameter :longitude, "Point's longitude, which we use to determine area of inclusion.",
      required: true,
      "Type" => "String"
    parameter :distance, "Area in metres on map, where objects located", "Type" => "Integer, String"

    response_field :violations, "Type" => "JSON object"

    let(:latitude) { 55.765494 }
    let(:longitude) { 49.149921 }

    let(:close_point) {{ latitude: 55.768591, longitude: 49.165269 }}
    let!(:close_violations) { create_list(:violation, 3, close_point) }
    let(:latitude) { 55.765494 }
    let(:longitude) { 49.149921 }

    let(:raw_post) { params.to_json }

    example_request "Violations within area." do
      explanation "Possibility to retrieve news in area of some point."

      expect(status).to eq(200)
    end
  end
end

resource "Violation - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }
  let!(:violation) { create(:violation) }

  get "/api/v1/violations/:id" do
    parameter :id, "Violation's id.",
      required: true,
      "Type" => "String, Integer"

    response_field :violation, "Type" => "JSON object"

    let(:id) { violation.id }

    let(:raw_post) { params.to_json }

    example_request "Return violation by id." do
      explanation "Possibility to retrieve violation by id."

      expect(status).to eq(200)
    end
  end
end

resource "Violation - Find - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  get "/api/v1/violations/:id" do
    parameter :id, "Violation's id.",
      required: true,
      "Type" => "String, Integer"

    response_field :violation, "Type" => "JSON object"

    let(:id) { rand(100) }

    let(:raw_post) { params.to_json }

    example_request "Violation is not found." do
      explanation "Return error, when violation is not found in database."

      expect(status).to eq(404)
    end
  end
end

resource "Violation - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  post "/api/v1/violations/" do
    parameter :violation, require: true, "Type" => "JSON object"
    parameter :title, "Violation's title.", scope: :user, required: true, "Type" => "String"
    parameter :description, "Violation's description.", scope: :user, required: true,
      "Type" => "String, Text"
    parameter :latitude, "Point's latitude, which we use to determine area of inclusion.",
      scope: :user,required: true, "Type" => "String"
    parameter :longitude, "Point's longitude, which we use to determine area of inclusion.",
      scope: :user, required: true, "Type" => "String"
    parameter :category, "Violation's category.", scope: :user, required: true,
      "Type" => "String, Integer"
    parameter :photos, "Violation's photos.", scope: :user, "Type" => "Array"

    let(:violation) { attributes_for(:violation) }

    let(:raw_post) { params.to_json }

    example_request "Creates violation with photos from remote url." do
      explanation "User can create violation."

      expect(status).to eq(201)
    end
  end
end

resource "Violation - Create - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  post "/api/v1/violations/" do
    parameter :violation, require: true, "Type" => "JSON object"
    parameter :title, "Violation's title.", scope: :user, required: true, "Type" => "String"
    parameter :description, "Violation's description.", scope: :user, required: true,
      "Type" => "String, Text"
    parameter :latitude, "Point's latitude, which we use to determine area of inclusion.",
      scope: :user,required: true, "Type" => "String"
    parameter :longitude, "Point's longitude, which we use to determine area of inclusion.",
      scope: :user, required: true, "Type" => "String"
    parameter :category, "Violation's category.", scope: :user, required: true,
      "Type" => "String, Integer"
    parameter :photos, "Violation's photos.", "Type" => "Array"

    let(:violation) { attributes_for(:violation).slice(:title, :description) }

    let(:raw_post) { params.to_json }

    example_request "Shouldn't create violation." do
      explanation "Should return error message when params missing."

      expect(status).to eq(400)
    end
  end
end

resource "Violation - Destroy - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }
  let!(:violation) { create(:violation) }

  delete "/api/v1/violations/:id" do
    parameter :id, "Violation's id.",
      required: true,
      "Type" => "String, Integer"

    response_field :violation, "Type" => "JSON object"

    let(:id) { violation.id }

    let(:raw_post) { params.to_json }

    example_request "Delete violation." do
      explanation "Destroy violation by id."

      expect(status).to eq(200)
    end
  end
end

resource "Violation - Destroy - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  delete "/api/v1/violations/:id" do
    parameter :id, "Violation's id.",
      required: true,
      "Type" => "String, Integer"

    response_field :violation, "Type" => "JSON object"

    let(:id) { rand(100) }

    let(:raw_post) { params.to_json }

    example_request "Violation is not found." do
      explanation "Return error, when violation is not found in database."

      expect(status).to eq(404)
    end
  end
end
