require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Places - Find - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:place) { create(:place, :place_registration_attributes_with_user, user: create(:user)) }
  let(:token) { Token.generate(place) }
  let(:new_place) { build(:place, :place_registration_attributes_with_user, user: build(:user)) }

  get "/api/v1/places" do
    parameter :place, required: true, "Type" => "JSON object"
    parameter :phone_number, "Place phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :place,
      "Type" => "String"
    parameter :social_id, "Place id from social network", scope: :place, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :place,
      "Type" => "Integer"

    let(:social_id) { new_place.social_id }
    let(:social_marker) { User.social_markers[new_place.social_marker] }

    let(:raw_post) { params.to_json }

    example_request "Finding a place token by social_id & social_marker" do
      explanation "Client can find place token by social_id & social_marker pair or phone_number & social_id.
        This endpoint returns errors if social_id & social_marker pair is invalid or was not found.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"
      expect(status).to eq(404)
    end
  end

  get "/api/v1/places" do
    parameter :place, required: true, "Type" => "JSON object"
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :place,
      "Type" => "String"
    parameter :social_id, "User id from social network", scope: :place, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :place,
      "Type" => "Integer"

    let(:phone_number) { new_place.phone_number }
    let(:social_id) { new_place.social_id }

    let(:raw_post) { params.to_json }

    example_request "Finding a place token by phone number & social_id" do
      explanation "Client can find place token by social_id & social_marker pair or phone_number & social_id.
        This endpoint returns errors if phone_number & social_id pair is invalid or was not found.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"
      expect(status).to eq(404)
    end
  end

  get "/api/v1/places" do
    header "Authorization", :token

    parameter :name, "Place name(Place title)", "Type" => "String"

    response_field :place, "Type" => "JSON object"
    response_field :name,
      "Place name(Place title)", scope: :place, "Type" => "String"
    response_field :description,
      "Place description", scope: :place, "Type" => "String"
    response_field :image_path,
      "Place image url",
      scope: :place, "Type" => "String"
    response_field :additional_phone_number,
      "Place official number", scope: :place, "Type" => "String"
    response_field :latitude,
      "Place latitude", scope: :place, "Type" => "String"
    response_field :longitude,
      "Place longitude", scope: :place, "Type" => "String"
    response_field :phone_number,
      "Phone number used for registration.",
      scope: :place, "Type" => "String"
    #TODO: Add new fields

    let(:name) { new_place.name }
    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by name" do
      explanation "Client can find place by name.
        This endpoint returns 403 if name is invalid or not found."

      expect(status).to eq(403)
    end
  end

  get "/api/v1/places" do
    header "Authorization", :token

    parameter :description, "Place's description", "Type" => "String"

    response_field :place, "Type" => "JSON object"
    parameter :user, scope: :place, "Type" => "JSON object"
    response_field :name,
      "Place name(Place title)", scope: :place, "Type" => "String"
    response_field :description,
      "Place description", scope: :place, "Type" => "String"
    response_field :image_path,
      "Place image url",
      scope: :place, "Type" => "String"
    response_field :additional_phone_number,
      "Place official number", scope: :place, "Type" => "String"
    response_field :latitude,
      "Place latitude", scope: :place, "Type" => "String"
    response_field :longitude,
      "Place longitude", scope: :place, "Type" => "String"
    response_field :phone_number,
      "Phone number used for registration.",
      scope: :place, "Type" => "String"

    let(:description) { new_place.description }
    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by description" do
      explanation "Client can find place by description.
        This endpoint returns 403 if description is invalid or not found."

      expect(status).to eq(403)
    end
  end

  get "/api/v1/places" do
    header "Authorization", :token

    parameter :phone_number, "Place's owner phone_number", "Type" => "String"

    response_field :place, "Type" => "JSON object"
    parameter :user, scope: :place, "Type" => "JSON object"
    response_field :name,
      "Place name(Place title)", scope: :place, "Type" => "String"
    response_field :description,
      "Place description", scope: :place, "Type" => "String"
    response_field :image_path,
      "Place image url",
      scope: :place, "Type" => "String"
    response_field :additional_phone_number,
      "Place official number", scope: :place, "Type" => "String"
    response_field :latitude,
      "Place latitude", scope: :place, "Type" => "String"
    response_field :longitude,
      "Place longitude", scope: :place, "Type" => "String"
    response_field :phone_number,
      "Phone number used for registration.",
      scope: :place, "Type" => "String"

    let(:phone_number) { new_place.phone_number }
    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by phone_number" do
      explanation "Client can find place by phone_number.
        This endpoint returns place if phone_number is valid or found."

      expect(status).to eq(403)
    end
  end

  get "/api/v1/places" do
    header "Authorization", :token

    response_field :place, "Type" => "JSON object"
    parameter :user, scope: :place, "Type" => "JSON object"
    response_field :name,
      "Place name(Place title)", scope: :place, "Type" => "String"
    response_field :description,
      "Place description", scope: :place, "Type" => "String"
    response_field :image_path,
      "Place image url",
      scope: :place, "Type" => "String"
    response_field :additional_phone_number,
      "Place official number", scope: :place, "Type" => "String"
    response_field :latitude,
      "Place latitude", scope: :place, "Type" => "String"
    response_field :longitude,
      "Place longitude", scope: :place, "Type" => "String"
    response_field :phone_number,
      "Phone number used for registration.",
      scope: :place, "Type" => "String"

    let(:raw_post) { params.to_json }

    example_request "Finding a place by token" do
      explanation "Client can find place by token.
        This endpoint returns 403 if token is invalid or not found."

      expect(status).to eq(403)
    end
  end
end

resource "Places - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:place) { create(:place, :place_registration_attributes_with_user, user: create(:user, owner: true)) }
  let(:token) { Token.generate(place) }
  let(:auth_token) { "Bearer #{token}" }
  let(:new_place) { build(:place, :place_registration_attributes_with_user, user: build(:user, owner: true)) }

  get "/api/v1/places" do
    parameter :place, required: true, "Type" => "JSON object"
    parameter :phone_number, "Place phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :place,
      "Type" => "String"
    parameter :social_id, "Place id from social network", scope: :place, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :place,
      "Type" => "Integer"

    response_field :place, "Type" => "JSON object"
    response_field :token, "Auth token", scope: :place, "Type" => "String"

    let(:social_id) { place.social_id }
    let(:social_marker) { User.social_markers[place.social_marker] }

    let(:raw_post) { params.to_json }

    example_request "Finding a place token by social_id & social_marker" do
      explanation "Client can find place token by social_id & social_marker pair or phone_number & social_id.
        This endpoint returns place token if social_id & social_marker pair is valid or found.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      place_token = JSON.parse(response_body)['place']['token']
      expect(place_token).to eq token
      expect(status).to eq(200)
    end
  end

  get "/api/v1/places" do
    parameter :place, required: true, "Type" => "JSON object"
    parameter :phone_number, "User phone number.
      Allow any number format. Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: :place,
      "Type" => "String"
    parameter :social_id, "User id from social network", scope: :place, "Type" => "String"
    parameter :social_marker, "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: :place,
      "Type" => "Integer"

    response_field :place, "Type" => "JSON object"
    response_field :token, "Auth token", scope: :place, "Type" => "String"

    let(:phone_number) { place.phone_number }
    let(:social_id) { place.social_id }

    let(:raw_post) { params.to_json }

    example_request "Finding a place token by phone number & social_id" do
      explanation "Client can find place token by social_id & social_marker pair or phone_number & social_id.
        This endpoint returns place token if phone_number & social_id pair is valid or was found.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      place_token = JSON.parse(response_body)['place']['token']
      expect(place_token).to eq token
      expect(status).to eq(200)
    end
  end

  get "/api/v1/places" do
    header "Authorization", :auth_token

    parameter :name, "Place name(Place title)", "Type" => "String"

    response_field :place, "Type" => "JSON object"
    parameter :user, scope: :place, "Type" => "JSON object"
    response_field :name,
      "Place name(Place title)", scope: :place, "Type" => "String"
    response_field :description,
      "Place description", scope: :place, "Type" => "String"
    response_field :image_path,
      "Place image url",
      scope: :place, "Type" => "String"
    response_field :additional_phone_number,
      "Place official number", scope: :place, "Type" => "String"
    response_field :latitude,
      "Place latitude", scope: :place, "Type" => "String"
    response_field :longitude,
      "Place longitude", scope: :place, "Type" => "String"
    response_field :phone_number,
      "Phone number used for registration.",
      scope: :place, "Type" => "String"

    let(:name) { place.name }
    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by name" do
      explanation "Client can find place by name.
        This endpoint returns place if name is valid or found."

      expect(JSON(response_body)).to eq JSON(RablRails.render(place, 'api/v1/places/item'))
      expect(status).to eq(200)
    end
  end

  get "/api/v1/places" do
    header "Authorization", :auth_token

    parameter :description, "Place's description", "Type" => "String"

    response_field :place, "Type" => "JSON object"
    parameter :user, scope: :place, "Type" => "JSON object"
    response_field :name,
      "Place name(Place title)", scope: :place, "Type" => "String"
    response_field :description,
      "Place description", scope: :place, "Type" => "String"
    response_field :image_path,
      "Place image url",
      scope: :place, "Type" => "String"
    response_field :additional_phone_number,
      "Place official number", scope: :place, "Type" => "String"
    response_field :latitude,
      "Place latitude", scope: :place, "Type" => "String"
    response_field :longitude,
      "Place longitude", scope: :place, "Type" => "String"
    response_field :phone_number,
      "Phone number used for registration.",
      scope: :place, "Type" => "String"

    let(:description) { place.description }
    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by description" do
      explanation "Client can find place by description.
        This endpoint returns place if description is valid or found."

      expect(JSON(response_body)).to eq JSON(RablRails.render(place, 'api/v1/places/item'))
      expect(status).to eq(200)
    end
  end

  get "/api/v1/places" do
    header "Authorization", :auth_token

    parameter :phone_number, "Place's owner phone_number", "Type" => "String"

    response_field :place, "Type" => "JSON object"
    parameter :user, scope: :place, "Type" => "JSON object"
    response_field :name,
      "Place name(Place title)", scope: :place, "Type" => "String"
    response_field :description,
      "Place description", scope: :place, "Type" => "String"
    response_field :image_path,
      "Place image url",
      scope: :place, "Type" => "String"
    response_field :additional_phone_number,
      "Place official number", scope: :place, "Type" => "String"
    response_field :latitude,
      "Place latitude", scope: :place, "Type" => "String"
    response_field :longitude,
      "Place longitude", scope: :place, "Type" => "String"
    response_field :phone_number,
      "Phone number used for registration.",
      scope: :place, "Type" => "String"

    let(:phone_number) { place.phone_number }
    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by phone_number" do
      explanation "Client can find place by phone_number.
        This endpoint returns place if phone_number is valid or found."

      expect(JSON(response_body)).to eq JSON(RablRails.render(place, 'api/v1/places/item'))
      expect(status).to eq(200)
    end
  end

  get "/api/v1/places" do
    header "Authorization", :auth_token

    response_field :place, "Type" => "JSON object"
    parameter :user, scope: :place, "Type" => "JSON object"
    response_field :name,
      "Place name(Place title)", scope: :place, "Type" => "String"
    response_field :description,
      "Place description", scope: :place, "Type" => "String"
    response_field :image_path,
      "Place image url",
      scope: :place, "Type" => "String"
    response_field :additional_phone_number,
      "Place official number", scope: :place, "Type" => "String"
    response_field :latitude,
      "Place latitude", scope: :place, "Type" => "String"
    response_field :longitude,
      "Place longitude", scope: :place, "Type" => "String"
    response_field :phone_number,
      "Phone number used for registration.",
      scope: :place, "Type" => "String"

    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by token" do
      explanation "Client can find place by token.
        This endpoint returns place if token is valid or found."

      expect(JSON(response_body)).to eq JSON(RablRails.render(place, 'api/v1/places/item'))
      expect(status).to eq(200)
    end
  end
end

resource "Places - Create - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:place) { create(:place, :place_registration_attributes_with_user) }
  let(:token) { Token.generate(place) }
  let(:new_place) { build(:place, :place_registration_attributes_with_user, user: build(:user)) }

  post "/api/v1/places" do
    parameter :place, required: true, "Type" => "JSON object"
    parameter :user_attributes, scope: :place, required: true, "Type" => "JSON object"
    parameter :name,
      "Place name(Place title)", required: true, scope: :place, "Type" => "String"
    parameter :description,
      "Place description", scope: :place, required: true, "Type" => "String"
    parameter :additional_phone_number,
      "Place official number", scope: :place, required: true, "Type" => "String"
    parameter :latitude,
      "Place latitude", scope: :place, required: true, "Type" => "String"
    parameter :longitude,
      "Place longitude", scope: :place, required: true, "Type" => "String"
    parameter :phone_number,
      "Phone number for registration and auth.
      Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :social_id,
      "Place id from social network",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :social_marker,
      "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :image_path,
      "Place's image. Can be uploaded as a new image or added as remote url.",
      scope: [:place, :user_attributes], required: true, "Type" => "String"

    let(:name) { nil }
    let(:description) { nil }
    let(:additional_phone_number) { nil }
    let(:image_path) { nil }
    let(:phone_number) { nil }
    let(:social_id) { nil }
    let(:social_marker) { nil }
    let(:latitude) { nil }
    let(:longitude) { nil }

    let(:raw_post) { params.to_json }

    response_field :errors, "Array of errors"

    example_request "Creating a place" do
      explanation "Create place in db if all server validation passed and returns auth token.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      expect(status).to eq(400)
    end
  end
end

resource "Places - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:place) { create(:place, :place_registration_attributes_with_user) }
  let(:token) { Token.generate(place) }
  let(:new_place) { build(:place, :place_registration_attributes_with_user, user: build(:user)) }

  post "/api/v1/places" do
    parameter :place, required: true, "Type" => "JSON object"
    parameter :user_attributes, scope: :place, required: true, "Type" => "JSON object"
    parameter :name,
      "Place name(Place title)", required: true, scope: :place, "Type" => "String"
    parameter :description,
      "Place description", scope: :place, required: true, "Type" => "String"
    parameter :additional_phone_number,
      "Place official number", scope: :place, required: true, "Type" => "String"
    parameter :latitude,
      "Place latitude", scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :longitude,
      "Place longitude", scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :phone_number,
      "Phone number for registration and auth.
      Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :social_id,
      "Place id from social network",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :social_marker,
      "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :image_path,
      "Place's image. Can be uploaded as a new image or added as remote url.",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :weekdays_hours, "Place's working hours during the week. Format: (7:00 - 17:30)",
      scope: :place, "Type" => "String"
    parameter :weekends_hours, "Place's working hours during the weekend. Format: (10:00 - 17:30)",
      scope: :place, "Type" => "String"
    parameter :address, "Place's address", scope: :place, "Type" => "String"

    let(:name) { new_place.name }
    let(:description) { new_place.description }
    let(:additional_phone_number) { new_place.additional_phone_number }
    let(:image_path) { new_place.image_path }
    let(:phone_number) { new_place.phone_number }
    let(:latitude) { new_place.latitude }
    let(:longitude) { new_place.longitude }
    let(:social_id) { new_place.social_id }
    let(:social_marker) { new_place.social_marker }
    let(:weekdays_hours) { new_place.weekdays_hours }
    let(:weekends_hours) { new_place.weekends_hours }
    let(:address) { new_place.address }

    let(:raw_post) { params.to_json }

    response_field :place, "Return new created place."

    example_request "Creating a place" do
      explanation "Create place in db if all server validation passed and returns auth token.

        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      expect(status).to eq(201)
    end
  end
end

resource "Places - Update - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header 'Authorization', :token

  let(:place) { create(:place) }
  let(:token) { "Bearer #{Token.generate(place)}" }
  let(:new_place) { build(:place, :place_registration_attributes_with_user) }

  put "/api/v1/places/:id" do
    let(:id) { place.id }
    let(:new_place) do
      FactoryGirl.build(:place, :place_registration_attributes_with_user, user: build(:user))
    end

    parameter :place, required: true, "Type" => "JSON object"
    parameter :user_attributes, scope: :place, required: true, "Type" => "JSON object"
    parameter :name,
      "Place name(Place title)", required: true, scope: :place, "Type" => "String"
    parameter :description,
      "Place description", scope: :place, required: true, "Type" => "String"
    parameter :additional_phone_number,
      "Place official number", scope: :place, required: true, "Type" => "String"
    parameter :latitude,
      "Place latitude", scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :longitude,
      "Place longitude", scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :phone_number,
      "Phone number for registration and auth.
      Phone number translate to '+XXXXXXXXXXX' format before save in db",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :social_id,
      "Place id from social network",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :social_marker,
      "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
      scope: [:place, :user_attributes], required: true, "Type" => "String"
    parameter :image_path,
      "Place image url",
      scope: [:place, :user], required: true, "Type" => "String"
    parameter :weekdays_hours, "Place's working hours during the week. Format: (7:00 - 17:30)",
      scope: :place, "Type" => "String"
    parameter :weekends_hours, "Place's working hours during the weekend. Format: (10:00 - 17:30)",
      scope: :place, "Type" => "String"
    parameter :address, "Place's address", "Type" => "String"

    let(:name) { new_place.name }
    let(:description) { new_place.description }
    let(:additional_phone_number) { new_place.additional_phone_number }
    let(:phone_number) { new_place.phone_number }
    let(:latitude) { Faker::Address.latitude }
    let(:longitude) { Faker::Address.longitude }
    let(:social_id) { new_place.social_id }
    let(:social_marker) { User.social_markers[new_place.social_marker] }
    let(:address) { new_place.address }
    let(:weekdays_hours) { new_place.weekdays_hours }
    let(:weekends_hours) { new_place.weekends_hours }
    let(:image_path) { new_place.image_path }

    let(:raw_post) { params.to_json }

    response_field :place, "Type" => "JSON object"
    response_field :token, "Auth token", scope: :place, "Type" => "String"

    example_request "Updating a place with remote image url." do
      explanation "Updating a place in db if all server validation passed.
        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      expect(status).to eq(200)
    end
  end
end

resource "Places - Update - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header 'Authorization', :token

  let(:place) { create(:place) }
  let(:token) { "Bearer #{Token.generate(place)}" }
  let(:place2) { create(:place) }
  let(:new_place) { build(:place, :place_registration_attributes_with_user) }

  parameter :place, required: true, "Type" => "JSON object"
  parameter :user_attributes, scope: :place, required: true, "Type" => "JSON object"
  parameter :name,
    "Place name(Place title)", required: true, scope: :place, "Type" => "String"
  parameter :description,
    "Place description", scope: :place, required: true, "Type" => "String"
  parameter :additional_phone_number,
    "Place official number", scope: :place, required: true, "Type" => "String"
  parameter :latitude,
    "Place latitude", scope: [:place, :user_attributes], required: true, "Type" => "String"
  parameter :longitude,
    "Place longitude", scope: [:place, :user_attributes], required: true, "Type" => "String"
  parameter :phone_number,
    "Phone number for registration and auth.
    Phone number translate to '+XXXXXXXXXXX' format before save in db",
    scope: [:place, :user_attributes], required: true, "Type" => "String"
  parameter :social_id,
    "Place id from social network",
    scope: [:place, :user_attributes], required: true, "Type" => "String"
  parameter :social_marker,
    "Social network marker (vk: 0, ok: 1, fb: 2, digits: 3)",
    scope: [:place, :user_attributes], required: true, "Type" => "String"
  parameter :image_path,
    "Place image url",
    scope: [:place, :user_attributes], required: true, "Type" => "String"

  put "/api/v1/places/:id" do
    let(:id) { place.id }

    let(:name) { nil }
    let(:description) { new_place.description }
    let(:additional_phone_number) { new_place.additional_phone_number }
    let(:phone_number) { new_place.phone_number }
    let(:latitude) { Faker::Address.latitude }
    let(:longitude) { Faker::Address.longitude }
    let(:social_id) { new_place.social_id }
    let(:social_marker) { User.social_markers[new_place.social_marker] }
    let(:image_path) { new_place.image_path }

    let(:raw_post) { params.to_json }

    response_field :place, "Type" => "JSON object"
    response_field :token, "Auth token", scope: :place, "Type" => "String"

    example_request "Invalid place attributes. Shouldn't update a place." do
      explanation "Updating a place in db if all server validation passed.
        Phone_number & Social_marker and Social_id & Social_marker this pairs must be unique"

      expect(status).to eq(400)
    end
  end
end

resource "Places - Get - Errors" do
  get "/api/v1/users/:id" do
    example "Show user page" do
      true
    end
  end
end

resource "Places - Get - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let(:place) { create(:place, :place_registration_attributes_with_user, user: create(:user, owner: true)) }
  let(:id) { place.id }

  get "/api/v1/places/:id" do
    parameter :id, "Place id", "Type" => "String"
    response_field :place, "Type" => "JSON object"

    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by id" do
      explanation "Client can find place by id. This endpoint returns place if id is valid or found."

      expect(JSON(response_body)).to eq JSON(RablRails.render(place, 'api/v1/places/item'))
      expect(status).to eq(200)
    end
  end
end

resource "Places - Get - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:place) { create(:place, :place_registration_attributes_with_user, user: create(:user, owner: true)) }
  let(:new_place) { build(:place, :place_registration_attributes_with_user, user: build(:user)) }
  let(:id) { place.id + 1 }

  get "/api/v1/places/:id" do
    parameter :id, "Place id", "Type" => "String"

    let(:attrs) { Place::JSON_ATTRS.map(&:to_s) }
    let(:raw_post) { params.to_json }

    example_request "Finding a place by id" do
      explanation "Client can find place by id. This endpoint returns place if id is valid or found."

      expect(status).to eq(404)
    end
  end
end
