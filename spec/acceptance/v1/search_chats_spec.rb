require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Chats - Get - Success" do
  header "CONTENT_TYPE", "application/json"
  header "ACCEPT", "application/json"
  header "HOST", ""

  let!(:recipient) { create(:place) }

  context "Sender is user" do
    get "/api/v1/search_chats" do
      header "Authorization", :token

      let!(:sender) { create(:user) }
      let!(:token) { "Bearer #{Token.generate(sender)}" }
      let!(:chat) {
        create(:chat, sender: sender, recipient: recipient.user)
      }
      let!(:chat) {
        chat = create(:chat, sender: sender, recipient: recipient.user)
        create(:message, chat: Chat.last, sender: sender)

        chat
      }

      let!(:recipient_phone_number) { recipient.phone_number }
      parameter :recipient_phone_number, "Recipient's phone number.", "Type" => "String"

      let(:raw_post) { params.to_json }

      response_field :chat, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chat
        response_field :users, "Type" => "JSON object", scope: :chat

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chat, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chat, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chat
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chat, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chat, :last_message]
          response_field :sender,"If sender is user", "Type" => "JSON object",
            scope: [:chat, :last_message]

              response_field :id, "User id", scope: [:chat, :last_message, :sender],
                "Type" => "Integer"

              response_field :first_name, "User first_name", scope: [:chat, :last_message, :sender],
                "Type" => "String"

              response_field :last_name, "User last_name", scope: [:chat, :last_message, :sender],
                "Type" => "String"

              response_field :age, "User age",
                scope: [:chat, :last_message, :sender], "Type" => "Integer"

              response_field :sex, "User sex",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :latitude, "User latitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :longitude, "User longitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "User phone number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :image_path, "User image url",
                scope: [:chat, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chat, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chat, :last_message]

      example_request "Allow current user to get chat by recipient phone number. Current user is user." do
        explanation "Allow to get chat by recipient phone number.
          This endpoint returns 200 and chat json if phone number is valid"

        expect(status).to eq(200)
      end
    end
  end

  context "Sender is place" do
    get "/api/v1/search_chats" do
      header "Authorization", :token

      let!(:sender) { create(:place) }
      let!(:token) { "Bearer #{Token.generate(sender)}" }
      let!(:chat) {
        chat = create(:chat, sender: sender.user, recipient: recipient.user)
        create(:message, chat: Chat.last, sender: sender.user)

        chat
      }

      let!(:recipient_phone_number) { recipient.phone_number }

      parameter :recipient_phone_number, "Recipient's phone number.", "Type" => "String"

      let(:raw_post) { params.to_json }

      response_field :chat, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chat
        response_field :users, "Type" => "JSON object", scope: :chat

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chat, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chat, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chat
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chat, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chat, :last_message]

          response_field :sender,"If sender is place", "Type" => "JSON object",
            scope: [:chat, :last_message]

              response_field :id, "Place id", scope: [:chat, :last_message, :sender],
                "Type" => "Integer"

              response_field :name, "Place name(Place title)",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :description, "Place description",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :additional_phone_number, "Place official number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :image_path, "Place image url",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :latitude, "Place latitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :longitude, "Place longitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :average_rating, "Average rating of place",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "Place phone number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chat, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chat, :last_message]

      example_request "Allow current user to get chat by recipient phone number. Current user is place" do
        explanation "Allow to get chat by recipient phone number.

        This endpoint returns 200 and chat json if phone number is valid"

        expect(status).to eq(200)
      end
    end
  end
end
