require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Messages - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let!(:user_token) { "Bearer #{Token.generate(user)}" }
  let!(:place_token) { "Bearer #{Token.generate(place)}" }

  context "Empty array response" do
    context "Sender is user" do
      get "/api/v1/chats/:id/messages" do
        header "Authorization", :user_token

        let!(:chat) { create(:chat, sender: user) }
        let!(:messages) do
          messages = create_list(:message, 2, sender: user, chat: chat)
          messages.first.mark_as_read!(for: user)

          messages
        end

        let!(:id) { chat.id }

        parameter :start_message_id, "Pagination started from this message", "Type" => "Integer"
        parameter :count, "The number of messages to return", "Type" => "Integer"
        parameter :chat_id, "Id of chat with messages", "Type" => "Integer"

        example_request "Finding current user's messages without params and current user is user" do
          explanation "Auth client can find his messages in the chat."

          expect(status).to eq(200)
        end
      end
    end

    context "Sender is place" do
      get "/api/v1/chats/:id/messages" do
        header "Authorization", :place_token

        let!(:sender) { create(:place) }
        let!(:chat) { create(:chat, sender: place.user) }
        let!(:messages) do
          messages = create_list(:message, 2, sender: place.user, chat: chat)
          messages.first.mark_as_read!(for: place.user)

          messages
        end

        let!(:id) { chat.id }

        parameter :start_message_id, "Pagination started from this message", "Type" => "Integer"
        parameter :count, "The number of messages to return", "Type" => "Integer"
        parameter :chat_id, "Id of chat with messages", "Type" => "Integer"

        response_field :messages, "Type" => "JSON object"

        example_request "Finding current user's messages without params and current user is place" do
          explanation "Auth client can find his messages in the chat."

          expect(status).to eq(200)
        end
      end
    end
  end

  context "Non-empty array response" do
    context "Sender is user" do
      get "/api/v1/chats/:id/messages" do
        header "Authorization", :user_token

        let!(:chat) { create(:chat, sender: user) }
        let!(:messages) do
          messages = create_list(:message, 11, sender: user, chat: chat)
          messages.first.mark_as_read!(for: user)

          messages
        end

        let!(:start_message_id) { Message.last.id }
        let!(:count) { Message.all.count }
        let!(:id) { chat.id }

        parameter :start_message_id, "Pagination started from this message", "Type" => "Integer"
        parameter :count, "The number of messages to return", "Type" => "Integer"
        parameter :chat_id, "Id of chat with messages", "Type" => "Integer"

        response_field :count, "Show number of messages for current user", "Type" => "Integer"
        response_field :messages, "Type" => "JSON object"
          response_field :id, "Message id", "Type" => "Integer", scope: :messages
          response_field :body, "Text of the message", "Type" => "String", scope: [:messages]

          response_field :sender,"If sender is user", "Type" => "JSON object",
            scope: [:messages]

              response_field :id, "User id", scope: [:messages, :sender],
                "Type" => "Integer"

              response_field :first_name, "User first_name", scope: [:messages, :sender],
                "Type" => "String"

              response_field :last_name, "User last_name", scope: [:messages, :sender],
                "Type" => "String"

              response_field :age, "User age",
                scope: [:messages, :sender], "Type" => "Integer"

              response_field :sex, "User sex",
                scope: [:messages, :sender], "Type" => "String"

              response_field :latitude, "User latitude",
                scope: [:messages, :sender], "Type" => "String"

              response_field :longitude, "User longitude",
                scope: [:messages, :sender], "Type" => "String"

              response_field :phone_number, "User phone number",
                scope: [:messages, :sender], "Type" => "String"

              response_field :image_path, "User image url",
                scope: [:messages, :sender], "Type" => "String"

          response_field :created_at_millis, "Id of last read message in this chat",
            "Type" => "Integer", scope: [:messages]

          response_field :read, "This flag show state of current message read or unread",
              "Type" => "Boolean", scope: [:messages]

        example_request "Finding current user's messages and current user is user" do
          explanation "Auth client can find his messages in the chat."
          expect(status).to eq(200)
        end
      end
    end

    context "Sender is place" do
      get "/api/v1/chats/:id/messages" do
        header "Authorization", :place_token

        let!(:sender) { create(:place) }
        let!(:chat) { create(:chat, sender: place.user) }
        let!(:messages) do
          messages = create_list(:message, 2, sender: place.user, chat: chat)
          messages.first.mark_as_read!(for: place.user)

          messages
        end

        let!(:start_message_id) { Message.last.id }
        let!(:count) { Message.all.count }
        let!(:id) { chat.id }

        parameter :start_message_id, "Pagination started from this message", "Type" => "Integer"
        parameter :count, "The number of messages to return", "Type" => "Integer"
        parameter :chat_id, "Id of chat with messages", "Type" => "Integer"

        response_field :count, "Show number of messages for current user", "Type" => "Integer"
        response_field :messages, "Type" => "JSON object"
          response_field :id, "Message id", "Type" => "Integer", scope: :messages
          response_field :body, "Text of the message", "Type" => "String", scope: [:messages]

          response_field :sender,"If sender is place", "Type" => "JSON object",
            scope: [:messages]

              response_field :id, "Place id", scope: [:messages, :sender],
                "Type" => "Integer"

              response_field :name, "Place name(Place title)",
                scope: [:messages, :sender], "Type" => "String"

              response_field :description, "Place description",
                scope: [:messages, :sender], "Type" => "String"

              response_field :additional_phone_number, "Place official number",
                scope: [:messages, :sender], "Type" => "String"

              response_field :image_path, "Place image url",
                scope: [:messages, :sender], "Type" => "String"

              response_field :latitude, "Place latitude",
                scope: [:messages, :sender], "Type" => "String"

              response_field :longitude, "Place longitude",
                scope: [:messages, :sender], "Type" => "String"

              response_field :average_rating, "Average rating of place",
                scope: [:messages, :sender], "Type" => "String"

              response_field :phone_number, "Place phone number",
                scope: [:messages, :sender], "Type" => "String"

          response_field :created_at_millis, "Id of last read message in this chat",
            "Type" => "Integer", scope: [:messages]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:messages]

        example_request "Finding current user's messages and current user is place" do
          explanation "Auth client can find his messages in the chat."
          expect(status).to eq(200)
        end
      end
    end
  end
end

resource "Messages - Read - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:sender) { create(:user) }
  let!(:recipient) { create(:user) }
  let!(:external_user) { create(:user) }
  let!(:chat) { create(:chat, sender: sender, recipient: recipient) }
  let!(:messages) { create_list(:message, 2, sender: sender, chat: chat) }
  let!(:token) { "Bearer #{Token.generate(external_user)}" }
  let(:chat_id) { chat.id }
  let(:id) { messages.first.id }

  put "/api/v1/chats/:chat_id/messages/:id" do
    example_request "Can't read message." do
      explanation "Auth client can't read message from chat where user isn't a participant."

      expect(status).to eq(403)
    end
  end
end

resource "Messages - Read - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let!(:chat) { create(:chat, sender: user, recipient: place.user) }
  let!(:user_message) { create(:message, sender: user, chat: chat) }
  let!(:place_message) { create(:message, sender: place.user, chat: chat) }

  let!(:user_token) { "Bearer #{Token.generate(user)}" }
  let!(:place_token) { "Bearer #{Token.generate(place)}" }

  context 'If current user is user' do
    header "Authorization", :user_token

    let(:chat_id) { chat.id }
    let(:id) { user_message.id }


    put "/api/v1/chats/:chat_id/messages/:id" do
      example_request "Mark as read message for user and current user is user" do
        explanation "Auth client can read message in the chat."

        expect(status).to eq(200)
      end
    end
  end

  context 'If current user is place' do
    header "Authorization", :place_token

    let(:chat_id) { chat.id }
    let(:id) { user_message.id }


    put "/api/v1/chats/:chat_id/messages/:id" do
      example_request "Mark as read message for user and current user is place" do
        explanation "Auth client can read message in the chat."

        expect(status).to eq(200)
      end
    end
  end
end

resource "Messages - Create - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:chat) { create(:chat, recipient: user) }
  let!(:messages) { create_list(:message, 2, sender: user, chat: chat) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  post "/api/v1/chats/:chat_id/messages" do
    response_field :messages, "Type" => "JSON object"

    let!(:chat_id) { chat.id }
    let!(:body) { nil }
    let(:raw_post) { params.to_json }

    parameter :message, "Type" => "JSON object", required: true
    parameter :body, "The text of the message", "Type" => "String", scope: :message
    parameter :chat_id, "The chat id where we want create the message",
      "Type" => "Integer", scope: :message

    example_request "Current user can't create message with empty body into exist chat" do
      explanation "Auth client can't create message with empty body"
      expect(status).to eq(400)
    end
  end
end

resource "Messages - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }

  let!(:user_token) { "Bearer #{Token.generate(user)}" }
  let!(:place_token) { "Bearer #{Token.generate(place)}" }

  context 'If current user is user' do
    header "Authorization", :user_token

    let!(:user) { create(:user) }
    let!(:chat) { create(:chat, sender: user) }
    let!(:messages) { create_list(:message, 2, sender: user, chat: chat) }

    post "/api/v1/chats/:chat_id/messages" do
      response_field :messages, "Type" => "JSON object"

      let!(:chat_id) { chat.id }
      let!(:body) { 'Some text' }
      let(:raw_post) { params.to_json }

      parameter :message, "Type" => "JSON object", required: true
      parameter :body, "The text of the message", "Type" => "String", scope: :message
      parameter :chat_id, "The chat id where we want create the message",
        "Type" => "Integer", scope: :message


      response_field :message, "Type" => "JSON object"
        response_field :id, "Message id", "Type" => "Integer", scope: :message
        response_field :body, "Text of the message", "Type" => "String", scope: [:message]

        response_field :sender,"If sender is user", "Type" => "JSON object",
          scope: [:message]

            response_field :id, "User id", scope: [:message, :sender],
              "Type" => "Integer"

            response_field :first_name, "User first_name", scope: [:message, :sender],
              "Type" => "String"

            response_field :last_name, "User last_name", scope: [:message, :sender],
              "Type" => "String"

            response_field :age, "User age",
              scope: [:message, :sender], "Type" => "Integer"

            response_field :sex, "User sex",
              scope: [:message, :sender], "Type" => "String"

            response_field :latitude, "User latitude",
              scope: [:message, :sender], "Type" => "String"

            response_field :longitude, "User longitude",
              scope: [:message, :sender], "Type" => "String"

            response_field :phone_number, "User phone number",
              scope: [:message, :sender], "Type" => "String"

            response_field :image_path, "User image url",
              scope: [:message, :sender], "Type" => "String"

        response_field :created_at_millis, "Id of last read message in this chat",
          "Type" => "Integer", scope: [:message]

        response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:message]

      example_request "Current user cant create message into exist chat and current user is user" do
        explanation "User can create message into chat where user is a paraticipant."

        expect(status).to eq(201)
      end
    end
  end

  context 'If current user is place' do
    header "Authorization", :place_token

    let!(:user) { create(:user) }
    let!(:chat) { create(:chat, recipient: place.user) }
    let!(:messages) { create_list(:message, 2, sender: user, chat: chat) }

    post "/api/v1/chats/:chat_id/messages" do
      response_field :messages, "Type" => "JSON object"

      let!(:chat_id) { chat.id }
      let!(:body) { "Some text" }
      let(:raw_post) { params.to_json }

      parameter :message, "Type" => "JSON object", required: true
      parameter :body, "The text of the message", "Type" => "String", scope: :message
      parameter :chat_id, "The chat id where we want create the message",
        "Type" => "Integer", scope: :message

      response_field :message, "Type" => "JSON object"
        response_field :id, "Message id", "Type" => "Integer", scope: :message
        response_field :body, "Text of the message", "Type" => "String", scope: [:message]

        response_field :sender,"If sender is place", "Type" => "JSON object",
          scope: [:message]

            response_field :id, "Place id", scope: [:message, :sender],
              "Type" => "Integer"

            response_field :name, "Place name(Place title)",
              scope: [:message, :sender], "Type" => "String"

            response_field :description, "Place description",
              scope: [:message, :sender], "Type" => "String"

            response_field :additional_phone_number, "Place official number",
              scope: [:message, :sender], "Type" => "String"

            response_field :image_path, "Place image url",
              scope: [:message, :sender], "Type" => "String"

            response_field :latitude, "Place latitude",
              scope: [:message, :sender], "Type" => "String"

            response_field :longitude, "Place longitude",
              scope: [:message, :sender], "Type" => "String"

            response_field :average_rating, "Average rating of place",
              scope: [:message, :sender], "Type" => "String"

            response_field :phone_number, "Place phone number",
              scope: [:message, :sender], "Type" => "String"

        response_field :created_at_millis, "Id of last read message in this chat",
          "Type" => "Integer", scope: [:message]

        response_field :read, "This flag show state of current message read or unread",
          "Type" => "Boolean", scope: [:message]

      example_request "Current user cant create message into exist chat and current user is place" do
        explanation "User can create message into chat where user is a paraticipant."

        expect(status).to eq(201)
      end
    end
  end
end

