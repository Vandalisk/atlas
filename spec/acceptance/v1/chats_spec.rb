require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Chats - Find - Success" do
  header "CONTENT_TYPE", "application/json"
  header "ACCEPT", "application/json"
  header "HOST", ""

  let!(:user) { create(:user) }
  let!(:place) { create(:place) }
  let!(:user_token) { "Bearer #{Token.generate(user)}" }
  let!(:place_token) { "Bearer #{Token.generate(place)}" }

  context "Empty array response" do
    context "Sender is user" do
      header "Authorization", :user_token

      get "/api/v1/chats" do
        response_field :count, "Show number of chats for current user", "Type" => "Integer"
        response_field :chats, "Type" => "JSON object"
          response_field :id, "Chat id", "Type" => "Integer", scope: :chats
          response_field :users, "Type" => "JSON object", scope: :chats

            response_field :recipient_phone_number, "Recipient phone number",
              "Type" => "String", scope: [:chats, :users]

            response_field :sender_phone_number, "Sender phone number",
              "Type" => "String", scope: [:chats, :users]

          response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
            "Type" => "Integer", scope: :chats

          response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
            "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

          response_field :last_message, "Type" => "JSON object", scope: :chats
            response_field :id, "Last message id", "Type" => "Integer", scope: [:chats, :last_message]
            response_field :body, "Text of the message", "Type" => "String", scope: [:chats, :last_message]
            response_field :sender,"If sender is user", "Type" => "JSON object",
              scope: [:chats, :last_message]

                response_field :id, "User id", scope: [:chats, :last_message, :sender],
                  "Type" => "Integer"

                response_field :first_name, "User first_name", scope: [:chats, :last_message, :sender],
                  "Type" => "String"

                response_field :last_name, "User last_name", scope: [:chats, :last_message, :sender],
                  "Type" => "String"

                response_field :age, "User age",
                  scope: [:chats, :last_message, :sender], "Type" => "Integer"

                response_field :sex, "User sex",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :latitude, "User latitude",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :longitude, "User longitude",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :phone_number, "User phone number",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :image_path, "User image url",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

            response_field :created_at_millis, "Id of last read message in this chat",
              "Type" => "Integer", scope: [:chats, :last_message]

            response_field :read, "This flag show state of current message read or unread",
              "Type" => "Boolean", scope: [:chats, :last_message]

        example_request "Finding current user's chats if chats not present and current user is user" do
          explanation "Auth client can find his chats.
            This endpoint returns chats empty object if chats are not found."
          expect(status).to eq(200)
        end
      end
    end

    context "Sender is place" do
      header "Authorization", :place_token

      get "/api/v1/chats" do
        response_field :count, "Show number of chats for current user", "Type" => "Integer"
        response_field :chats, "Type" => "JSON object"
          response_field :id, "Chat id", "Type" => "Integer", scope: :chats
          response_field :users, "Type" => "JSON object", scope: :chats

            response_field :recipient_phone_number, "Recipient phone number",
              "Type" => "String", scope: [:chats, :users]

            response_field :sender_phone_number, "Sender phone number",
              "Type" => "String", scope: [:chats, :users]

          response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
            "Type" => "Integer", scope: :chats

          response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
            "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

          response_field :last_message, "Type" => "JSON object", scope: :chats
            response_field :id, "Last message id", "Type" => "Integer", scope: [:chats, :last_message]
            response_field :body, "Text of the message", "Type" => "String", scope: [:chats, :last_message]

            response_field :sender,"If sender is place", "Type" => "JSON object",
              scope: [:chats, :last_message]

                response_field :id, "Place id", scope: [:chats, :last_message, :sender],
                  "Type" => "Integer"

                response_field :name, "Place name(Place title)",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :description, "Place description",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :additional_phone_number, "Place official number",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :image_path, "Place image url",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :latitude, "Place latitude",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :longitude, "Place longitude",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :average_rating, "Average rating of place",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

                response_field :phone_number, "Place phone number",
                  scope: [:chats, :last_message, :sender], "Type" => "String"

            response_field :created_at_millis, "Id of last read message in this chat",
              "Type" => "Integer", scope: [:chats, :last_message]

            response_field :read, "This flag show state of current message read or unread",
              "Type" => "Boolean", scope: [:chats, :last_message]


        example_request "Finding current user's chats if chats not present and current user is place" do
          explanation "Auth client can find his chats.
            This endpoint returns chats empty object if chats are not found."
          expect(status).to eq(200)
        end
      end
    end
  end

  context "Non-empty array response" do
    context "Sender is user" do
      header "Authorization", :user_token

      let(:recipient2) { create(:user) }
      let!(:chats) do
        [
          create(:chat, sender: user, recipient: place.user),
          create(:chat, sender: user, recipient: recipient2)
        ]
      end

      let!(:messages) do
        messages = create_list(:message, 3, chat: chats.first, sender: user) +
        create_list(:message, 3, chat: chats.last, sender: place.user)

        chats.first.messages.first.mark_as_read!(for: user)
        chats.last.messages.first.mark_as_read!(for: place.user)

        messages
      end

      response_field :count, "Show number of chats for current user", "Type" => "Integer"
      response_field :chats, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chats
        response_field :users, "Type" => "JSON object", scope: :chats

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chats, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chats, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chats
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chats, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chats, :last_message]
          response_field :sender,"If sender is user", "Type" => "JSON object",
            scope: [:chats, :last_message]

              response_field :id, "User id", scope: [:chats, :last_message, :sender],
                "Type" => "Integer"

              response_field :first_name, "User first_name", scope: [:chats, :last_message, :sender],
                "Type" => "String"

              response_field :last_name, "User last_name", scope: [:chats, :last_message, :sender],
                "Type" => "String"

              response_field :age, "User age",
                scope: [:chats, :last_message, :sender], "Type" => "Integer"

              response_field :sex, "User sex",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :latitude, "User latitude",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :longitude, "User longitude",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "User phone number",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :image_path, "User image url",
                scope: [:chats, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chats, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chats, :last_message]

      get "/api/v1/chats" do
        response_field :chats, "Type" => "JSON object"

        example_request "Finding current user's chats and current user is user" do
          explanation "Auth client can find his chats.
            This endpoint returns chats object if current user have chats."
          expect(status).to eq(200)
        end
      end
    end

    context "Sender is place" do
      header "Authorization", :place_token

      let(:recipient2) { create(:user) }
      let!(:chats) do
        [
          create(:chat, sender: place.user, recipient: user),
          create(:chat, sender: recipient2, recipient: user)
        ]
      end

      let!(:messages) do
        messages = create_list(:message, 3, chat: chats.first, sender: user) +
        create_list(:message, 3, chat: chats.last, sender: place.user)


        chats.first.messages.first.mark_as_read!(for: user)
        chats.last.messages.first.mark_as_read!(for: place.user)

        messages
      end

      response_field :count, "Show number of chats for current user", "Type" => "Integer"
      response_field :chats, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chats
        response_field :users, "Type" => "JSON object", scope: :chats

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chats, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chats, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chats
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chats, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chats, :last_message]

          response_field :sender,"If sender is place", "Type" => "JSON object",
            scope: [:chats, :last_message]

              response_field :id, "Place id", scope: [:chats, :last_message, :sender],
                "Type" => "Integer"

              response_field :name, "Place name(Place title)",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :description, "Place description",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :additional_phone_number, "Place official number",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :image_path, "Place image url",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :latitude, "Place latitude",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :longitude, "Place longitude",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :average_rating, "Average rating of place",
                scope: [:chats, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "Place phone number",
                scope: [:chats, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chats, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chats, :last_message]


      get "/api/v1/chats" do
        response_field :chats, "Type" => "JSON object"

        example_request "Finding current user's chats and current user is place" do
          explanation "Auth client can find his chats.
            This endpoint returns chats object if current user have chats."
          expect(status).to eq(200)
        end
      end
    end
  end
end

resource "Chats - Get - Errors" do
  header "CONTENT_TYPE", "application/json"
  header "ACCEPT", "application/json"
  header "HOST", ""
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  get "/api/v1/chats/show" do
    parameter :id, "Chat's id.", "Type" => "Integer"
    let!(:id) { 1 }

    let(:raw_post) { params.to_json }

    example_request "Tries to get chat by id." do
      explanation "Tries to get chat by id which doesn't exist in database.

      This endpoint returns 404 error if client sends is invalid id"

      expect(status).to eq(404)
    end
  end
end

resource "Chats - Get - Success" do
  header "CONTENT_TYPE", "application/json"
  header "ACCEPT", "application/json"
  header "HOST", ""

  let!(:recipient) { create(:place) }

  context "Sender is user" do
    get "/api/v1/chats/:id" do
      header "Authorization", :token

      let!(:sender) { create(:user) }
      let!(:token) { "Bearer #{Token.generate(sender)}" }
      let!(:chat) {
        create(:chat, sender: sender, recipient: recipient.user)
      }
      let!(:chat) {
        chat = create(:chat, sender: sender, recipient: recipient.user)
        create(:message, chat: Chat.last, sender: sender)

        chat
      }
      let!(:id) { chat.id }

      parameter :id, "Chat's id.", "Type" => "Integer"

      let(:raw_post) { params.to_json }

      response_field :chat, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chat
        response_field :users, "Type" => "JSON object", scope: :chat

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chat, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chat, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chat
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chat, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chat, :last_message]
          response_field :sender,"If sender is user", "Type" => "JSON object",
            scope: [:chat, :last_message]

              response_field :id, "User id", scope: [:chat, :last_message, :sender],
                "Type" => "Integer"

              response_field :first_name, "User first_name", scope: [:chat, :last_message, :sender],
                "Type" => "String"

              response_field :last_name, "User last_name", scope: [:chat, :last_message, :sender],
                "Type" => "String"

              response_field :age, "User age",
                scope: [:chat, :last_message, :sender], "Type" => "Integer"

              response_field :sex, "User sex",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :latitude, "User latitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :longitude, "User longitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "User phone number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :image_path, "User image url",
                scope: [:chat, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chat, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chat, :last_message]

      example_request "Allow to current user get chat by id and current user is user" do
        explanation "Allow to get chat by id. This endpoint returns 200 and chat json if id is valid"

        expect(status).to eq(200)
      end
    end
  end

  context "Sender is place" do
    get "/api/v1/chats/:id" do
      header "Authorization", :token

      let!(:sender) { create(:place) }
      let!(:token) { "Bearer #{Token.generate(sender)}" }
      let!(:chat) {
        chat = create(:chat, sender: sender.user, recipient: recipient.user)
        create(:message, chat: Chat.last, sender: sender.user)

        chat
      }

      let!(:id) { chat.id }

      parameter :id, "Chat's id.", "Type" => "Integer"

      let(:raw_post) { params.to_json }

      response_field :chat, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chat
        response_field :users, "Type" => "JSON object", scope: :chat

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chat, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chat, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chat
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chat, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chat, :last_message]

          response_field :sender,"If sender is place", "Type" => "JSON object",
            scope: [:chat, :last_message]

              response_field :id, "Place id", scope: [:chat, :last_message, :sender],
                "Type" => "Integer"

              response_field :name, "Place name(Place title)",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :description, "Place description",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :additional_phone_number, "Place official number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :image_path, "Place image url",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :latitude, "Place latitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :longitude, "Place longitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :average_rating, "Average rating of place",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "Place phone number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chat, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chat, :last_message]

      example_request "Allow to current user get chat by id and current user is place" do
        explanation "Allow to get chat by id.

        This endpoint returns 200 and chat json if id is valid"

        expect(status).to eq(200)
      end
    end
  end
end

resource "Chats - Create - Errors" do
  header "CONTENT_TYPE", "application/json"
  header "ACCEPT", "application/json"
  header "HOST", ""
  header "Authorization", :token

  let!(:sender) { create(:user) }
  let!(:recipient) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(sender)}" }

  post "/api/v1/chats" do
    parameter :chat, require: true, "Type" => "JSON Object"
    parameter :recipient_phone_number, "Recipient phone number",
      require: true, "Type" => "String", scope: :chat

    parameter :body, "Text of the first message in the chat",
      require: true, "Type" => "String", scope: :chat

    let(:recipient_phone_number) { nil }
    let(:body) { nil }

    let(:raw_post) { params.to_json }

    example_request "Tries to create chat." do
      explanation "Tries to create chat between current user and recipient when users doesn't exist in
      database and body is empty.

      This endpoint returns errors if client sends an invalid data"

      expect(status).to eq(404)
    end
  end
end

resource "Chats - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:recipient) { create(:user) }

  context "Sender is user" do
    header "Authorization", :token

    let!(:sender) { create(:user) }
    let!(:recipient) { create(:user) }
    let!(:token) { "Bearer #{Token.generate(sender)}" }

    post "/api/v1/chats" do
      parameter :chat, require: true, "Type" => "JSON Object"
      parameter :recipient_phone_number, "Recipient phone number",
        require: true, "Type" => "String", scope: :chat

      parameter :body, "Text of the first message in the chat",
        require: true, "Type" => "String", scope: :chat

      let(:recipient_phone_number) { recipient.phone_number }
      let(:body) { "Hasta la vista, baby" }

      let(:raw_post) { params.to_json }

      response_field :chat, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chat
        response_field :users, "Type" => "JSON object", scope: :chat

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chat, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chat, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chat
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chat, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chat, :last_message]
          response_field :sender,"If sender is user", "Type" => "JSON object",
            scope: [:chat, :last_message]

              response_field :id, "User id", scope: [:chat, :last_message, :sender],
                "Type" => "Integer"

              response_field :first_name, "User first_name", scope: [:chat, :last_message, :sender],
                "Type" => "String"

              response_field :last_name, "User last_name", scope: [:chat, :last_message, :sender],
                "Type" => "String"

              response_field :age, "User age",
                scope: [:chat, :last_message, :sender], "Type" => "Integer"

              response_field :sex, "User sex",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :latitude, "User latitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :longitude, "User longitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "User phone number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :image_path, "User image url",
                scope: [:chat, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chat, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chat, :last_message]

      example_request "Create chat from current user and current user is user and recipient is a user" do
        explanation "Auth client can create chat between self and one recipient.

        This endpoint returns 201 and chat json if chat is created"

        expect(status).to eq(201)
      end
    end
  end

  context "Sender is place" do
    header "Authorization", :token

    let!(:sender) { create(:place) }
    let!(:recipient) { create(:user) }
    let!(:token) { "Bearer #{Token.generate(sender)}" }

    post "/api/v1/chats" do
      parameter :chat, require: true, "Type" => "JSON Object"
      parameter :recipient_phone_number, "Recipient phone number",
        require: true, "Type" => "String", scope: :chat

      parameter :body, "Text of the first message in the chat",
        require: true, "Type" => "String", scope: :chat

      let(:recipient_phone_number) { recipient.phone_number }
      let(:body) { "Hasta la vista, baby" }

      let(:raw_post) { params.to_json }

      response_field :chat, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chat
        response_field :users, "Type" => "JSON object", scope: :chat

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chat, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chat, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chat
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chat, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chat, :last_message]

          response_field :sender,"If sender is place", "Type" => "JSON object",
            scope: [:chat, :last_message]

              response_field :id, "Place id", scope: [:chat, :last_message, :sender],
                "Type" => "Integer"

              response_field :name, "Place name(Place title)",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :description, "Place description",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :additional_phone_number, "Place official number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :image_path, "Place image url",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :latitude, "Place latitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :longitude, "Place longitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :average_rating, "Average rating of place",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "Place phone number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chat, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chat, :last_message]

      example_request "Create chat from current user and current user is place and recipient is a user" do
        explanation "Auth client can create chat between self and one recipient.

        This endpoint returns 201 and chat json if chat is created"

        expect(status).to eq(201)
      end
    end
  end
end

resource "Chats - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''

  let!(:recipient) { create(:user) }

  context "Sender is user" do
    header "Authorization", :token

    let!(:sender) { create(:user) }
    let!(:recipient) { create(:place) }
    let!(:token) { "Bearer #{Token.generate(sender)}" }

    post "/api/v1/chats" do
      parameter :chat, require: true, "Type" => "JSON Object"
      parameter :recipient_phone_number, "Recipient phone number",
        require: true, "Type" => "String", scope: :chat

      parameter :body, "Text of the first message in the chat",
        require: true, "Type" => "String", scope: :chat

      let(:recipient_phone_number) { recipient.phone_number }
      let(:body) { "Hasta la vista, baby" }

      let(:raw_post) { params.to_json }

      response_field :chat, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chat
        response_field :users, "Type" => "JSON object", scope: :chat

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chat, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chat, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chat
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chat, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chat, :last_message]
          response_field :sender,"If sender is user", "Type" => "JSON object",
            scope: [:chat, :last_message]

              response_field :id, "User id", scope: [:chat, :last_message, :sender],
                "Type" => "Integer"

              response_field :first_name, "User first_name", scope: [:chat, :last_message, :sender],
                "Type" => "String"

              response_field :last_name, "User last_name", scope: [:chat, :last_message, :sender],
                "Type" => "String"

              response_field :age, "User age",
                scope: [:chat, :last_message, :sender], "Type" => "Integer"

              response_field :sex, "User sex",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :latitude, "User latitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :longitude, "User longitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "User phone number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :image_path, "User image url",
                scope: [:chat, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chat, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chat, :last_message]

      example_request "Create chat from current user and current user is user and recipient is a place" do
        explanation "Auth client can create chat between self and one recipient.

        This endpoint returns 201 and chat json if chat is created"

        expect(status).to eq(201)
      end
    end
  end

  context "Sender is place" do
    header "Authorization", :token

    let!(:sender) { create(:place) }
    let!(:recipient) { create(:place) }
    let!(:token) { "Bearer #{Token.generate(sender)}" }

    post "/api/v1/chats" do
      parameter :chat, require: true, "Type" => "JSON Object"
      parameter :recipient_phone_number, "Recipient phone number",
        require: true, "Type" => "String", scope: :chat

      parameter :body, "Text of the first message in the chat",
        require: true, "Type" => "String", scope: :chat

      let(:recipient_phone_number) { recipient.phone_number }
      let(:body) { "Hasta la vista, baby" }

      let(:raw_post) { params.to_json }

      response_field :chat, "Type" => "JSON object"
        response_field :id, "Chat id", "Type" => "Integer", scope: :chat
        response_field :users, "Type" => "JSON object", scope: :chat

          response_field :recipient_phone_number, "Recipient phone number",
            "Type" => "String", scope: [:chat, :users]

          response_field :sender_phone_number, "Sender phone number",
            "Type" => "String", scope: [:chat, :users]

        response_field :unread_sender_messages_count, "Show number of unread messages in this chat for sender",
          "Type" => "Integer", scope: :chats

        response_field :unread_recipient_messages_count, "Show common number of unread messages in this chat for recipient",
          "Type" => "Integer", scope: :chats

        response_field :last_read_message_id, "Id of last read message in this chat.
          0 if all messages in chat not reading",
          "Type" => "Integer", scope: :chat

        response_field :last_message, "Type" => "JSON object", scope: :chat
          response_field :id, "Last message id", "Type" => "Integer", scope: [:chat, :last_message]
          response_field :body, "Text of the message", "Type" => "String", scope: [:chat, :last_message]

          response_field :sender,"If sender is place", "Type" => "JSON object",
            scope: [:chat, :last_message]

              response_field :id, "Place id", scope: [:chat, :last_message, :sender],
                "Type" => "Integer"

              response_field :name, "Place name(Place title)",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :description, "Place description",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :additional_phone_number, "Place official number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :image_path, "Place image url",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :latitude, "Place latitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :longitude, "Place longitude",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :average_rating, "Average rating of place",
                scope: [:chat, :last_message, :sender], "Type" => "String"

              response_field :phone_number, "Place phone number",
                scope: [:chat, :last_message, :sender], "Type" => "String"

          response_field :created_at_millis, "Created at timestamp in millis",
            "Type" => "Integer", scope: [:chat, :last_message]

          response_field :read, "This flag show state of current message read or unread",
            "Type" => "Boolean", scope: [:chat, :last_message]

      example_request "Create chat from current user and current user is place and recipient is a place" do
        explanation "Auth client can create chat between self and one recipient.

        This endpoint returns 201 and chat json if chat is created"

        expect(status).to eq(201)
      end
    end
  end
end
