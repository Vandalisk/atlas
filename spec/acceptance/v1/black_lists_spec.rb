require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Black_lists - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:place2) { create(:place) }
  let!(:black_list) do
    create_list(:black_list, 2, place: place, user: place2.user) +
    create_list(:black_list, 2, place: place)
  end

  get "/api/v1/black_lists" do
    parameter :places, "Return only places", "Type" => "Boolean"
    parameter :users, "Return only users", "Type" => "Boolean"

    response_field :users, "Type" => "Array"
    response_field :places, "Type" => "Array"

    example_request "Obtain objects from black list." do
      explanation "Possibility to retrieve current place's objects from black list."

      expect(status).to eq(200)
    end
  end
end

resource "Black_list - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:place2) { create(:place) }
  let!(:black_list) { create_list(:black_list, 2, place: place, user: place2.user) }

  get "/api/v1/black_lists/:phone_number" do
    let(:phone_number) { place2.phone_number }
    let(:raw_post) { params.to_json }

    parameter :phone_number, "User's or place's phone number for check", "Type" => "String"

    response_field :blocked, "Returns true if blocked. False otherwise.", "Type" => "Boolean"

    example_request "Checks if user is in the black list." do
      explanation "Possibility to retrieve current place's objects from black list."

      expect(status).to eq(200)
    end
  end
end

resource "Black_list - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:place2) { create(:place) }
  let!(:black_list) { create_list(:black_list, 2, place: place, user: user) }

  get "/api/v1/black_lists/:phone_number" do
    let(:phone_number) { place2.phone_number }
    let(:raw_post) { params.to_json }

    parameter :phone_number, "User's or place's phone number for check", "Type" => "String"

    response_field :blocked, "Returns true if blocked. False otherwise.", "Type" => "Boolean"

    example_request "Checks if place is in the black list." do
      explanation "Possibility to retrieve current place's objects from black list."

      expect(status).to eq(200)
    end
  end
end

resource "Black_list - Find - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:place2) { create(:place) }
  let!(:black_list) { create_list(:black_list, 2, place: place, user: user) }

  get "/api/v1/black_lists/:phone_number" do
    let(:phone_number) { Faker::PhoneNumber.phone_number }
    let(:raw_post) { params.to_json }

    parameter :phone_number, "User's or place's phone number for check", "Type" => "String"

    response_field :blocked, "Returns true if blocked. False otherwise.", "Type" => "Boolean"

    example_request "Checks if object is in the black list." do
      explanation "Returns error if no such user or place in the database"

      expect(status).to eq(404)
    end
  end
end

resource "Black_list - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:place2) { create(:place) }

  let!(:black_list) { create_list(:black_list, 2, place: place, user: place2.user) }

  post "/api/v1/black_lists" do
    let(:phone_number) { place2.phone_number }
    let(:raw_post) { params.to_json }

    parameter :black_list, "Type" => "JSON object"
    parameter :phone_number, "User's phone number to add to black list.", scope: :black_list,
      "Type" => "String"

    example_request "Adds place to current place's black list." do
      explanation "Possibility to add place to current place's black list."

      expect(status).to eq(200)
    end
  end
end

resource "Black_list - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:user) { create(:user) }

  let!(:black_list) { create_list(:black_list, 2, place: place, user: user) }

  post "/api/v1/black_lists" do
    let(:phone_number) { user.phone_number }
    let(:raw_post) { params.to_json }

    parameter :black_list, "Type" => "JSON object"
    parameter :phone_number, "User's phone number to add to black list.", scope: :black_list,
      "Type" => "String"

    example_request "Adds user to current place's black list." do
      explanation "Possibility to add user to current place's black list."

      expect(status).to eq(200)
    end
  end
end

resource "Black_list - Create - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:place2) { create(:place) }

  let!(:black_list) { create_list(:black_list, 2, place: place, user: place2.user) }

  post "/api/v1/black_lists" do
    let(:phone_number) { Faker::PhoneNumber.cell_phone }
    let(:raw_post) { params.to_json }

    parameter :black_list, "Type" => "JSON object"
    parameter :phone_number, "User's phone number to add to black list.", scope: :black_list,
      "Type" => "String"

    example_request "Adds place to current place's black list." do
      explanation "Returns error, when user is not found in the database."

      expect(status).to eq(404)
    end
  end
end

resource "Black_list - Destroy - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:user) { create(:user) }
  let!(:black_list) { create_list(:black_list, 2, place: place, user: user) }

  delete "/api/v1/black_lists/:phone_number" do
    let(:phone_number) { user.phone_number }
    let(:raw_post) { params.to_json }

    parameter :phone_number, "User's phone number for check", "Type" => "String"

    response_field :blocked, "Returns true if blocked. False otherwise.", "Type" => "Boolean"

    example_request "Delete user from black list." do
      explanation "Possibility to delete user from current place's black list."

      expect(status).to eq(200)
    end
  end
end

resource "Black_list - Destroy - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:place2) { create(:place) }
  let!(:black_list) { create_list(:black_list, 2, place: place, user: place2.user) }

  delete "/api/v1/black_lists/:phone_number" do
    let(:phone_number) { place2.phone_number }
    let(:raw_post) { params.to_json }

    parameter :phone_number, "Place's phone number for check", "Type" => "String"

    response_field :blocked, "Returns true if blocked. False otherwise.", "Type" => "Boolean"

    example_request "Delete place from black list." do
      explanation "Possibility to delete place from current place's black list."

      expect(status).to eq(200)
    end
  end
end

resource "Black_list - Destroy - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:place2) { create(:place) }
  let!(:black_list) { create_list(:black_list, 2, place: place, user: place2.user) }

  delete "/api/v1/black_lists/:phone_number" do
    let(:phone_number) { Faker::PhoneNumber.phone_number }
    let(:raw_post) { params.to_json }

    parameter :phone_number, "Place's phone number for check", "Type" => "String"

    response_field :blocked, "Returns true if blocked. False otherwise.", "Type" => "Boolean"

    example_request "Delete place from black list." do
      explanation "Returns error if no such user or place in the database."

      expect(status).to eq(404)
    end
  end
end
