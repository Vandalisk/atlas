require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Friendships - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  before(:each) do
    HasFriendship::Friendship.destroy_all
  end

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  context "Empty array response" do
    get "/api/v1/friendships" do
      parameter :requested, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        If value is empty it sets to false by default", "Type" => "Integer, String, Boolean"

      let(:requested) { '1' }
      let(:raw_post) { params.to_json }

      response_field :requested_users, "Type" => "JSON object"
      response_field :requested_places, "Type" => "JSON object"

      example_request "Finding current user requested friends if friends not present" do
        explanation "Auth client can find current user requested or pending friends.
          This endpoint returns empty if friends not found."
        expect(status).to eq(200)
      end
    end

    get "/api/v1/friendships" do
      parameter :pending, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        If value is empty it sets to false by default", "Type" => "Integer, String, Boolean"

      let(:pending) { '1' }
      let(:raw_post) { params.to_json }

      response_field :requested_users, "Type" => "JSON object"
      response_field :requested_places, "Type" => "JSON object"

      example_request "Finding current user pending friends if friends not present" do
        explanation "Auth client can find current user requested or pending friends.
          This endpoint returns empty if friends not found."
        expect(status).to eq(200)
      end
    end

    get "/api/v1/friendships" do
      parameter :requested, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        If value is empty it sets to false by default", "Type" => "Integer, String, Boolean"
      parameter :pending, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        If value is empty it sets to false by default", "Type" => "Integer, String, Boolean"

      let(:pending) { '1' }
      let(:requested) { '1' }
      let(:raw_post) { params.to_json }

      response_field :requested_users, "Type" => "JSON object"
      response_field :requested_places, "Type" => "JSON object"

      example_request "Finding current user pending & requested friends if friends not present" do
        explanation "Auth client can find current user requested or pending friends.
          This endpoint returns empty if friends not found."
        expect(status).to eq(200)
      end
    end
  end

  context "Non-empty array response" do
    let!(:user_friend) { create(:user) }
    let!(:place_friend) { create(:place) }
    let!(:friends) { [user_friend, place_friend] }
    let!(:new_user_friend) { create(:user) }
    let!(:new_place_friend) { create(:place) }
    let!(:new_friends) { [new_user_friend, new_place_friend] }
    let!(:requested_friends) do
      user_friend.friend_request(user); place_friend.friend_request(user)
    end

    get "/api/v1/friendships" do
      parameter :requested, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        If value is empty it sets to false by default", "Type" => "Integer, String, Boolean"

      let(:requested) { '1' }
      let(:raw_post) { params.to_json }

      response_field :requested_users, "Type" => "JSON object"
      response_field :requested_places, "Type" => "JSON object"

      response_field :id, scope: :requested_users, "Type" => "Integer"
      response_field :first_name, scope: :requested_users, "Type" => "String"
      response_field :last_name, scope: :requested_users, "Type" => "String"

      response_field :id, scope: :requested_places, "Type" => "Integer"
      response_field :name, scope: :requested_places, "Type" => "String"
      response_field :description, scope: :requested_places, "Type" => "String"

      example_request "Finding current user requested friends" do
        explanation "Auth client can find current user requested or pending friends.
          This endpoint returns  requested_users array if friend user.
          or requested_places if friend place or both."
        expect(status).to eq(200)
      end
    end

    get "/api/v1/friendships" do
      parameter :pending, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        If value is empty it sets to false by default", "Type" => "Integer, String, Boolean"

      let!(:pending_friends) do
        user.friend_request(user_friend); user.friend_request(place_friend)
      end

      let(:pending) { '1' }
      let(:raw_post) { params.to_json }

      response_field :requested_users, "Type" => "JSON object"
      response_field :requested_places, "Type" => "JSON object"

      response_field :id, scope: :requested_users, "Type" => "Integer"
      response_field :first_name, scope: :requested_users, "Type" => "String"
      response_field :last_name, scope: :requested_users, "Type" => "String"

      response_field :id, scope: :requested_places, "Type" => "Integer"
      response_field :name, scope: :requested_places, "Type" => "String"
      response_field :description, scope: :requested_places, "Type" => "String"

      example_request "Finding current user pending friends" do
        explanation "Auth client can find current user requested or pending friends.
          This endpoint returns pending_users array if friend user.
          or pending_places if friend place or both."
        expect(status).to eq(200)
      end
    end

    get "/api/v1/friendships" do
      parameter :requested, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        If value is empty it sets to false by default", "Type" => "Integer, String, Boolean"
      parameter :pending, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        If value is empty it sets to false by default", "Type" => "Integer, String, Boolean"

      let!(:pending_friends) do
        user.friend_request(user_friend); user.friend_request(place_friend)
      end
      let!(:requested_friends) do
        new_user_friend.friend_request(user); new_place_friend.friend_request(user)
      end

      let(:pending) { '1' }
      let(:requested) { '1' }
      let(:raw_post) { params.to_json }

      response_field :requested_users, "Type" => "JSON object"
      response_field :requested_places, "Type" => "JSON object"

      response_field :id, scope: :requested_users, "Type" => "Integer"
      response_field :first_name, scope: :requested_users, "Type" => "String"
      response_field :last_name, scope: :requested_users, "Type" => "String"

      response_field :id, scope: :requested_places, "Type" => "Integer"
      response_field :name, scope: :requested_places, "Type" => "String"
      response_field :description, scope: :requested_places, "Type" => "String"

      example_request "Finding current user pending & requested friends" do
        explanation "Auth client can find current user requested or pending friends.
          This endpoint returns pending_users array if friend user.
          or pending_places if friend place or both."
        expect(status).to eq(200)
      end
    end
  end
end

resource "Friendships - Create - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  post "/api/v1/friendships" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"

    let(:id) { rand(10...42) }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Accept friend request from requested friend by current user" do
      explanation "Auth client can accept user friend request by id and type.
        This endpoint returns 404 if friend was not found."

      expect(status).to eq(404)
    end
  end

  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }

  let!(:friendship) do
    user.friend_request(user_friend); user.friend_request(place_friend)
    user_friend.accept_request(user); place_friend.accept_request(user)
  end

  post "/api/v1/friendships" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"
    let(:id) { user_friend.id }
    let(:type) { '0' }

    let(:raw_post) { params.to_json }

    example_request "Trying to accept friend request from exist friend by current user" do
      explanation "Auth client can accept user friend request by id and type.
        This endpoint returns 400 if current user want to accept request from exist friend."

      expect(status).to eq(400)
    end
  end

  post "/api/v1/friendships" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define record type(user: 0, place: 1).", required: true,
      "Type" => "Integer, String, Boolean"

    let(:id) {  place_friend.id }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Accept friend request from any user who is not a friend by current user" do
      explanation "Auth client can accept user friend request by id and type.
        This endpoint returns 400 if friend not found in friend list"

      expect(status).to eq(400)
    end
  end
end

resource "Friendships - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }

  let!(:requested_friendship) do
    user_friend.friend_request(user); place_friend.friend_request(user)
  end

  post "/api/v1/friendships" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define record type(user: 0, place: 1).", required: true,
      "Type" => "Integer, String, Boolean"

    let(:id) {  place_friend.id }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Accept friend request from requested friend by current user" do
      explanation "Auth client can accept user friend request by id and type.
        This endpoint returns 201 if friend found and became a friend."

      expect(status).to eq(201)
    end
  end
end

resource "Friendships - Delete - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  delete "/api/v1/friendships/:id" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"

    let(:id) { rand(100...420) }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Decline friend request from random user by current user" do
      explanation "Auth client can accept user friend request by id and type.
        This endpoint returns 404 if friend was not found."

      expect(status).to eq(404)
    end
  end

  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }

  delete "/api/v1/friendships/:id" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"
    let(:id) { user_friend.id }
    let(:type) { '0' }

    let(:raw_post) { params.to_json }

    example_request "Trying to decline friend request from not exist friend by current user" do
      explanation "Auth client can accept user friend request by id and type.
        This endpoint returns 400 if current user want to accept request from exist friend."

      expect(status).to eq(400)
    end
  end
end

resource "Friendships - Delete - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }

  let!(:friendship) do
    user.friend_request(user_friend); user.friend_request(place_friend)
    user_friend.accept_request(user); place_friend.accept_request(user)
  end

  delete "/api/v1/friendships/:id" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"

    let(:id) { place_friend.id }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Decline friend request from requested friend by current user" do
      explanation "Auth client can decline user friend request by id and type.
        This endpoint returns 200 if friend request was declined."

      expect(status).to eq(200)
    end
  end
end
