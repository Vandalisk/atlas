require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Settings" do
  get "/settings/:id" do
    example "Show settings" do
      true
    end
  end

  put "/settings/:id" do
    example "Update settings" do
      true
    end
  end
end
