
require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Favourite Events - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }
  let!(:favourite_events) { create_list(:event, 3) }
  before { user.favourite_events << favourite_events }

  get "/api/v1/favourite_events" do
    response_field :favourite_events, "User's favourite events.", "Type" => "JSON object"
    response_field :id, "Event's id", "Type" => "Integer", scope: :favourite_events
    response_field :title, "Text of the message", "Type" => "String", scope: :favourite_events
    response_field :description, "Event's description", "Type" => "String, Text", scope: :favourite_events
    response_field :time,"Time of event", "Type" => "JSON object", scope: :favourite_events
    response_field :created_at, "Event's created_at.", scope: :favourite_events, "Type" => "Datetime UTC."
    response_field :place, "Place, where event is perfomed.", "Type" => "JSON object", scope: :favourite_events
    response_field :id, "Place's id", scope: [:favourite_events, :place], "Type" => "Integer"
    response_field :name, "Place's name", scope: [:favourite_events, :place], "Type" => "String"
    response_field :description, "Place's description", scope: [:favourite_events, :place], "Type" => "Text"
    response_field :additional_phone_number, "Place's additional phone number.", scope: [:favourite_events, :place],
      "Type" => "String"
    response_field :latitude, "Place latitude", scope: [:favourite_events, :place], "Type" => "String"
    response_field :longitude, "Place longitude", scope: [:favourite_events, :place], "Type" => "String"
    response_field :phone_number, "Phone number used for registration.", scope: [:favourite_events, :place],
      "Type" => "String"
    response_field :average_rating, "Avarage rating of place", scope: [:favourite_events, :place], "Type" => "Float"
    response_field :image_path, "Place image url", scope: [:favourite_events, :place], "Type" => "String"

    example_request "User's favourite events." do
      explanation "Possibility to retrieve favourite user's events."

      expect(status).to eq(200)
    end
  end
end

resource "Favourite Events - Find - Error" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:place) { create(:place) }
  let!(:token) { "Bearer #{Token.generate(place)}" }
  let!(:favourite_events) { create_list(:event, 3) }
  before { place.user.favourite_events << favourite_events }

  get "/api/v1/favourite_events" do
    example_request "User's favourite events." do
      explanation "Place can't retrieve favourite events."

      expect(status).to eq(403)
    end
  end
end

resource "Favourite Events - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }
  let!(:favourite_events) { create_list(:event, 3) }
  before { user.favourite_events << favourite_events }

  post "/api/v1/favourite_events" do
    let(:event) { create(:event) }
    let(:id) { event.id }
    let(:raw_post) { params.to_json }

    parameter :id, "Id of event", required: true
    example_request "Add to user's favourite events." do
      explanation "Possibility to add favourite events to user's favourite_events."

      expect(status).to eq(201)
    end
  end
end

resource "Favourite Events - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }
  let!(:favourite_events) { create_list(:event, 3) }
  before { user.favourite_events << favourite_events }

  post "/api/v1/favourite_events" do
    let(:id) { rand(50..100) }
    let(:raw_post) { params.to_json }

    parameter :id, "Id of event", required: true
    example_request "Add to user's favourite events." do
      explanation "Error when no such event."

      expect(status).to eq(404)
    end
  end
end
