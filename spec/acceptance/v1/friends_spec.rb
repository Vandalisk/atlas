require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Friends - Find - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  before(:each) do
    HasFriendship::Friendship.destroy_all
  end

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  context "Empty array response" do
    get "/api/v1/friends" do
      response_field :users, "Type" => "JSON object"
      response_field :places, "Type" => "JSON object"

      example_request "Finding current user friends if friends not present" do
        explanation "Auth client can find current user friends.
          This endpoint returns empty object if friends not found."
        expect(status).to eq(200)
      end
    end
  end

  context "Non-empty array response" do
    let!(:user_friend) { create(:user) }
    let!(:place_friend) { create(:place) }
    let!(:friends) { [user_friend, place_friend] }
    let!(:new_user_friend) { create(:user) }
    let!(:new_place_friend) { create(:place) }
    let!(:new_friends) { [new_user_friend, new_place_friend] }
    let!(:friendships) do
      user_friend.friend_request(user); place_friend.friend_request(user)
      user.accept_request(user_friend); user.accept_request(place_friend)
    end

    get "/api/v1/friends" do
      let(:requested) { '1' }
      let(:raw_post) { params.to_json }

      response_field :users, "Type" => "JSON object"
      response_field :places, "Type" => "JSON object"

      response_field :id, scope: :users, "Type" => "Integer"
      response_field :first_name, scope: :users, "Type" => "String"
      response_field :last_name, scope: :users, "Type" => "String"

      response_field :id, scope: :places, "Type" => "Integer"
      response_field :name, scope: :places, "Type" => "String"
      response_field :description, scope: :places, "Type" => "String"

      example_request "Finding current user friends" do
        explanation "Auth client can find current friends.
          This endpoint returns  users array if friend user.
          or places if friend place or both."
        expect(status).to eq(200)
      end
    end
  end
end

resource "Friends - Create - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  post "/api/v1/friends" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"

    let(:id) { rand(10...42) }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Create friend request from current user to friend" do
      explanation "Auth client can create friend request to user by id and type.
        This endpoint returns 404 if friend was not found."

      expect(status).to eq(404)
    end
  end

  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }

  let!(:friendship) do
    user.friend_request(user_friend); user.friend_request(place_friend)
    user_friend.accept_request(user); place_friend.accept_request(user)
  end

  post "/api/v1/friendships" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"
    let(:id) { user_friend.id }
    let(:type) { '0' }

    let(:raw_post) { params.to_json }

    example_request "Trying to create friend request to exist friend by current user" do
      explanation "Auth client can create user friend request by id and type.
        This endpoint returns 400 if current user want to create friend request to exist friend."

      expect(status).to eq(400)
    end
  end
end

resource "Friends - Create - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }

  post "/api/v1/friends" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
      Value from this field define record type(user: 0, place: 1).", required: true,
      "Type" => "Integer, String, Boolean"

    let(:id) { place_friend.id }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Create friend request from any user who is not a friend by current user" do
      explanation "Auth client can create friend request to any user or place by id and type.
        This endpoint returns 201 if friend request created"

      expect(status).to eq(201)
    end
  end
end

resource "Friends - Delete - Errors" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  delete "/api/v1/friends/:id" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"

    let(:id) { rand(10...42) }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Remove random user from friends by current user" do
      explanation "Auth client can remove user from friends request by id and type.
        This endpoint returns 404 if friend was not found."

      expect(status).to eq(404)
    end
  end

  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }

  delete "/api/v1/friends/:id" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"
    let(:id) { user_friend.id }
    let(:type) { '0' }

    let(:raw_post) { params.to_json }

    example_request "Trying remove user from friends if user not a friend by current user" do
      explanation "Auth client can remove friend from friends by id and type.
        This endpoint returns 400 if user not a friend."

      expect(status).to eq(400)
    end
  end
end

resource "Friendships - Delete - Success" do
  header 'CONTENT_TYPE', 'application/json'
  header 'ACCEPT', 'application/json'
  header 'HOST',''
  header "Authorization", :token

  let!(:user) { create(:user) }
  let!(:token) { "Bearer #{Token.generate(user)}" }

  let!(:user_friend) { create(:user) }
  let!(:place_friend) { create(:place) }
  let!(:friends) { [user_friend, place_friend] }

  let!(:friendship) do
    user.friend_request(user_friend); user.friend_request(place_friend)
    user_friend.accept_request(user); place_friend.accept_request(user)
  end

  delete "/api/v1/friendships/:id" do
    parameter :id, "Record id", required: true, "Type" => "Integer"
    parameter :type, "Boolean flag. Allow 1, '1', true and 0, '0', false.
        Value from this field define record type(user: 0, place: 1).", required: true,
        "Type" => "Integer, String, Boolean"

    let(:id) { place_friend.id }
    let(:type) { '1' }

    let(:raw_post) { params.to_json }

    example_request "Remove friend from friends by current user" do
      explanation "Auth client can remove friend from friends by id and type.
        This endpoint returns 200 if friend was removed."

      expect(status).to eq(200)
    end
  end
end
