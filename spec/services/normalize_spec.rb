require 'rails_helper'

RSpec.describe Normalizer do
  let!(:denormalized_phone_number) { Faker::PhoneNumber.cell_phone }
  let!(:normalized_number_regex) { /^[+][0-9]+$/ }

  it "returns true" do
    expect(denormalized_phone_number.size).to be >= 11
    expect(denormalized_phone_number.first).not_to eq "+"
  end

  it "returns number with 11 digits and begining with +" do
    normalized_number = Normalizer.phone_number(denormalized_phone_number)

    expect(normalized_number =~ normalized_number_regex).to eq(0)
  end
end
