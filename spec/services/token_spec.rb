require 'rails_helper'

RSpec.describe Token do
  let!(:user) { create(:user) }
  let!(:payload) do
    {
      "phone_number" => Normalizer.phone_number(user.phone_number),
      "social_id" => user.social_id,
      "social_marker" => User.social_markers[user.social_marker],
      "created_at" => user.created_at.to_s,
      "updated_at" => user.updated_at.to_s
    }
  end
  let!(:token_mock) { JWT.encode(payload, ENV['AUTH_TOKEN_SECRET_KEY']) }

  context "Encode token with payload" do
    it "returns token" do
      encoded_token = Token.encode(payload)

      expect(encoded_token).to eq(token_mock)
    end
  end

  context "Decode token with right token" do
    it "returns payload" do
      decoded_token = Token.decode(token_mock)

      expect(decoded_token).to eq(payload)
    end

    it "returns exeption" do
      expect{Token.decode('invalid_token')}.to raise_error(JWT::DecodeError)
    end
  end

  context "Generate token for user" do
    it "returns token" do
      encoded_token = Token.generate(user)

      expect(encoded_token).to eq(token_mock)
    end
  end
end
