Rails.application.routes.draw do
  devise_for :super_users
  devise_for :admins
  devise_for :moderators

  namespace :super_users do
    resources :places, only: %i(index show edit update destroy)
    resources :users, only: %i(index show edit update destroy)
    resources :moderators
    resources :admins
    resources :events
    resources :settings

    root to: "places#index"
  end

  namespace :private do
    resources :places, only: :index
    resources :tokens, only: %i(index show)
  end

  namespace :public do
    resources :violations, only: :show
  end

  namespace :admins do
    resources :violations, except: %i(new create) do
      put 'publish' => 'violations#publish', on: :member
      put 'unpublish' => 'violations#unpublish', on: :member
      get 'new_email' => 'violations#new_email', on: :member
      post 'new_email' => 'violations#create_email', on: :member
    end

    resources :administrations

    root to: "violations#index"
  end

  namespace :moderators do
    resources :news
    resources :moderators
    resources :places
    resources :users

    root to: "news#index"
  end

  namespace :api do
    namespace :v1 do
      root to: 'maps#index'

      resources :users, only: %i(index create show update) do
        delete '/' => 'users#destroy', on: :collection
      end

      resources :violations do
        post "photos" => "violations#photos" , on: :member
      end

      resources :search_chats, only: %i(index)
      resources :chats, only: %i(index show create) do
        resources :messages, only: %i(index create update)
      end

      resources :services, only: %i(index)
      resources :search, only: %i(index)

      resources :black_lists, only: %i(index create update) do
        delete ":phone_number" => "black_lists#destroy", on: :collection
        get ":phone_number" => "black_lists#show", on: :collection
      end

      resources :places, only: %i(index create update show) do
        resources :comments, only: %i(index create destroy)


        resources :events, only: %i(index create show update destroy) do
          get "search" => "events#search", on: :collection
        end

      end

      resources :maps, only: %i(index)
      resources :favourite_events, only: %i(index create)
      resources :news, only: %i(index)

      #TODO: Add constrait for type validation on destroy
      resources :friends, only: %i(index create destroy)
      #TODO: Add constrait for type validation on destroy
      resources :friendships, only: %i(index create destroy)
      resources :violations

      resources :settings, only: %i(show update)
      resources :image_uploads, only: %i(create)
      resources :images_uploads, only: %i(create)
    end
  end
end
