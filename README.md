## How to start
`$ bundle exec bin/setup`

##Documenation

Run server `bin/rails s` and go to `localhost:3000/api/docs` to see api docs

For update doc run `bundle exec rake docs:generate`

You can find db diagrams in public/doc

