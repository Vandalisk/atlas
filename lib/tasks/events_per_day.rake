namespace :events_per_day do
  desc "Clear all events for place, which were made yesterday."

  task clear: :environment do
    Place.update_all(events_today: 0)
  end
end
