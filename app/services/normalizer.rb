module Normalizer
  module_function

  def phone_number(phone_number)
    phone_number.delete("^0-9").prepend("+")
  end

  # TODO: Add specs for this method
  def phone_number_in_params(params)
    if params.has_key?(:phone_number)
      params.merge({ phone_number: Normalizer.phone_number(params[:phone_number]) })
    elsif params.has_key?(:user) && params[:user].has_key?(:phone_number)
      params.deep_merge({ user: { phone_number: Normalizer.phone_number(params[:phone_number]) }})
    else
      params
    end
  end
end
