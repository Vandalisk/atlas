module Broadcast
  module_function

  QUOTE = "I'll be back".freeze

  def push_to_broadcast(event, entry, channel)
    return default_implementation(event, entry, channel) unless Rails.env.test?
    return test_implementation if Rails.env.test?
  end

  def default_implementation(event, entry, channel)
    broadcast(event, entry, channel)
  end

  def test_implementation
    broadcast(:success, QUOTE)
  end

  private_class_method :default_implementation, :test_implementation
end
