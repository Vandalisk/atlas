module Token
  module_function

  def encode(payload)
    JWT.encode(payload, ENV['AUTH_TOKEN_SECRET_KEY'])
  end

  def decode(token)
    JWT.decode(token, ENV['AUTH_TOKEN_SECRET_KEY'], true).first
  end

  def generate(record)
    payload = {
      phone_number: Normalizer.phone_number(record.phone_number),
      social_id: record.social_id,
      social_marker: User.social_markers[record.social_marker],
      created_at: record.created_at.to_s,
      updated_at: record.updated_at.to_s
    }
    encode(payload)
  end

  def handle(header)
    # TODO: Add spec
    return unless header
    pattern = /^Bearer /
    header.gsub(pattern, "") if header.match(pattern)
  end
end
