module PrivateSettings
  def check_friendship(sender, recipient)
    return unless recipient.private
    raise Pundit::NotAuthorizedError unless HasFriendship::Friendship.exist?(recipient, sender)
  end

  def check_black_list(sender, recipient)
    raise Pundit::NotAuthorizedError if recipient.place.blocked?(sender)
  end
end
