module UserAdapter
  module_function
  #TODO: Add specs

  def get_user(record)
    record.is_a?(Place) ? record.user : record
  end

  def get_user_by_id(id, owner)
    owner.to_b ? Place.find(id).user : User.find(id)
  end

  def get_record(id, owner)
    owner.to_b ? Place.find(id) : User.find(id)
  end

  def get_record_type(record)
    record.is_a?(Place) ? 1 : 0
  end

  def get_user_by_phone(phone)
    get_user(User.get_by_params({ 'phone_number' => Normalizer.phone_number(phone)}))
  end
end
