module ChatAdapter
  module_function

  def get_by_recipient_phone_number(current_user, recipient_phone_number)
    recipient = UserAdapter.get_user_by_phone(recipient_phone_number)
    Chat.involving_pair(recipient, current_user)
  end
end
