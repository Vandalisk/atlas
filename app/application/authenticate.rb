module Authenticate
  module_function

  def by(token)
    model_attributes = Token.decode(token).except("created_at", "updated_at")
    User.get_by_params(model_attributes)
  end
end
