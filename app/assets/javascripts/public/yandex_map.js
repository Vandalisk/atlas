if(typeof ymaps != 'undefined'){
  ymaps.ready(init);
  var myMap;

  function init(){
    myMap = new ymaps.Map("map", {
        center: [43.585525, 39.723062],
        zoom: 7
    });

    if($('#latitude') && $('#longitude')){
      var latitude = parseFloat($('#latitude').text());
      var longitude = parseFloat($('#longitude').text());
    }

    var coords = [latitude, longitude];
    myMap.setCenter(coords)
    var myGeocoder = ymaps.geocode(coords);

    myGeocoder.then(
      function (res) {
        var street = res.geoObjects.get(0);
        var name = street.properties.get('name');
        var text = street.properties.get('text');

        myMap.geoObjects.removeAll();

        myPlacemark = new ymaps.Placemark(coords, {
          hintContent: name,
          balloonContentHeader: name,
          balloonContentBody: text,
        });
        myMap.geoObjects.add(myPlacemark);
      }
    );
  }
}
