if(typeof ymaps != 'undefined'){
  ymaps.ready(init);
  var myMap;
  function init(){
    myMap = new ymaps.Map("map", {
        center: [43.585525, 39.723062],
        zoom: 7
    });

    if(gon.latitude && gon.longitude){
      var coords = [gon.latitude, gon.longitude];
      myMap.setCenter(coords)
      var myGeocoder = ymaps.geocode(coords);

      myGeocoder.then(
        function (res) {
          var street = res.geoObjects.get(0);
          var name = street.properties.get('name');
          var text = street.properties.get('text');

          myMap.geoObjects.removeAll();

          myPlacemark = new ymaps.Placemark(coords, {
            hintContent: name,
            balloonContentHeader: name,
            balloonContentBody: text,
          });
          myMap.geoObjects.add(myPlacemark);
        }
      );
    }

    myMap.events.add('click', function (e) {
      urlParts = window.location.pathname.split('/');
      var resorce = urlParts[2].slice(0, -1);
      var coords = e.get('coords');
      var myGeocoder = ymaps.geocode(coords);

      myGeocoder.then(
        function (res) {
          var street = res.geoObjects.get(0);
          var name = street.properties.get('name');
          var text = street.properties.get('text')
          confirmFormText = "Месторасположение " + name + "?"
          result = confirm(confirmFormText);

          if(result){
            $("input#" + resorce + "_latitude").val(coords[0]);
            $("input#" + resorce + "_longitude").val(coords[1]);
            myMap.geoObjects.removeAll();
            myPlacemark = new ymaps.Placemark(coords, {
              hintContent: name,
              balloonContentHeader: name,
              balloonContentBody: text,
            });
            myMap.geoObjects.add(myPlacemark);
          }
        }
      );

    });
  }
}
