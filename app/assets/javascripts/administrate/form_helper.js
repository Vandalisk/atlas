$(function() {
  $.ajax({
    url: Routes.private_places_path(),
    headers: {
        'Content-Type':'application/json'
    },
    method: 'GET',
    dataType: 'json',
    success: function(data){
      places = data.collection;
      placesNames = $.map(places, function(obj){ return obj.name });
      $("input#news_place").autocomplete({
        source: placesNames
      });
      $("input#news_place").autocomplete({
        source: placesNames
      });
      $("input#event_place").autocomplete({
        source: placesNames
      });
    },
    error: function(error){
      console.log('error: '+ error);
    }
  });

  function replace_place(place) {
    if ($("input#event_place").length) {
      $("input#event_place").val(place.name);
    }
    if ($("input#news_place").length) {
       $("input#event_place").val(place.name);
    };
  }
  if(typeof gon !== "undefined" && gon.place !== undefined) { replace_place(gon.place) }
});
