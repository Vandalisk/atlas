require "azure"

class AzureImageUploader < BaseUploader
  AZURE_IMAGES_DIR_PATH = "#{ENV['AZURE_STORAGE_BLOB_HOST']}#{ENV['AZURE_CONTAINER']}"

  def upload(image)
    upload_blob_to_azure(image) if valid_ext?(prepare_tempfile(image))
  end

  def upload_collection(images)
    images.map do |image|
      upload(image).name
    end
  end

  private

  def prepare_tempfile(image)
    tempfile = Tempfile.new(["", ".png"])
    tempfile << image
    tempfile.rewind
    ActionDispatch::Http::UploadedFile.new(tempfile: tempfile).tempfile
  end

  def valid_ext?(image_file)
    mime_type = MiniMagick::Image.new(image_file.path).mime_type

    if BaseUploader::EXTENSION_WHITELIST.exclude?(mime_type)
      raise TypeError, BaseUploader::EXTENSION_VALIDATION_MESSAGE
    else
      true
    end
  end

  def upload_blob_to_azure(image_file)
    Azure.storage_account_name = ENV['AZURE_STORAGE_ACCOUNT']
    Azure.storage_access_key = ENV['AZURE_STORAGE_ACCESS_KEY']

    Azure::Blob::BlobService.new.create_block_blob(
      ENV['AZURE_CONTAINER'], "images/#{SecureRandom.hex}", image_file
    )
  end
end
