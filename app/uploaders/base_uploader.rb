class BaseUploader
  EXTENSION_WHITELIST = %w(jpg jpeg png image/png)
  EXTENSION_VALIDATION_MESSAGE = "Wrong mime type! Allow only jpeg, jpg and png".freeze
end
