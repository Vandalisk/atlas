class Moderators::ApplicationController < Administrate::ApplicationController
  before_filter :authenticate_moderator!
end