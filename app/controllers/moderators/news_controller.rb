class Moderators::NewsController < Moderators::ApplicationController
  def index
    search_term = params[:search].to_s.strip
    resources = current_moderator.news
    resources = resources.order(:id) if params[:order].nil?
    resources = order.apply(resources)
    resources = resources.page(params[:page]).per(records_per_page)
    page = Administrate::Page::Collection.new(dashboard, order: order)

    render locals: {
      resources: resources,
      search_term: search_term,
      page: page,
    }
  end

  def create
    params[:news][:place_id] = Place.find_by(name: params[:news][:place]).id
    params[:news][:moderator_id] = current_moderator.id
    resource = resource_class.new(resource_params)
    resource.radius = current_moderator.radius

    if resource.save
      redirect_to(
        [namespace, resource],
        notice: translate_with_resource("create.success"),
      )
    else
      render :new, locals: {
        page: Administrate::Page::Form.new(dashboard, resource),
      }
    end
  end

  def update
    if requested_resource.update(resource_params.merge(radius: current_moderator.radius))
      redirect_to(
        [namespace, requested_resource],
        notice: translate_with_resource("update.success"),
      )
    else
      render :edit, locals: {
        page: Administrate::Page::Form.new(dashboard, requested_resource),
      }
    end
  end

  # See https://administrate-docs.herokuapp.com/customizing_controller_actions
  # for more information
end
