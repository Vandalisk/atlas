class Admins::ViolationsController < Admins::ApplicationController
  def publish
    update_resource(requested_resource, 'published', 'publish')
  end

  def unpublish
    update_resource(requested_resource, 'under_consideration', 'unpublish')
  end

  def edit
    gon.push({ latitude: requested_resource.latitude, longitude: requested_resource.longitude })
    render locals: {
      page: Administrate::Page::Form.new(dashboard, requested_resource),
    }
  end

  def update
    if requested_resource.update(params_with_status)
      redirect_to(
        [namespace, requested_resource],
        notice: translate_with_resource("update.success"),
      )
    else
      render :edit, locals: {
        page: Administrate::Page::Form.new(dashboard, requested_resource),
      }
    end
  end

  def new_email
    @id = params[:id]
    @administrations = Administration.all
  end

  def create_email
    params[:administration].pop
    violation = Violation.find(params[:id])
    url = request.original_url.split('/new_email')[0]
    admins_appeal = params[:message]
    administration_emails = Administration.where(id: params[:administration]).pluck(:email)
    ViolationMailer.complain_email(admins_appeal, administration_emails, url, violation).deliver_later
    redirect_to admins_violations_path, notice: t("administrate.success.email_delivered")
  end

  private

  def params_with_status
    resource_params.merge(status: Violation.status_by_value(Integer(resource_params[:status])))
  end

  def update_resource(resource, status, action)
    translation_hash = {
      requested_resource: requested_resource.id, action: t("administrate.actions.#{action}")
    }
    message = if resource.update(status: status)
        t("administrate.actions.publish_violation", translation_hash)
      else
        t("administrate.errors.not_published", translation_hash)
      end

    redirect_to admins_violations_path, notice: message
  end
end
