class Api::V1::ChatsController < Api::BaseController
  def index
    @chats = Chat.involving(UserAdapter.get_user(current_user)).desc_order
    @chats_counter = Chat.involving(UserAdapter.get_user(current_user)).count
  end

  def show
    @chat = Chat.find(params[:id])
    respond_with(@chat)
  end

  def create
    command = CreateChat.new(create_params)
    command.subscribe(PusherObserver.new)
    @chat = command.execute

    respond_with(@chat, status: :created)
  end

  private

  def show_params
    params.permit(:id, :recipient_phone_number)
  end

  def create_params
    params.require(:chat).permit(:recipient_phone_number, :body)
      .merge(sender_phone_number: current_user.phone_number)
  end
end
