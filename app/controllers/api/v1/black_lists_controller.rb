class Api::V1::BlackListsController < Api::BaseController
  def index
    places_flag = index_params[:places].to_b
    users_flag = index_params[:users].to_b

    if users_flag
      @users = current_place.users_in_black_list
      render template: "api/v1/black_lists/users" and return
    end

    if places_flag
      @places = current_place.places_in_black_list
      render template: "api/v1/black_lists/places" and return
    end

    @users = current_place.users_in_black_list
    @places = current_place.places_in_black_list
  end

  def show
    user = UserAdapter.get_user_by_phone(params[:phone_number])
    render json: { blocked: current_place.blocked?(user) }.to_json, status: :ok
  end

  def create
    record_respond(BlackList.add_by_phone_number!(current_place, create_params[:phone_number]))
  end

  def destroy
    record_respond(BlackList.destroy_by_phone_number!(current_place, params[:phone_number]))
  end

  private

  def index_params
    params.permit(:users, :places)
  end

  def create_params
    params.require(:black_list).permit(:phone_number)
  end
end
