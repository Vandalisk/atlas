class Api::V1::ImagesUploadsController < Api::BaseController
  skip_before_filter :authenticate

  def create
    blob_urls = AzureImageUploader.new.upload_collection(params[:images])
    result = blob_urls.map do |url|
      { image_url: "#{AzureImageUploader::AZURE_IMAGES_DIR_PATH}/#{url}" }
    end

    render json: result.to_json
  end
end

