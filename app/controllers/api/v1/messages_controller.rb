class Api::V1::MessagesController < Api::BaseController
  def index
    @messages = Message.limit_messages_from_chat(
      current_user,
      index_params[:chat_id],
      index_params[:count],
      index_params[:start_message_id]
    )
    @messages_counter = @messages.count
  end

  def create
    chat = Chat.find(create_params[:chat_id])
    authorize(chat)

    command = CreateMessage.new(chat, create_params.except(:chat_id))
    command.subscribe(PusherObserver.new)
    @message = command.execute

    respond_with(@message, status: :created)
  end

  def update
    chat = Chat.find(read_params[:chat_id])
    authorize(chat)

    command = UpdateMessage.new(chat, read_params.except(:chat_id))
    command.subscribe(PusherObserver.new)

    command.execute ? (head :ok) : (head :bad_request)
  end

  private

  def index_params
    params.permit(:start_message_id, :count, :chat_id)
  end

  def create_params
    params.require(:message).permit(:body, :chat_id).merge(sender: current_user)
  end

  def read_params
    params.permit(:id, :chat_id)
  end
end
