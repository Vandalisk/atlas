class Api::V1::ViolationsController < Api::BaseController
  def index
    @violations = Violation.violations_nearby(current_user, params[:distance])
  end

  def show
    @violation = Violation.find(params[:id])
  end

  def create
    record_respond(Violation.create!(violation_params), :created)
  end

  def destroy
    record_respond(Violation.find(params[:id]).destroy)
  end

  private

  def violation_params
    params.
      require(:violation).permit(:title, :description, :latitude, :longitude, :category, photos: [])
  end
end
