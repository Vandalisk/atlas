class Api::V1::PlacesController < Api::BaseController
  skip_before_filter :authenticate, except: :update

  def index
    authenticate if request.headers["Authorization"].present?

    if @current_user
      if params[:name].present?
        @place = Place.find_by!("name ILIKE ?", "#{params[:name]}%")
        render :show and return
      end

      if params[:description].present?
        @place = Place.find_by!("description ILIKE ?", "#{params[:description]}%")
        render :show and return
      end

      if params[:phone_number].present?
        @place = Place.joins(:user).find_by!("users.phone_number = ?", Normalizer.phone_number(params[:phone_number]))
        render :show and return
      end

      @place = @current_user.place
      render :show and return
    end

    respond(Place.get_by_params(Normalizer.phone_number_in_params(find_params)))
  end

  def show
    @place = Place.find(params[:id])
    respond_with(@place)
  end

  def update
    record_respond(current_place.update!(update_params), :ok)
  end

  def create
    attrs = create_params.deep_merge(user_attributes: required_user_attributes)
    respond(Place.create!(attrs), :created)
  end

  def destroy
    record_respond(Place.find(params[:id]).destroy!, :ok) if params[:mobile_test].present?
  end

  private

  def find_params
    params.require(:place).permit(:phone_number, :social_id, :social_marker)
  end

  def create_params
    params.require(:place).permit(
      :name,
      :description,
      :image_path,
      :address,
      :additional_phone_number,
      :weekdays_hours,
      :weekends_hours,
      user_attributes: [
        :phone_number,
        :social_id,
        :social_marker,
        :latitude,
        :longitude
      ]
    )
  end

  def update_params
    params.require(:place).permit(
      :name,
      :description,
      :image_path,
      :address,
      :additional_phone_number,
      :weekdays_hours,
      :weekends_hours
    )
  end

  def find_params
    params.require(:place).permit(:phone_number, :social_id, :social_marker)
  end

  def required_user_attributes
    {
      first_name: params[:place][:name],
      last_name: params[:place][:name],
      age: "30",
      sex: "Male",
      owner: true
    }
  end
end
