class Api::V1::EventsController < Api::BaseController
  def index
    find_place
    @events = @place.events
  end

  def show
    find_place
    @event = @place.events.find(params[:id])

    respond_with(@event, status: :ok, locals: { place: @place })
  end

  def create
    find_place

    record_respond(current_place.events.create!(event_params))
  end

  def update
    find_event

    record_respond(@event.update!(event_params))
  end

  def destroy
    find_event
    record_respond(@event.destroy!, status: :ok)
  end

  private

  def find_place
    @place = Place.find(params[:place_id])
  end

  def find_event
    @event = current_place.events.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:title, :description, :time)
  end
end
