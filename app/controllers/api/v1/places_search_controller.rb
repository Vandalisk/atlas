class Api::V1::SearchController < Api::BaseController
  def index
    @places = Place.search_by_category_name(search_params[:category])
  end

  private

  def search_params
    params.permit(:category)
  end
end
