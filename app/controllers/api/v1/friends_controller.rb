class Api::V1::FriendsController < Api::BaseController
  include FriendshipConcern

  def index
    @users_friends = current_user.user_friends
    @places_friends = current_user.place_friends
  end

  def create
    check_black_list(current_user, friend) if friend.owner?
    record_respond(current_user.friend_request(friend), :created)
  end

  def destroy
    record_respond(current_user.remove_friend(friend))
  end
end
