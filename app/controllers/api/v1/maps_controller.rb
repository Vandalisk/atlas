class Api::V1::MapsController < Api::BaseController
  def index
    places_flag = index_params[:places].to_b
    users_flag = index_params[:users].to_b

    if users_flag
      @pending_users = current_user.user_pending_friends.visible
      @requested_users = current_user.user_requested_friends.visible
      @users_friends = current_user.user_friends.visible
      @users = User.users_nearby(current_user).closest_to(current_user).visible
      render template: "api/v1/maps/users" and return
    end

    if places_flag
      @pending_places = current_user.place_pending_friends
      @requested_places = current_user.place_requested_friends
      @places_friends = current_user.place_friends
      @places = User.places_nearby(current_user).closest_to(current_user)
      render template: 'api/v1/maps/places' and return
    end

    @violations = Violation.all

    if current_user.invisible?
      @pending_places = current_user.place_pending_friends
      @requested_places = current_user.place_requested_friends
      @places_friends = current_user.place_friends
      @places = User.places_nearby(current_user).closest_to(current_user)
      render template: 'api/v1/maps/invisible' and return
    end

    @pending_users = current_user.user_pending_friends.visible
    @requested_users = current_user.user_requested_friends.visible
    @users_friends = current_user.user_friends.visible
    @pending_places = current_user.place_pending_friends
    @requested_places = current_user.place_requested_friends
    @places_friends = current_user.place_friends
    @places = User.places_nearby(current_user).closest_to(current_user)
    @users = User.users_nearby(current_user).closest_to(current_user).visible
  end

  private

  def index_params
    params.permit(:places, :users)
  end
end
