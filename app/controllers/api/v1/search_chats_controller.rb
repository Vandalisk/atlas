class Api::V1::SearchChatsController < Api::BaseController
  def index
    @chat = ChatAdapter.get_by_recipient_phone_number(current_user, show_params[:recipient_phone_number])
    raise ActiveRecord::RecordNotFound unless @chat
    respond_with(@chat)
  end

  private

  def show_params
    params.permit(:recipient_phone_number)
  end
end
