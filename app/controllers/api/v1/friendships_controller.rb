class Api::V1::FriendshipsController < Api::BaseController
  include FriendshipConcern

  def index
    requested = params[:requested].to_b
    pending = params[:pending].to_b

    @requested_users = current_user.user_requested_friends
    @requested_places = current_user.place_requested_friends

    @pending_users = current_user.user_pending_friends
    @pending_places = current_user.place_pending_friends

    render :index and return if requested && pending || !requested && !pending
    render template: "api/v1/friendships/requested" and return if requested
    render template: "api/v1/friendships/pending" and return if pending
  end

  def create
    record_respond(current_user.accept_request(friend), :created)
  end

  def destroy
    record_respond(current_user.decline_request(friend))
  end
end
