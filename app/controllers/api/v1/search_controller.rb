class Api::V1::SearchController < Api::BaseController
  def index
    search_params = { query: { params: permitted_params, current_user: current_user }}

    searcher = Search.new(search_params)

    @places = searcher.places
    @users = searcher.users
  end

  private

  def permitted_params
    params.permit(:name, :sex, categories: [])
  end
end
