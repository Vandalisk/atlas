class Api::V1::ServicesController < Api::BaseController
  def index
    @services = current_user.events_nearby.closest_to(current_user)
  end
end
