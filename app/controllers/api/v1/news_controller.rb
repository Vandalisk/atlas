class Api::V1::NewsController < Api::BaseController
  def index
    @news = News.news_nearby(current_user)
  end
end
