class Api::V1::FavouriteEventsController < Api::BaseController
  def index
    FavouriteEventPolicy.new(current_user, nil).index?
    @favourite_events = current_user.favourite_events
  end

  def create
    record_respond(current_user.favourite_events << Event.find(params[:id]), :created)
  end
end
