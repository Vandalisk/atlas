class Api::V1::UsersController < Api::BaseController

  skip_before_filter :authenticate, except: :update

  def index
    authenticate if request.headers["Authorization"].present?

    if @current_user
      #TODO: Move to search controller in future
      if params[:first_name].present? && params[:last_name].present?
        @user = User.find_by!(first_name: params[:first_name], last_name: params[:last_name])
        render :show and return
      end

      #TODO: Move to search controller in future
      if params[:phone_number].present?
        @user = User.find_by!(phone_number: Normalizer.phone_number(params[:phone_number]))
        render :show and return
      end

      @user = @current_user
      render :show and return
    end

    respond(User.get_by_params(Normalizer.phone_number_in_params(find_params)), 200)
  end

  def show
    @user = User.find(params[:id])
    respond_with(@user)
  end

  def update
    record_respond(current_user.update(user_params), :ok)
  end

  def create
    respond(User.create!(user_params), :created)
  end

  def destroy
    if params[:mobile_test].present?
      unless params.has_key?(:phone_number)
        render json: { message: 'Phone number is required. Please try again.' }, status: 400
        return
      end

      users = User.where(phone_number: Normalizer.phone_number_in_params(params)[:phone_number])

      if users.empty?
        render json: { message: 'Record with such phone_number and social marker is not found' }, status: 404
        return
      end

      users.destroy_all

      render json: { message: 'Successfully deleted all accounts by this phone number' }, status: 200
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  private

  def user_params
    params.require(:user).permit(
      :first_name,
      :last_name,
      :age,
      :latitude,
      :longitude,
      :sex,
      :latitude,
      :longitude,
      :phone_number,
      :social_id,
      :social_marker,
      :invisible,
      :image_path
    )
  end

  def find_params
    params.require(:user).permit(:phone_number, :social_id, :social_marker)
  end
end
