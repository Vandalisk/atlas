class Api::V1::ImageUploadsController < Api::BaseController
  skip_before_filter :authenticate

  def create
    blob = AzureImageUploader.new.upload(params[:image])

    render json: { image_url: "#{AzureImageUploader::AZURE_IMAGES_DIR_PATH}/#{blob.name}" }.to_json
  end
end

