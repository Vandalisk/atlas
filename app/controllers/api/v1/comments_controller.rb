class Api::V1::CommentsController < Api::BaseController
  def index
    @comments = Comment.where(place_id: params[:place_id])
  end

  def create
    record_respond(
      Comment.create!(create_params.merge(user: current_user, place_id: params[:place_id])),
      :created
    )
  end

  def destroy
    @comment = Comment.find(params[:id])
    authorize @comment
    record_respond(@comment.destroy)
  end

  private

  def create_params
    params.require(:comment).permit(:body, :rating)
  end
end
