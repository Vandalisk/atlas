class Api::BaseController < ApplicationController
  respond_to :json

  rescue_from ActiveRecord::RecordInvalid, with: :record_invalid
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ArgumentError, with: :invalid_arguments
  rescue_from TypeError, with: :type_invalid
  rescue_from JWT::DecodeError, with: :invalid_token
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  # TODO: Add json for 500 status code

  before_action :authenticate

  def authenticate
    @current_user = UserAdapter.get_user(Authenticate.by(Token.handle(request.headers["Authorization"])))
    raise ActiveRecord::RecordNotFound unless @current_user
  end

  def current_user
    @current_user
  end

  def current_place
    current_place = @current_user.place
    raise ActiveRecord::RecordNotFound unless current_place
    current_place
  end

  protected

  def user_not_authorized
    head :forbidden
  end

  def respond(record, status=false)
    raise ActiveRecord::RecordNotFound unless record
    render json: { record.class.name.downcase.to_sym => { token: Token.generate(record) } },
      status: status || :ok
  end

  def type_invalid(exception)
    render json: { errors: exception.message }.to_json, status: :bad_request
    return
  end

  def record_respond(record, status=false)
    record ? (head status || :ok) : (head :bad_request)
  end

  def record_invalid(exception)
    render json: { errors: exception.record.errors.full_messages }.to_json, status: :bad_request
    return
  end

  def record_not_found(exception)
    #TODO: Add specs
    model_name = String(exception).split('with').first.split('find').last.delete(' ')

    render json: { errors: "#{model_name} not found" }.to_json, status: :not_found
  end

  def invalid_arguments(exception)
    render json: { errors: exception.message }.to_json, status: :bad_request
    return
  end

  def invalid_token
    head :forbidden
  end
end
