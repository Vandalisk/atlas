class SuperUsers::EventsController < SuperUsers::ApplicationController
  def index
    search_term = params[:search].to_s.strip
    resources = Administrate::Search.new(resource_resolver, search_term).run
    resources = resources.order(:id) if params[:order].nil?
    resources = order.apply(resources)
    resources = resources.page(params[:page]).per(records_per_page)
    page = Administrate::Page::Collection.new(dashboard, order: order)

    render locals: {
      resources: resources,
      search_term: search_term,
      page: page,
    }
  end

  def create
    params[:event][:place_id] = Place.find_by(name: params[:event][:place]).id
    resource = resource_class.new(resource_params)

    if resource.save
      redirect_to(
        [namespace, resource],
        notice: translate_with_resource("create.success"),
      )
    else
      render :new, locals: {
        page: Administrate::Page::Form.new(dashboard, resource),
      }
    end
  end

  def edit
    gon.place = requested_resource.place
    render locals: {
      page: Administrate::Page::Form.new(dashboard, requested_resource),
    }
  end

  def update
    params[:event][:place_id] = Place.find_by(name: params[:event][:place]).id
    if requested_resource.update(resource_params)
      redirect_to(
        [namespace, requested_resource],
        notice: translate_with_resource("update.success"),
      )
    else
      render :edit, locals: {
        page: Administrate::Page::Form.new(dashboard, requested_resource),
      }
    end
  end
end
