class SuperUsers::ApplicationController < Administrate::ApplicationController
  before_filter :authenticate_super_user!

  before_action :current_setting

  def current_setting
    @current_setting ||= current_super_user.setting
  end
end