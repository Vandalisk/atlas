class SuperUsers::PlacesController < SuperUsers::ApplicationController
  def index
    search_term = params[:search].to_s.strip
    resources = Administrate::Search.new(resource_resolver, search_term).run
    resources = resources.order(:id) if params[:order].nil?
    resources = order.apply(resources)
    resources = resources.page(params[:page]).per(records_per_page)
    page = Administrate::Page::Collection.new(dashboard, order: order)

    render locals: {
      resources: resources,
      search_term: search_term,
      page: page,
    }
  end

  def create
    resource = resource_class.new(create_params.deep_merge(user_attributes: required_user_attributes).to_h)

    if resource.save
      redirect_to(
        [namespace, resource],
        notice: translate_with_resource("create.success"),
      )
    else
      render :new, locals: {
        page: Administrate::Page::Form.new(dashboard, resource),
      }
    end
  end

  def edit
    gon.push({ latitude: requested_resource.latitude, longitude: requested_resource.longitude })
    render locals: {
      page: Administrate::Page::Form.new(dashboard, requested_resource),
    }
  end

  def update
    if requested_resource.update(update_params)
      redirect_to(
        [namespace, requested_resource],
        notice: translate_with_resource("update.success"),
      )
    else
      render :edit, locals: {
        page: Administrate::Page::Form.new(dashboard, requested_resource),
      }
    end
  end

  private

  def create_params
    params.require(:place).permit(
      :name,
      :description,
      :additional_phone_number,
      :max_events_per_day,
      :remote_image_path_url,
      user_attributes: [
        :phone_number,
        :social_id,
        :social_marker,
        :latitude,
        :longitude,
        :image_path,
      ]
    )
  end

  def update_params
    params.require(:place).permit(
      :name,
      :description,
      :image_path,
      :additional_phone_number,
      :image_path,
      :remote_image_path_url
    )
  end

  def required_user_attributes
    {
      first_name: params[:place][:name],
      last_name: params[:place][:name],
      age: "30",
      sex: "Male",
      phone_number: params[:place][:additional_phone_number],
      latitude: params[:place][:latitude],
      longitude: params[:place][:longitude],
      owner: true
    }
  end
end
