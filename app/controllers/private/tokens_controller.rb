class Private::TokensController < ApplicationController
  def index
    places_tokens = User.owners.map {|user| Token.generate(user) }
    users_tokens = User.not_owners.map {|user| Token.generate(user) }

    render json: { places: places_tokens, users: users_tokens }
  end

  def show
    user = User.find(params[:id])
    render json: { token: Token.generate(user) }
  end
end
