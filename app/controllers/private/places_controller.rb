class Private::PlacesController < ApplicationController
  def index
    render json: { collection: Place.all }
  end
end
