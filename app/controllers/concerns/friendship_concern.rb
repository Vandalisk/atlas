module FriendshipConcern
  extend ActiveSupport::Concern
  include PrivateSettings

  included do
    protected

    def friend
      UserAdapter.get_user_by_id(params[:id], params[:type])
    end
  end
end
