class Public::ViolationsController < ApplicationController
  def show
    @violation = Violation.find(params[:id])
  end
end
