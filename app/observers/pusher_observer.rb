class PusherObserver
  def successful_create(entry, channel, event='create')
    Pusher.trigger(
      channel,
      event,
      JSON(RablRails.render(entry,"api/v1/#{entry.class.table_name}/item")).merge("status" => 201)
    )
  end

  def successful_update(entry, channel, event='update')
    Pusher.trigger(
      channel,
      event,
      { status: 200 }
    )
  end
end
