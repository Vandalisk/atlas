class FavouriteEventPolicy < ApplicationPolicy
  def index?
    raise Pundit::NotAuthorizedError if @user.owner?
  end
end
