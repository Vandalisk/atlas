class MessagePolicy < ApplicationPolicy
  def create?
    @record.chat.involve?(@user)
  end

  def update?
    @record.chat.involve?(@user)
  end
end
