class UserPolicy < ApplicationPolicy
  def update?
    @record.eql?(@user)
  end
end
