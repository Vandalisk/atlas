class ChatPolicy < ApplicationPolicy
  def create?
    @record.involve?(@user)
  end

  def update?
    @record.involve?(@user)
  end
end
