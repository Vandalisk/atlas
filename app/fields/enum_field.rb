require "administrate/field/base"

class EnumField < Administrate::Field::Base
  def to_s(resource_name)
    I18n.t("activerecord.attributes.#{resource_name}.#{attributes}.#{data}")
  end

  def enum_attributes(resource_name)
    resource_model(resource_name).send(attributes)
  end

  def selected_attribute(resource_name)
    enum_attributes(resource_name)[data]
  end

  def resource_model(resource_name)
    resource_name.to_s.capitalize.constantize
  end

  def attributes
    attribute.to_s.pluralize
  end
end
