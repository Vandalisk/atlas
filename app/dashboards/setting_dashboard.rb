require "administrate/base_dashboard"

class SettingDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    super_user: Field::BelongsTo,
    id: Field::Number,
    timezone: Field::String,
    max_events_per_day: Field::Number,
    search_radius_friends: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :super_user,
    :id,
    :timezone,
    :max_events_per_day,
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :timezone,
    :max_events_per_day,
    :search_radius_friends,
  ].freeze

  FORM_ATTRIBUTES = [
    :timezone,
    :max_events_per_day,
    :search_radius_friends,
  ].freeze

  def display_resource(setting)
    "#{I18n.t('activerecord.models.setting')} ##{setting.id}"
  end
end
