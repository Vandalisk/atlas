require "administrate/base_dashboard"

class ModeratorDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    email: Field::Email,
    encrypted_password: Field::String,
    password: Field::String,
    password_confirmation: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    radius: Field::Number,
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :id,
    :email,
    :radius
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :email,
    :radius,
  ].freeze

  FORM_ATTRIBUTES = [
    :email,
    :password,
    :password_confirmation,
    :radius,
  ].freeze

  def display_resource(moderator)
    "#{I18n.t('activerecord.models.moderator')} ##{moderator.id}"
  end
end
