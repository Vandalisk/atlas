require "administrate/base_dashboard"

class UserDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    place: Field::HasOne,
    id: Field::Number,
    first_name: Field::String,
    last_name: Field::String,
    age: Field::Number,
    sex: Field::String,
    search_radius_friends: Field::Number,
    invisible: Field::Boolean,
    phone_number: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    image_path: Field::String,
    social_id: Field::String,
    social_marker: Field::String.with_options(searchable: false),
    owner: Field::Boolean,
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :id,
    :first_name,
    :last_name,
    :age,
    :phone_number,
    :search_radius_friends,
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :first_name,
    :last_name,
    :age,
    :sex,
    :search_radius_friends,
    :invisible,
    :phone_number,
    :created_at,
    :updated_at
  ].freeze

  FORM_ATTRIBUTES = [
    :first_name,
    :last_name,
    :age,
    :sex,
    :search_radius_friends,
    :invisible,
    :phone_number
  ].freeze

  def display_resource(user)
    "#{I18n.t('activerecord.models.user')} ##{user.id}"
  end
end
