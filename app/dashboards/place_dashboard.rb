require "administrate/base_dashboard"

class PlaceDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    name: Field::String,
    description: Field::Text,
    events_today: Field::Number,
    latitude: Field::String,
    longitude: Field::String,
    max_events_per_day: Field::Number,
    image_path: Field::String,
    additional_phone_number: Field::String,
    latitude: Field::Number.with_options(decimals: 2),
    longitude: Field::Number.with_options(decimals: 2),
    weekdays_hours: Field::String,
    weekends_hours: Field::String,
    address: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    average_rating: Field::Number.with_options(decimals: 2),
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :id,
    :max_events_per_day,
    :events_today,
    :address,
    :name,
    :description,
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :max_events_per_day,
    :events_today,
    :description,
    :image_path,
    :additional_phone_number,
    :weekdays_hours,
    :weekends_hours,
    :address,
    :latitude,
    :longitude,
    :created_at,
    :updated_at,
    :average_rating,
  ].freeze

  FORM_ATTRIBUTES = [
    :name,
    :description,
    :max_events_per_day,
    :weekdays_hours,
    :weekends_hours,
    :address,
    :latitude,
    :longitude,
    :additional_phone_number,
  ].freeze

  def display_resource(place)
    "#{I18n.t('activerecord.models.place')} ##{place.id}: #{place.name}"
  end
end
