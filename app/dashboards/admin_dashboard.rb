require "administrate/base_dashboard"

class AdminDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    email: Field::Email,
    encrypted_password: Field::String,
    password: Field::String,
    password_confirmation: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :id,
    :email,
    :created_at,
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :email,
    :created_at,
    :updated_at,
  ].freeze

  FORM_ATTRIBUTES = [
    :email,
    :password,
    :password_confirmation,
  ].freeze

  def display_resource(admin)
    "#{I18n.t('activerecord.models.admin')} ##{admin.id}"
  end
end
