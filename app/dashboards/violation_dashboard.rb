require "administrate/base_dashboard"

class ViolationDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    user_id: Field::Number,
    id: Field::Number,
    title: Field::String,
    description: Field::Text,
    latitude: Field::String.with_options(searchable: false),
    longitude: Field::String.with_options(searchable: false),
    status: EnumField,
    category: Field::Number,
    photos: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :id,
    :title,
    :description,
    :user_id,
    :created_at,
    :status
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :title,
    :description,
    :latitude,
    :longitude,
    :category,
    :status,
    :created_at,
    :updated_at
  ].freeze

  FORM_ATTRIBUTES = [
    :title,
    :description,
    :latitude,
    :longitude,
    :status,
    :category
  ].freeze

  def display_resource(violation)
    "#{I18n.t('activerecord.models.violation')} ##{violation.id}"
  end
end
