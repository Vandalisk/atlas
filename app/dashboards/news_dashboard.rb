require "administrate/base_dashboard"

class NewsDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    place: Field::BelongsTo,
    id: Field::Number,
    title: Field::String,
    description: Field::Text,
    radius: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    moderator_id: Field::Number
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :place,
    :id,
    :title,
    :created_at,
    :description,
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :place,
    :id,
    :title,
    :description,
    :created_at,
    :updated_at,
  ].freeze

  FORM_ATTRIBUTES = [
    :place,
    :title,
    :description,
    :moderator_id
  ].freeze

  def display_resource(news)
    "#{I18n.t('activerecord.models.news')} ##{news.id}"
  end
end
