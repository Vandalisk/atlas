require "administrate/base_dashboard"

class EventDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    place: Field::BelongsTo,
    id: Field::Number,
    title: Field::String,
    time: Field::DateTime,
    description: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :place,
    :id,
    :title,
    :time,
    :description,
    :created_at
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :place,
    :id,
    :title,
    :time,
    :description,
    :created_at,
    :updated_at,
  ].freeze

  FORM_ATTRIBUTES = [
    :place,
    :title,
    :time,
    :description,
  ].freeze

  def display_resource(event)
    "#{I18n.t('activerecord.models.event')} ##{event.id}"
  end
end
