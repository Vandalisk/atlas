node(:places) do
  partial("api/v1/places/collection", object: @places)
end

node(:users) do
  partial("api/v1/users/collection", object: @users)
end
