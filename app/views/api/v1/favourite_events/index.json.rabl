collection :@favourite_events

Event::JSON_ATTRS.each do |attr|
  attributes attr
end

node(:place) { |event| partial("api/v1/places/item", object: event.place) }

