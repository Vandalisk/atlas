collection :@news
News::JSON_ATTRS.each do |attr|
  attribute attr
end

node(:place) { |news| partial "api/v1/places/item", object: news.place }

unless Rails.env.production?
  node(:created_at) { |news| news.created_at.strftime('%d/%m/%Y %H:%M:%S') }
  node(:updated_at) { |news| news.updated_at.strftime('%d/%m/%Y %H:%M:%S') }
end
