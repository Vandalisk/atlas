collection :@services
node(:event){ |service| partial("api/v1/events/item", object: service) }
node(:place){ |service| partial("api/v1/places/item", object: service.place) }
