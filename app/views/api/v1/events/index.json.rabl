object false

node(:place) do
  partial("api/v1/places/item", object: @place)
end
node(:events) do
  partial("api/v1/events/collection", object: @events)
end
