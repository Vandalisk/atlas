object false

node(:place) do
  partial("api/v1/places/item", object: @place)
end
node(:events) do
  partial("api/v1/events/item", object: @event)
end
