object false

node(:count) { @chats_counter }
node :chats do
  partial("api/v1/chats/collection", object: @chats)
end
