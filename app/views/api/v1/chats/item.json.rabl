object :@chat

Chat::JSON_ATTRS.each do |attr|
  attributes attr
end

node(:last_message) do |chat|
  if chat.last_message.is_a?(Message)
    partial("api/v1/messages/item", object: chat.last_message)
  else
    chat.last_message
  end
end

node(:recipient) do |chat|
  message_recipient = @current_user == chat.sender ? chat.recipient : chat.sender

  if message_recipient.owner?
    partial "api/v1/places/item", object: message_recipient.place
  else
    partial "api/v1/users/item", object: message_recipient
  end
end
