node(:users) do
  partial("api/v1/users/collection", object: @users)
end

extends "api/v1/friends/users"
extends "api/v1/friendships/users"
