node(:violations) do
  partial("api/v1/violations/collection", object: @violations)
end
