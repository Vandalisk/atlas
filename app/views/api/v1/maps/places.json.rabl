node(:places) do
  partial("api/v1/places/collection", object: @places)
end

extends "api/v1/friends/places"
extends "api/v1/friendships/places"
