object false

Message::JSON_ATTRS.each do |attr|
  attributes attr
end

node(:sender) do |message|
  message_sender = message.sender

  if message_sender.owner?
    partial "api/v1/places/item", object: message_sender.place
  else
    partial "api/v1/users/item", object: message_sender
  end
end

