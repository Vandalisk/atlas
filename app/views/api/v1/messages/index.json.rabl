object false

node(:count) { @messages_counter }
node(:messages) do
  partial("api/v1/messages/collection", object: @messages)
end
