class ViolationMailer < ApplicationMailer
  def complain_email(admins_comment, emails, url, violation)
    @admins_comment = admins_comment
    @url  = url
    @violation = violation
    mail(to: emails, subject: "Нарушение № #{violation.id} из приложения атлас.")
  end
end
