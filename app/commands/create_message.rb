class CreateMessage
  include Wisper::Publisher
  include Broadcast
  include PrivateSettings

  def initialize(chat, message_params)
    @chat = chat
    @message_params = message_params
    @sender = message_params[:sender]
    @recipient = @chat.interlocutor(message_params[:sender])
  end

  def execute
    @recipient.owner? ? check_black_list(@sender, @recipient) : check_friendship(@sender, @recipient)

    message = @chat.messages.create!(@message_params)

    push_to_broadcast(:successful_create, message, "chat_#{@chat.id}")

    message
  end
end
