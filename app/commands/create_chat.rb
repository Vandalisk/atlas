class CreateChat
  include Wisper::Publisher
  include Broadcast
  include PrivateSettings

  def initialize(params)
    @chat_params = params
    @recipient = UserAdapter.get_user_by_phone(String(params[:recipient_phone_number]))
    @sender = UserAdapter.get_user_by_phone(String(params[:sender_phone_number]))
  end

  def execute
    @recipient.owner? ? check_black_list(@sender, @recipient) : check_friendship(@sender, @recipient)

    chat = Chat.create!(recipient: @recipient, sender: @sender)
    chat.messages.create!(sender: @sender, body: @chat_params[:body])

    push_to_broadcast(:successful_create, chat, channel_name)

    chat
  end

  private

  def channel_name
    "#{get_phone_number(@sender)}_#{get_phone_number(@recipient)}_chats"
  end

  def get_phone_number(user)
    user.phone_number[1..-1]
  end
end
