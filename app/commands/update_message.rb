class UpdateMessage
  include Wisper::Publisher
  include Broadcast

  def initialize(chat, message_params)
    @chat = chat
    @message_params = message_params
  end

  def execute
    message = @chat.messages.find(@message_params[:id])

    if message.mark_as_read!(for: message.recipient)
      push_to_broadcast(:successful_update, message, "chat_#{@chat.id}")
    else
      message.errors[:read] = "Message is already read."
      raise ActiveRecord::RecordInvalid.new(message)
    end
  end
end
