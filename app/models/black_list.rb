class BlackList < ActiveRecord::Base
  belongs_to :place
  belongs_to :user

  class << self
    def add_by_phone_number!(place, phone_number)
      blocked_user = UserAdapter.get_user_by_phone(phone_number)
      place.black_list.create!(user: blocked_user)
    end

    def destroy_by_phone_number!(place, phone_number)
      unblocked_user = UserAdapter.get_user_by_phone(phone_number)
      black_list = BlackList.find_by!(place: place, user: unblocked_user)
      place.black_list.destroy(black_list)
    end
  end
end
