class Place < ActiveRecord::Base
  include AreaManipulationsConcern
  JSON_ATTRS = %i(
    id
    name
    description
    additional_phone_number
    image_path
    address
    latitude
    longitude
    weekdays_hours
    weekends_hours
    average_rating
    phone_number
    image_path
  )

  delegate :social_marker,
    :social_id,
    :phone_number,
    :my_friends,
    :user_friends,
    :place_friends,
    :my_requested_friends,
    :user_requested_friends,
    :place_requested_friends,
    :my_pending_friends,
    :user_pending_friends,
    :place_pending_friends,
    :friend_request,
    :remove_friend,
    :latitude,
    :longitude,
    :users_nearby,
    :places_nearby,
    :objects_nearby,
    :accept_request,
    :decline_request,
    :chats,
    :messages,
  to: :user, allow_nil: true

  validates :name, :description, :additional_phone_number, :user, presence: true

  belongs_to :user, -> { where(owner: true) }, dependent: :destroy
  belongs_to :category

  has_many :comments, dependent: :destroy

  has_many :news, dependent: :destroy

  has_many :events, dependent: :destroy
  has_many :black_list

  accepts_nested_attributes_for :user

  scope :avg_rating, -> (id) do
     sql = "
        SELECT
        case
          when COUNT(*) = 0 then 0
          else
          (SUM(rating)::float / COUNT(*))
        end as average
        FROM places
        INNER JOIN comments
        ON comments.place_id = places.id
        WHERE places.id = #{id}
        "
     ActiveRecord::Base.connection.execute(sql).first
  end

  scope :places_in_black_list, -> (place_id) do
    joins(
      'INNER JOIN users ON (places.user_id = users.id)
       INNER JOIN black_lists ON (users.id = black_lists.user_id)
      ').
      where('black_lists.place_id = ? AND owner = TRUE', place_id)
  end

  scope :grep_name, -> (name) { where("name ILIKE ?", "%#{name}%") }

  scope :search_by_category_names, -> (category_names) do
    joins(:category).where(categories: { title: category_names })
  end

  def blocked?(object)
    user = UserAdapter.get_user(object)
    black_list.exists?(user: user)
  end

  def users_in_black_list
    User.users_in_black_list(self.id)
  end

  def places_in_black_list
    Place.places_in_black_list(self.id)
  end

  def recalculate_rating
    self.update(average_rating: Place.avg_rating(self.id)["average"])
  end

  def self.get_by_params(params)
    User.get_by_params(params)
  end
end
