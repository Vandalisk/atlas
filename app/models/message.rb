class Message < ActiveRecord::Base
  #TODO: Remove pending model spec
  DEFAULT_MESSAGE_COUNT = 20

  include OrderQuery
  JSON_ATTRS = %i(id body created_at_millis read sender)

  delegate :recipient, to: :chat, allow_nil: true

  order_query :order_home, [:created_at]

  acts_as_readable on: :created_at

  before_save :mark_read_for_sender

  belongs_to :chat
  belongs_to :sender, foreign_key: :user_id, class_name: "User"

  scope :unread, -> (user) { unread_by(user) }
  scope :asc_order, -> do
    order("id ASC")
  end

  scope :joins_chat_users, -> do
    joins(chat: :sender).
    joins(chat: :recipient)
  end

  scope :keyset_pagination, -> (count, start_message_id) do
    asc_order.
    limit(count).
    order_home_at(find(start_message_id)).
    before
  end

  scope :limit_messages_from_chat, -> (user, chat_id, count, start_message_id) do
    messages = joins(:chat).where(chat_id: chat_id).
      where("chats.sender_id =? OR chats.recipient_id =?", user.id, user.id)

    if start_message_id
      messages.keyset_pagination(count, start_message_id)
    else
      messages.asc_order.last(count || DEFAULT_MESSAGE_COUNT)
    end
  end

  validates_presence_of :body, :chat_id, :sender

  def read
    recipient.have_read?(self)
  end

  def created_at_millis
    Integer(Time.parse(String(self.created_at)))
  end

  def sender_user
    return sender unless sender.owner
  end

  def sender_place
    return sender.place if sender.owner
  end

  private

  def mark_read_for_sender
    mark_as_read!(for: self.sender)
  end
end
