class Admin < ActiveRecord::Base
  devise :database_authenticatable, :timeoutable

  validates :password, length: { minimum: 6 }, if: -> { new_record? || changes[:encrypted_password] }
  validates :password, confirmation: true, if: -> { new_record? || changes[:encrypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:encrypted_password] }

  validates :email, presence: true
  validates :email, uniqueness: true
end
