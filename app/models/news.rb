class News < ActiveRecord::Base
  include AreaManipulationsConcern
  JSON_ATTRS = %i(id title description latitude longitude)

  delegate :latitude, :longitude, to: :place, allow_nil: true

  belongs_to :place
  belongs_to :moderator

  validates :title, :description, :place, presence: true
end
