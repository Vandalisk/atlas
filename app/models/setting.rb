class Setting < ActiveRecord::Base
  belongs_to :super_user

  after_update :recalculate_max_events, if: -> { max_events_per_day_changed? }
  after_update :change_time_zone, if: -> { timezone_changed? }
  after_update :change_search_radius_friends, if: -> { search_radius_friends_changed? }

  private

  def recalculate_max_events
    Place.update_all(max_events_per_day: self.max_events_per_day)
  end

  def change_time_zone
    ENV['TIMEZONE'] = self.timezone
    change_timezone_globally(self.timezone)
  end

  def change_search_radius_friends
    User.update_all(search_radius_friends: self.search_radius_friends)
  end

  def change_timezone_globally(new_timezone)
    env_text = File.read('.env')
    lines = env_text.split("\n")
    timezone_with_index = lines.enum_for(:each_with_index).select { |el, index| /TIMEZONE=/.match(el) }
    lines[timezone_with_index[0][1]] = "TIMEZONE=#{new_timezone}"
    new_text = lines.join("\n")
    File.open('.env', 'w') { |file| file.write(new_text) }
  end
end
