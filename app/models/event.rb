class Event < ActiveRecord::Base
  include AreaManipulationsConcern

  JSON_ATTRS = %i(id title description time created_at)
  belongs_to :place

  has_and_belongs_to_many :users, join_table: :users_events

  validates :title, :description, :place_id, :time, presence: true
  validate :count_within_limit, on: :create

  after_save :increase_events_counter

  private

  def count_within_limit
    if self.place.events(:reload).count >= self.place.max_events_per_day
      errors.add(:base, "Exceeded events limit")
    end
  end

  def increase_events_counter
    self.place.update(events_today: self.place.events_today+1)
  end
end
