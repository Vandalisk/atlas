class User < ActiveRecord::Base
  JSON_ATTRS = %i(id first_name last_name age sex latitude longitude phone_number image_path)

  include NormalizePhoneNumberConcern
  include SocialNetworkAuthConcern
  include ModelFriendshipConcern
  include AreaManipulationsConcern

  acts_as_reader

  validates :first_name,
    :last_name,
    :age,
    :sex,
    :search_radius_friends,
  presence: true

  validates :invisible, inclusion: { in: [true, false] }
  validates :sex, inclusion: { in: %w[Male Female], message: "value should equal Male or Female" }
  validates :age, inclusion: { in: 1..100, message: "value should be in range from 1 to 100" }

  has_one :place, dependent: :destroy
  has_many :violations, dependent: :destroy

  has_many :messages
  has_many :chats, foreign_key: :sender_id
  has_and_belongs_to_many :favourite_events, join_table: :users_events, class_name: :Event

  before_save :fill_full_name

  scope :owners , -> { where(owner: true) }
  scope :not_owners, -> { where(owner: false) }
  scope :visible, -> { where(invisible: false) }
  scope :grep_name, -> (name) { where("full_name ILIKE ?", "%#{name}%") }

  scope :users_in_black_list, -> (place_id) do
    joins('INNER JOIN black_lists ON (users.id = black_lists.user_id)').
      where('black_lists.place_id = ? AND owner = FALSE', place_id)
  end

  def events_nearby
    Event.events_nearby(self)
  end

  def chats
    Chat.where("sender_id = ? OR recipient_id = ?", self.id, self.id)
  end

  def all_chats
    self.chats + self.owned_chats
  end

  def owned_chats
    Chat.where(owner: self)
  end

  private

  def fill_full_name
    self.full_name = self.first_name + ' ' + self.last_name
  end
end
