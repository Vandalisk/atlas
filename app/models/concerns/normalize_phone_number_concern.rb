module NormalizePhoneNumberConcern
  extend ActiveSupport::Concern

  included do
    before_save :format_phone

    def format_phone(phone_number=false)
      phone_number ||= self.phone_number

      self.phone_number = Normalizer.phone_number(phone_number)
    end
  end
end
