module SocialNetworkAuthConcern
  extend ActiveSupport::Concern

  included do
    # Default value in db for social_marker is digits(3)
    enum social_marker: %i(vk ok fb digits)

    validates :phone_number, :social_marker, presence: true
    validates :social_id, :phone_number, uniqueness: { scope: :social_marker }

    before_save :set_digit_social_id, if: -> { self.class.social_markers[social_marker].equal?(3) }

    # TODO: Add specs for this method
    def self.get_by_params(params)
      record = get_by_params_predicates(params).order(:created_at).first!
      record.owner ? record.place : record
    end

    private

    def set_digit_social_id
      self.social_id = phone_number
    end

    def self.get_by_params_predicates(params)
      if params["social_marker"].present?
        where("phone_number = ? OR social_marker = ? AND social_id = ?",
          params["phone_number"],
          params["social_marker"],
          params["social_id"]
        )
      else
        where("phone_number = ? OR social_marker IS NULL AND social_id = ?",
          params["phone_number"], params["social_id"]
        )
      end
    end
  end
end
