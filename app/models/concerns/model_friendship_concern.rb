module ModelFriendshipConcern

  extend ActiveSupport::Concern

  included do
    #TODO: Add specs
    has_friendship

    def my_friends
      user_friends | place_friends
    end

    def user_friends
      users { friends }
    end

    def place_friends
      places { friends }
    end

    def my_requested_friends
      user_requested_friends | place_requested_friends
    end

    def user_requested_friends
      users { requested_friends }
    end

    def place_requested_friends
      places { requested_friends }
    end

    def my_pending_friends
      user_pending_friends | place_pending_friends
    end

    def user_pending_friends
      users { pending_friends }
    end

    def place_pending_friends
      places { pending_friends }
    end

    def friend_request(friend)
      friend = UserAdapter.get_user(friend)

      unless friendable == friend || HasFriendship::Friendship.exist?(friendable, friend)
        transaction do
          HasFriendship::Friendship.create_relation(friendable, friend, status: 0)
          HasFriendship::Friendship.create_relation(friend, friendable, status: 1)
        end
      end
    end

    def accept_request(friend)
      on_relation_with(friend) do |one, other|
        friendship = HasFriendship::Friendship.find_unblocked_friendship(one, other)
        friendship.accept! if can_accept_request?(friendship)
      end
    end

    def decline_request(friend)
      super
    rescue NoMethodError
      nil
    end

    alias_method :remove_friend, :decline_request

    private

    def users
      yield.where(owner: false)
    end

    def places
      Place.where(user: yield.where(owner: true))
    end

    def can_accept_request?(friendship)
      return if friendship.accepted?
      return if friendship.pending? && record == friendship.friendable
      return if friendship.requested? && record == friendship.friend

      true
    rescue NoMethodError
      nil
    end

    def on_relation_with(friend)
      friend = UserAdapter.get_user(friend)

      transaction do
        yield(friendable, friend)
        yield(friend, friendable)
      end
    end

    def record
      UserAdapter.get_user(self)
    end

    alias_method :friendable, :record
  end
end
