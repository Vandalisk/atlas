module AreaManipulationsConcern
  extend ActiveSupport::Concern
  DEFAULT_DISTANCE = 500

  included do
    scope :closest_to, -> (user) do
      order(
        "point(latitude, longitude) <-> point(#{user.latitude}, #{user.longitude})"
      )
    end

    scope :area_request, -> (current_user, distance=nil) do
      where(
        "earth_box(ll_to_earth(?, ?), ?) @> ll_to_earth(users.latitude, users.longitude)",
        current_user.latitude,
        current_user.longitude,
        Integer(distance || DEFAULT_DISTANCE)
      )
    end

    scope :places_nearby, -> (current_user, distance=nil) do
      Place.joins(:user).
        area_request(current_user, distance).
        where.not(user_id: current_user.id)
    end

    scope :users_nearby, ->(current_user) do
      User.area_request(current_user).
        where(owner: false).
        where.not(id: current_user.id)
    end

    scope :news_nearby, -> (current_user) do
      News.joins(place: :user).
        where(
          "earth_box(ll_to_earth(users.latitude, users.longitude), news.radius) @> ll_to_earth(?, ?)",
          current_user.latitude,
          current_user.longitude
        ).
        order(created_at: :desc)
    end

    scope :events_nearby, -> (current_user) { Event.joins(place: :user).area_request(current_user) }

    scope :violations_nearby, -> (current_user, distance=nil) do
      Violation.where("
          earth_box(ll_to_earth(?, ?), ?)
          @> ll_to_earth(latitude, longitude)
        ",
          current_user.latitude,
          current_user.longitude,
          Integer(distance || DEFAULT_DISTANCE)
        )
    end
  end
end
