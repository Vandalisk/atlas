class Violation < ActiveRecord::Base
  include AreaManipulationsConcern

  JSON_ATTRS = %i(id title description latitude longitude category photos)

  belongs_to :user

  enum status: %i(under_consideration published)

  validates :title,
    :description,
    :latitude,
    :longitude,
    presence: true

  def self.status_by_value(status)
    statuses.key(status)
  end
end
