class Comment < ActiveRecord::Base
  JSON_ATTRS = %i(id body user_id place_id rating)

  belongs_to :user
  belongs_to :place

  validates :rating, numericality: { greater_than: 0, less_than_or_equal_to: 5 }
  validates :rating, presence: true
  validates :body, length: { maximum: 300 }, allow_blank: true

  after_save :update_rating
  after_destroy :update_rating

  private

  def update_rating
    place.recalculate_rating
  end
end
