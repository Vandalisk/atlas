class Search < ActiveRecord::Base
  attr_accessor :query

  def places
    if query[:params][:name].present? && query[:params][:categories].present?
      return Place.search_by_category_names(query[:params][:categories]).grep_name(query[:params][:name])
    end

    if query[:params][:categories].present?
      return Place.places_nearby(query[:current_user]).search_by_category_names(query[:params][:categories])
    end

    return Place.grep_name(query[:params][:name]) if query[:params][:name].present?

    Place.all
  end

  def users
    if query[:params][:sex].present? && query[:params][:name].present?
      return User.not_owners.where(sex: query[:params][:sex]).grep_name(query[:params][:name])
    end

    if query[:params][:sex].present?
      return User.users_nearby(query[:current_user]).where(sex: query[:params][:sex])
    end

    return User.not_owners.grep_name(query[:params][:name]) if query[:params][:name].present?

    User.not_owners
  end
end
