class Chat < ActiveRecord::Base
  JSON_ATTRS = %i(
    id
    users
    unread_sender_messages_count
    unread_recipient_messages_count
    last_message
    last_read_message_id
  )

  acts_as_reader

  belongs_to :sender, foreign_key: :sender_id, class_name: "User"
  belongs_to :recipient, foreign_key: :recipient_id, class_name: "User"

  has_many :messages, -> { joins_chat_users }, dependent: :destroy

  validates_presence_of :recipient, :sender

  validate :uniqueness_of_recipient_sender_pair

  scope :involving, -> (user) { where("sender_id =? OR recipient_id =?", user.id, user.id) }
  scope :desc_order, -> { order("id DESC") }
  scope :asc_order, -> { order("id ASC") }
  scope :keyset_pagination, -> (count, start_message_id) do
    Message.
    asc_order.
    limit(count).
    order_home_at(find(start_message_id)).
    before
  end

  def unread_sender_messages
    messages.
    where("messages.id IN (?)",
      Message.select(:id).from(Message.unread_by(self.sender), :sender_messages_ids)
    )
  end

  def interlocutor(user)
    ([sender, recipient] - [user]).first
  end

  def unread_recipient_messages
    messages.
    where("messages.id IN (?)",
      Message.select(:id).from(Message.unread_by(self.recipient), :recipient_messages_ids)
    )
  end

  def read_messages
    messages.
    where("messages.id IN (?) OR messages.id IN (?)",
      Message.select(:id).from(Message.read_by(self.sender), :sender_messages_ids),
      Message.select(:id).from(Message.read_by(self.recipient), :recipient_messages_ids)
    )
  end

  def retrieved_messages
    messages.
    where("messages.chat_id = ? AND messages.user_id = ?", self, self.sender)
  end

  def sended_messages
    messages.
    where("messages.chat_id = ? AND messages.user_id = ?", self, self.recipient)
  end

  def involve?(user)
    sender.id.equal?(user.id) || recipient.id.equal?(user.id)
  end

  def users
    {
      recipient_phone_number: self.recipient_phone_number,
      sender_phone_number: self.sender_phone_number
    }
  end

  def recipient_phone_number
    UserAdapter.get_user(recipient).phone_number
  end

  def sender_phone_number
    UserAdapter.get_user(sender).phone_number
  end

  def last_message
    self.messages.last || {}
  end

  def last_read_message_id
    self.read_messages.last.try(:id) || 0
  end

  def unread_sender_messages_count
    self.unread_sender_messages.count || 0
  end

  def unread_recipient_messages_count
    self.unread_recipient_messages.count || 0
  end

  def self.involving_pair(recipient, sender)
    chat_participants = { recipient_id: recipient.id, sender_id: sender.id }

    self.find_by(
      "(sender_id = :sender_id AND recipient_id = :recipient_id) OR
       (sender_id = :recipient_id AND recipient_id = :sender_id)",
       chat_participants
    )
  end

  def uniqueness_of_recipient_sender_pair
    return if sender_id.nil? || recipient_id.nil?

    if Chat.involving_pair(recipient, sender)
      errors.add(:base, "Chat between users with id = #{sender_id} and id = #{recipient_id} already exists.")
    end
  end
end
